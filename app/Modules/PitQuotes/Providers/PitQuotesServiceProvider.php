<?php
namespace App\Modules\PitQuotes\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PitQuotesServiceProvider extends ServiceProvider
{
	/**
	 * Register the PitQuotes module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PitQuotes\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PitQuotes module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pit_quotes', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pit_quotes', base_path('resources/views/vendor/pit_quotes'));
		View::addNamespace('pit_quotes', realpath(__DIR__.'/../Resources/Views'));
	}
}
