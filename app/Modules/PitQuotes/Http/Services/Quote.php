<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 21.04.2017
 * Time: 8:07
 */

namespace App\Modules\PitQuotes\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class Quote extends Base
{
    protected $modelName = \App\Modules\PitQuotes\Database\Models\Quote::class;
    protected $hasStatus = true;
}