<?php
namespace App\Modules\PitQuotes\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class QuotesController extends Controller
{
    protected $prefix = 'admin:quotes';
    protected $moduleName = 'pit_quotes';
    protected $serviceName = App\Modules\PitQuotes\Http\Services\Quote::class;

//    protected function index()
//    {
//        var_dump($this->getViewRoute());
//        view($this->getViewRoute());
//        die();
//    }

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'quote' => 'required|min:5',
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'id' => 'required',
            'quote' => 'required|min:5',
        ]);
        return parent::edit($request, $id);
    }
}