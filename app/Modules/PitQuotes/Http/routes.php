<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function () {
    Route::group(['middleware' => 'JournalistAuth'], function () {
        Route::group(['prefix' => 'quotes'], function () {
            $asAction = ':quotes';
            Route::get('/', ['as' => $asAction . ':index', 'uses' => 'Admin\QuotesController@index']);
            Route::any('/add', ['as' => $asAction . ':add', 'uses' => 'Admin\QuotesController@add']);
            Route::get('/{id}/active', ['as' => $asAction . ':active', 'uses' => 'Admin\QuotesController@active']);
            Route::any('/{id}/edit', ['as' => $asAction . ':edit', 'uses' => 'Admin\QuotesController@edit']);
        });
    });
});