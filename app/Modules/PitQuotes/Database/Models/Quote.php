<?php

namespace App\Modules\PitQuotes\Database\Models;

use App\Modules\ThePit\Database\Models\Quote as Base;

class Quote extends Base
{
    protected $fillable = [
        'quote',
        'status'
    ];
}