@extends('pie_base::admin.layouts.edit')

@section('title_name'){{trans('pit_quotes::main.quote')}}@endsection

@section('form_body')
<link href="{{$app['pie_article.assets']->getPath('/css/colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet">

{!! Form::hidden('id', $model->id, ['class'=>'form-control'] ) !!}

<div class="form-group">
    {!! Form::label('Quote') !!}
    {!! Form::textarea('quote', $model->quote, ['class'=>'form-control','required'] ) !!}
</div>
<div class="ln_solid"></div>
@endsection

@section('scripts')
    <script src="{{$app['pie_article.assets']->getPath('/js/lib/colorpicker/bootstrap-colorpicker.js')}}"></script>
    <script>
        $(function () {
            $('.colorpicker_init').colorpicker({format: 'hex'});
        });
    </script>
@endsection