@extends('pie_base::admin.layouts.all')

@section('page_title') {{trans('pit_quotes::main.quotes')}} @endsection

@section('tHead')
    <th>Id</th>
    <th>Quota</th>
    <th>Status</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->id}}</td>
            <td>{{$model->quote}}</td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn {{($model->status)?"btn-raised btn-warning":"btn-raised btn-info"}}"
                   href="{{route('admin:quotes:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                <a class="btn btn-raised btn-success" href="{{route('admin:quotes:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection