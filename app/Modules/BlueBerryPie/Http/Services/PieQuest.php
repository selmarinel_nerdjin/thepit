<?php
namespace App\Modules\BlueBerryPie\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;
use Illuminate\Http\Request;

class PieQuest extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\BlueBerryPie\Database\Models\PieQuest';

    public function prepareSelect(){
        $quests = [];
        $model = $this->getModel();
        $this->setWhere(['status'=>$model::STATUS_ACTIVE]);
        foreach($this->getAll() as $q){
            $quests[$q->id] = "[{$q->id}] {$q->question}";
        }
        return $quests;
    }

}