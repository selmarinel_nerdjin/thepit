<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 30.07.2016
 * Time: 18:35
 */

namespace App\Modules\BlueBerryPie\Http\Controllers\Admin;


use App\Modules\BlueBerryPie\Http\Services\PieAnswer;
use App\Modules\BlueBerryPie\Http\Services\PieQuest;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class GameController extends Controller
{
    protected $prefix = 'game';
    protected $moduleName = 'blue_berry_pie';
    protected $serviceName = 'App\Modules\BlueBerryPie\Http\Services\PieGame';

    public function test(Request $request, $id)
    {
        $model = $this->service->getOne($id);
        if (!$model) {
            abort(404);
        }
        return view($this->getViewRoute('index'), ['game' => $model->getInfo()]);
    }

    public function gq(Request $request, $id)
    {
        $service = new PieQuest();
        $model = $service->getOne($id);
        if (!$model) {
            return $this->sendWithErrors('null');
        }
        return $this->sendOk(['gq' => $model->getInfo()]);
    }

    public function newQ(Request $request)
    {
        return abort(401);
    }

    public function editQ(Request $request, $id)
    {
        $model = $this->service->getOne($id);
        if (!$model) {
            dd('No found Game');
        }
        if ($request->method() == 'GET') {
            return view($this->getViewRoute('edit'), ['collection' => $model->quests]);
        } else {
            $response = $request->all();
            foreach ($response['question'] as $key=>$value){
                $question = new PieQuest();
                $question = ($question->getOne($response['q_id'][$key]))?:$question;
                $question->question = $value;
                $question->rule = $response['rule'][$key];
                $question->save();

                foreach ($response['next_question'] as $a_key=>$a_value){
                    $answer = new PieAnswer();
                    $answer = ($answer->getOne($response['a_id'][$a_key]))?:$answer;
                    $answer->answer = $response['answer'][$a_key];
                    $answer->value = $response['value'][$a_key];
                    $answer->quest_id = $question->id;
                    $answer->answer_id = $response['answer_id'][$a_key];

                    if(!$answer->answer_relation){
                        $relate = new PieQuest();
                        $relate = $relate->getModel()->create([
                            'question' => $response['next_question'][$a_key],
                            'game_id' => $question->game_id,
                        ]);
                        $answer->answer_id = $relate->id;
                    } else {
                        $answer->answer_relation->question = $response['next_question'][$a_key];
                        $answer->answer_relation->save();
                    }
                    $answer->save();
                }
            }
            return redirect(route('q:game',$id));
//            $model->answer->question = $response[0]['question'];
        }
    }
}