<?php
namespace App\Modules\BlueBerryPie\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BlueBerryPieDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('App\Modules\BlueBerryPie\Database\Seeds\FoobarTableSeeder');
	}

}
