<?php
namespace App\Modules\BlueBerryPie\Database\Models;

use App\Modules\PieBase\Database\Models\File;

class PieCover extends File
{
    protected $path = 'blueberry/';
}
