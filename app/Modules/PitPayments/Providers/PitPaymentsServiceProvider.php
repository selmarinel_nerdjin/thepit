<?php
namespace App\Modules\PitPayments\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PitPaymentsServiceProvider extends ServiceProvider
{
	/**
	 * Register the PitPayments module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PitPayments\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PitPayments module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pit_payments', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pit_payments', base_path('resources/views/vendor/pit_payments'));
		View::addNamespace('pit_payments', realpath(__DIR__.'/../Resources/Views'));
	}
}
