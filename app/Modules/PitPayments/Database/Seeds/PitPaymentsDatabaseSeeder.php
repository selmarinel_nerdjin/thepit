<?php
namespace App\Modules\PitPayments\Database\Seeds;

use App\Modules\PitPayments\Database\Models\PaymentLink;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PitPaymentsDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		PaymentLink::create([
		    "name" => "Concert.ua",
            "url" => "https://www.concert.ua/eventpage/the-pit-2017"
        ]);
		// $this->call('App\Modules\PitPayments\Database\Seeds\FoobarTableSeeder');
	}

}
