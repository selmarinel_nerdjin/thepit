<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 20.04.2017
 * Time: 19:26
 */

namespace App\Modules\PitPayments\Database\Models;

use App\Modules\PieBase\Database\Models\Base;

class PaymentLink extends Base
{
    protected $fillable =[
        'name',
        'url',
        'status'
    ];

    protected $table = 'payments_links';
}