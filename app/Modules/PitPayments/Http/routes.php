<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
    Route::get('/concert/ua', ['as' => 'concert:ua', 'uses' => 'PaymentController@concertUa']);
    Route::group(['prefix' => 'admin', 'middleware' => ['webAdmin']], function () {
        $asPrefix = 'admin:';
        Route::group(['prefix' => 'payments', 'middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
            $asAction = 'payment:';
        });
    });
});