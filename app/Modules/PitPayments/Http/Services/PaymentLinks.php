<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 20.04.2017
 * Time: 19:35
 */

namespace App\Modules\PitPayments\Http\Services;

use App\Modules\PitPayments\Database\Models\PaymentLink as Model;
use App\Modules\PieBase\Http\Services\Base;

class PaymentLinks extends Base
{
    protected $modelName = Model::class;

    const CONCERT_UA = 1;

    public function getUrl($linkId = 1)
    {
        return $this->getOne($linkId);
    }
}