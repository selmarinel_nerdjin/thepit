<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 20.04.2017
 * Time: 19:42
 */

namespace App\Modules\PitPayments\Http\Controllers;

use App;
use App\Modules\PieBase\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\PitPayments\Http\Services\PaymentLinks as Service;

class PaymentController extends Controller
{
    protected $serviceName = Service::class;

    /**
     * todo: Remove after update
     * @deprecated
     */
    const CONCERT_UA = "https://www.concert.ua/eventpage/the-pit-2017";

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function concertUa(Request $request)
    {
        $site = $this->service->getUrl();
        if ($site) {
            return redirect($site->url);
        } else {
            return redirect(self::CONCERT_UA);
        }
    }
}