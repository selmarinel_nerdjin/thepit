<?php
namespace App\Modules\PieSponsors\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieSponsorsServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieSponsors module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieSponsors\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieSponsors module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_sponsors', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_sponsors', base_path('resources/views/vendor/pie_sponsors'));
		View::addNamespace('pie_sponsors', realpath(__DIR__.'/../Resources/Views'));
	}
}
