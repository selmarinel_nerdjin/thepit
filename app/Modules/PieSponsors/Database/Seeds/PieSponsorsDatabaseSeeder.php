<?php
namespace App\Modules\PieSponsors\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PieSponsorsDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('App\Modules\PieSponsors\Database\Seeds\FoobarTableSeeder');
	}

}
