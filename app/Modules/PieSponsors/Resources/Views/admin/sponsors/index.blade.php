@extends('pie_base::admin.layouts.all')

@section('page_title') {{trans('pie_article::main.sub_menu_news')}} @endsection

@section('tHead')
    <th>{{trans('pie_sponsors::main.admin_name')}}</th>
    <th>{{trans('pie_sponsors::main.admin_link')}}</th>
    <th>{{trans('pie_sponsors::main.admin_emblem')}}</th>
    <th>{{trans('pie_base::main.table_status')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->name}}

            </td>
            <td>{{$model->link}}</td>
            <td>
                <img src="{{$model->getCover()}}" width="75" class="img-responsive">
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn {{($model->status)?"btn-raised btn-warning":"btn-raised btn-info"}}"
                   href="{{route('admin:sponsors:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                <a class="btn btn-raised btn-success" href="{{route('admin:sponsors:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn btn-danger" href="{{route('admin:sponsors:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection