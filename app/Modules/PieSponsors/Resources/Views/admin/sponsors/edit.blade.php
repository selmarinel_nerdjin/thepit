@extends('pie_base::admin.layouts.edit')

@section('title_name'){{trans('pie_article::main.sub_menu_news')}}@endsection

@section('head')
@endsection

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label(trans('pie_sponsors::main.admin_name')) !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_sponsors::main.admin_link')) !!}
        {!! Form::text('link', $model->link, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_sponsors::main.admin_emblem')) !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i class="glyphicon glyphicon-plus"></i> {{trans('pie_article::main.select_cover')}}</div></span>
            {!! Form::file('cover',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{$model->getCover()}}" width="250" class="img-responsive">
        </div>
    </div>
@endsection

@section('scripts')
    @yield('add_script')
    @yield('media_script')
@endsection