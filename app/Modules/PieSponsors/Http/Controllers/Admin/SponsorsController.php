<?php
namespace App\Modules\PieSponsors\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class SponsorsController extends Controller
{
    protected $prefix = 'admin:sponsors';
    protected $moduleName = 'pie_sponsors';
    protected $serviceName = App\Modules\PieSponsors\Http\Services\Sponsor::class;

    public function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel()
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'name' => 'required|min:2',
            'link' => 'required',
            'cover_id' => '',
        ]);
        if ($request->method() != 'GET' && isset($request['cover'])) {
            $imageId = $this->saveFile($request);
            $request->merge(array('cover_id' => $imageId));
        }
        $result = parent::add($request);
        if (isset($this->service->getModel()->id)) {
            $this->saveFile($request, $this->service->getModel()->id);
        }
        return $result;
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'name' => 'required|min:2',
            'link' => 'required',
            'cover_id' => '',
        ]);
        if ($request->hasFile('cover')) {
            $imageId = $this->saveFile($request);
            $request->merge(array('cover_id' => $imageId));
        }
        return parent::edit($request, $id);
    }

    private function saveFile(Request $request)
    {
        $service = new App\Modules\PieSponsors\Http\Services\Cover($request->file('cover'));
        $service->saveFile($request->user()->id);
        return $service->getModel()->id;
    }

}