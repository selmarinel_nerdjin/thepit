<?php
namespace App\Modules\PieArticle\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

class Like extends Base
{
    public $timestamps = true;
    protected $table = 'likes';

    protected $fillable = array(
        'user_id',
        'article_id',
        'status'
    );

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}