<?php
namespace App\Modules\PieArticle\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

/**
 * Class Articles
 * @package App\Modules\PieArticle\Database\Models
 * @property App\Modules\PieTranslator\Database\Models\TranslateArticles $lang
 */
class Articles extends Base
{
    protected $with = [
        'user',
        'category',
        'cover',
        'comments',
        'likes',
        'tags',
    ];
    public $timestamps = true;
    protected $table = 'articles';

    protected $fillable = array(
        'user_id',
        'name',
        'info',
        'text',
        'image_id',
        'category_id',
        'views',
        'status',
        'media_id',
        'views',
        'keywords',
        'description'
    );

    public function lang()
    {
        return $this->hasMany('App\Modules\PieTranslator\Database\Models\TranslateArticles', 'article_id');
    }

    public function category()
    {
        return $this->belongsTo('\App\Modules\PieArticle\Database\Models\Category', 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cover()
    {
        return $this->belongsTo('\App\Modules\PieArticle\Database\Models\Cover', 'image_id');
    }

    public function getCategory()
    {
        if (!isset($this->category->id) || $this->category->status != self::STATUS_ACTIVE) {
            $this->category = (new Category())->setDefault();
        }
        return $this->category;
    }

    public function getDate($format = 'j M Y')
    {
        return date($format, strtotime($this->created_at));
    }

    public function updateViews()
    {
        $this->views = ++$this->views;
        self::query()->where('id', $this->id)->update(['views' => $this->views]);
    }

    public function getCover($w = null)
    {
        return ($this->cover) ? $this->cover->getCover($w) : App('full_fill');
    }

    public function getUser()
    {
        return (isset($this->user)) ? $this->user : App::make('\App\Modules\PieBase\Database\Models\User')->setDefault();
    }

    public function getInfo($localization = 1)
    {
        $language = (object) $this->getTranslateArticle($localization);
        return [
            'id' => $this->id,
            'cover' => $this->getCover(350),
            'name' => $language->name,
            'info' => htmlspecialchars(mb_substr($language->info,0,25)) . "...",
            'text' => $language->text,
            'comments' => $this->comments,
            'likes' => $this->likes,
            'user' => $this->getUser(),
            'views' => $this->views,
            'category' => $this->category,
            'created_at' => $this->created_at->setTimeZone(new \DateTimeZone('Europe/Kiev'))->format("Y-m-d H:i:s")
        ];
    }

    public function setImageIdAttribute($value)
    {
        if (isset($value)) {
            $this->attributes['image_id'] = $value;
        }
    }

    public function comments()
    {
        return $this->hasMany(App\Modules\PieComments\Database\Models\Comments::class, 'article_id', 'id')->active();
    }

    public function getTranslateArticle($language = 1)
    {
        $language = $this->lang->where('language', $language)->first();
        if ($language && $language->status == Base::STATUS_ACTIVE) {
            return [
                'name' => $language->trans_name,
                'info' => $language->trans_info,
                'text' => $language->trans_text,
            ];
        }
        return [
            'name' => $this->name,
            'info' => $this->info,
            'text' => $this->text,
        ];
    }

    public function getTextAttribute()
    {
        $text = isset($this->attributes['text']) ? $this->attributes['text'] : '';
        return $text;
    }

    public function tags()
    {
        return $this->hasMany(Tag::class,'article_id','id');
    }

    private function convertHashtags($text = null)
    {
        $regex = "/#+([a-zA-Z_]+)/";
        if (!$text) {
            return $text;
        }
        $str = preg_replace($regex, '<a href="' . route('site:news') . '?filter=$1">$0</a>', $text);
        return $str;
    }

    public function likes()
    {
        return $this->hasMany(Like::class, "article_id", 'id')->active();
    }

    public function getLikeByUser(User $user)
    {
        if($user){
            $like = Like::query()->where('article_id',$this->id)->where("user_id",$user->id)->first();
            if($like){
                return $like->status == static::STATUS_ACTIVE;
            }
        }
        return false;
    }

    public function setDefault()
    {
        $this->id = 0;
        $this->name = '-';
        return $this;
    }
}