<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 20.07.2016
 * Time: 10:50
 */

namespace App\Modules\PieArticle\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieTranslator\Database\Models\CategoryTranslate;

class Category extends Base
{
    protected $table = 'categories';
    public $timestamps = false;
    protected $perPage = 15;
    const COLOR = '000000';
    protected $fillable = array(
        'color',
        'name',
        'status',
    );

    public function setColorAttribute($value)
    {
        $this->attributes['color'] = str_replace('#', '', $value);
    }

    public function getColorAttribute($value)
    {
        return '#' . $value;
    }

    public function setDefault()
    {
        $this->id = 0;
        $this->color = self::COLOR;
        $this->name = '-';
        return $this;
    }

    public function getColor()
    {
        return ($this->color != '#') ? $this->color : $this->setDefault()->color;
    }

    public function getSlugAttribute()
    {
        return $this->attributes['id'] . "_" . $this->transliterate($this->attributes['name']);
    }

    public function translate()
    {
        return $this->hasMany(CategoryTranslate::class, 'category_id','id');
    }

    public function getNameAttribute()
    {
        if($this->translate && $this->translate->first()){
            $lang = App\Modules\PieBase\Database\Models\Language::query()->where('name',App::getLocale())->first();
            $language = $this->translate->where('language',($lang) ? $lang->id : 1)->first();
            if($language){
                return $language->name;
            }
        }

        return $this->attributes['name'];

    }
}