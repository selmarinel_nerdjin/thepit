<?php
namespace App\Modules\PieArticle\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

class Tag extends Base
{
    public $timestamps = false;
    protected $table = 'tags';

    protected $fillable = array(
        'article_id',
        'tag'
    );
}