<?php

return [
    'menu_news' => 'Articles',
    'sub_menu_news' => 'News',
    'sub_menu_category' => 'Categories',
    'table_info' => 'Info',
    'table_cover' => 'Cover',
    'table_owner' => 'Creator',
    'table_category' => 'Category',
    'table_color' => 'Color',
    'table_text' => 'Text',
    'table_active' => 'Active/Disable',
    'table_creator' => 'Author',
    'select_cover' => 'Select cover',
];