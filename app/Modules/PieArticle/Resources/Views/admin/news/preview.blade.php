@extends('pie_base::admin.layouts.simple')
@section('css')
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/pit.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/page.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/uno.css')}}">
    <style>
        .category-color {
            background-color: {{$model->category->color}} ;
        }

        #template {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 2;
            display: flex;
            align-content: center;
        }
        #template div{
            position: absolute;
            transform: rotate(30deg);
            font-size: 25vh;
            text-align: center;
            width: 100%;
            top: 35vh;
            bottom: auto;
            font-family: "Courier New", Courier, monospace;
            font-weight: bolder;
            opacity: .4;
        }
    </style>
@endsection
@section('siteName',"Preview | ".$model->name)
@section('content')
    <div id="template">
        <div>
            TEMPLATE
        </div>
    </div>
    <?php $translate = $model->getTranslateArticle($lang); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page_title_main">{{$translate['name']}}</h1>
            </div>
            <div class="col-lg-offset-1 col-lg-10">
                <div class="page-header">
                    <h1 class="">{{$translate['info']}}</h1>
                    <span class="article_date" data-text="{{$model->created_at}}">
                        {{$model->created_at}}
                    </span>
                </div>
                <div class="col-xs-12 page-blocker">
                    <div class="page-image">
                        <img src="{{$model->getCover(650)}}" class="img-responsive">
                        <div class="">
                            <a href="{{route('site:news',['category'=>$model->category->id])}}"
                               class="tag text-left category-color">
                                {{$model->category->name}}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    {!! $translate['text'] !!}
                </div>
            </div>
        </div>
    </div>
@endsection