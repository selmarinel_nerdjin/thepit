@extends('pie_base::admin.layouts.edit')

@section('title_name'){{trans('pie_article::main.sub_menu_news')}}@endsection

@section('head')
    <link rel="stylesheet" href="{{$app['pie_article.assets']->getPath('css/article-edit.css')}}">
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('css/select/select2.min.css')}}">
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('js/lib/trumbowyg/ui/trumbowyg.min.css')}}">
@endsection

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label(trans('pie_base::main.table_name')) !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_info')) !!}
        {!! Form::text('info', $model->info, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_cover')) !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i
                            class="glyphicon glyphicon-plus"></i> {{trans('pie_article::main.select_cover')}}
                </div></span>
            {!! Form::file('cover',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{$model->getCover()}}" width="250" class="img-responsive">
        </div>
    </div>

    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_text')) !!}
        {!! Form::textarea('text', $model->text, ['class'=>'form-control','id'=>'edit']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_category')) !!}
        {!! Form::select('category_id', $categories,$model->category_id, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_creator')) !!}
        {!! Form::select('user_id', $users, ($model->user_id) ? $model->user_id : Request::user()->id, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label("Tags") !!}
        <select class="form-control" id="tags" multiple="" tabindex="-1" aria-hidden="true"
                name="tags[]">
            @if($model->tags->first())
                @foreach($model->tags as $tag)
                    <option value="{{$tag->tag}}" selected>{{$tag->tag}}</option>
                @endforeach
            @else
                <option value="ThePIT" selected>ThePIT</option>
            @endif
        </select>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            SEO
        </div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::label('@Keywords') !!}
                {!! Form::text('keywords', $model->keywords, ['class'=>'form-control','id'=>'keywords']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('@Description') !!}
                {!! Form::textarea('description', $model->description, ['class'=>'form-control','rows'=>3]) !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @yield('add_script')
    @yield('media_script')

    <script src="{{$app['pie_base.assets']->getPath('/js/lib/select/select2.full.js')}}"></script>
    {{--Trumbowyg--}}
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/trumbowyg.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/pasteimage/trumbowyg.pasteimage.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/base64/trumbowyg.base64.min.js')}}"></script>
    <link rel="stylesheet"
          href="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/colors/ui/trumbowyg.colors.min.css')}}">
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/colors/trumbowyg.colors.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/noembed/trumbowyg.noembed.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/upload/trumbowyg.upload.min.js')}}"></script>

    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/table/trumbowyg.table.min.js')}}"></script>
    <script>
        $(function () {


            $('#edit').trumbowyg({

                btnsDef: {
                    image: {
                        dropdown: ['insertImage', 'base64', 'noembed'],
                        ico: 'insertImage'
                    }
                },
                btns: [
                    ['viewHTML'],
                    ['undo', 'redo'],
                    ['formatting'],
                    'btnGrp-design',
                    ['link'],
                    ['image'],
                    'btnGrp-justify',
                    'btnGrp-lists',
                    ['table'],
                    ['foreColor', 'backColor'],
                    ['preformatted'],
                    ['horizontalRule'],
                    ['fullscreen']
                ]
            });

            $("#tags").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            });
        });

    </script>
@endsection