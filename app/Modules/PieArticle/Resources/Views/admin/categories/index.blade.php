@extends('pie_base::admin.layouts.all')

@section('page_title') {{trans('pie_article::main.sub_menu_category')}} @endsection

@section('tHead')
    <th>Id</th>
    <th>{{trans('pie_base::main.table_name')}}</th>
    <th>{{trans('pie_article::main.table_color')}}</th>
    <th>{{trans('pie_base::main.table_status')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->id}}</td>
            <td>{{$model->name}}</td>
            <td><span class="label" style="background: {{$model->color}}">&nbsp;</span> {{$model->color}}</td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-danger" href="{{route('admin:categories:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
                @if($model->translate->first())
                    @foreach($model->translate as $lang)
                        <a class="btn btn-raised btn-info" href="{{route('admin:category:translate:edit',['id'=>$lang->id])}}">
                            <i class="fa fa-language"></i> {{$lang->lang->name}}
                        </a>
                    @endforeach
                @endif
                @if($model->translate->count() < 3)
                    <a class="btn btn-raised btn-info" href="{{route('admin:category:translate:add:translate',['id'=>$model->id])}}">
                        <i class="fa fa-plus"></i> <i class="fa fa-language"></i>
                    </a>
                @endif
                <a class="btn btn-raised btn-success" href="{{route('admin:categories:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection