<?php
namespace App\Modules\PieArticle\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;
use App\Modules\PieArticle\Database\Models\Like as Model;

class Like extends Base
{
    protected $model;
    protected $modelName = Model::class;

    public function tick($userId, $articleId)
    {
        /**
         * @var App\Modules\PieArticle\Database\Models\Like $model
         */
        $model = $this->getModel()->query()->where("user_id", $userId)->where("article_id", $articleId)->first();
        if ($model) {
            if($model->status == Model::STATUS_ACTIVE){
                $model->fill(['status' => Model::STATUS_DELETED]);
            } else {
                $model->fill(['status' => Model::STATUS_ACTIVE]);
            }
            return ($model->save())?$model:null;
        }
        return $this->getModel()->create([
            "user_id" => $userId,
            "article_id" => $articleId,
            "status" => Model::STATUS_ACTIVE
        ]);
    }
}