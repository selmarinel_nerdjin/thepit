<?php
namespace App\Modules\PieArticle\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;

class Tags extends Base
{
    protected $model;
    protected $modelName = App\Modules\PieArticle\Database\Models\Tag::class;

    protected $hasStatus = false;

    public function getTags()
    {
        return $this->getModel()->query()->groupBy('tag')->get();
    }
    
}