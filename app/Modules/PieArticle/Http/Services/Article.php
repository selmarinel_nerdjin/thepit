<?php
namespace App\Modules\PieArticle\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;
use Illuminate\Http\Request;

class Article extends Base
{
    protected $model;
    protected $modelName = App\Modules\PieArticle\Database\Models\Articles::class;

    public function oneArticle($id)
    {
        $model = $this->getModel();
        $this->setWhere(['status' => $model::STATUS_ACTIVE]);
        return $this->getOne($id);
    }

    public function prepareSelect()
    {
        $articles = [];
        foreach ($this->getAll() as $article) {
            $articles[$article->id] = $article->name;
        }

        return $articles;
    }

    private function articleFilter(Array $parameters)
    {
        $query = $this->getModel()->query()->active();
        $query = ($parameters['category']) ? $query->where(['category_id' => $parameters['category']]) : $query;
        if (isset($parameters['tag']) && $parameters['tag']) {
            $tag = new App\Modules\PieArticle\Database\Models\Tag();
            $tags_id = $tag->query()->where('tag', $parameters['tag'])->get()->map(function ($model) {
                return $model->article_id;
            });
            if ($tags_id && !empty($tags_id)) {
                $query = $query->whereIn("id", $tags_id);
            }
        }

        if (isset($parameters['filter']) && $parameters['filter']) {
            $collection = $query->get();
            $ids = $collection->map(function ($model) {
                return $model->id;
            });
            if ($ids && !empty($ids)) {
                $translates = new App\Modules\PieTranslator\Http\Services\ArticleTranslator();
                $t_ids = $translates->getModel()->query()
                    ->whereIn('article_id', $ids)
                    ->where(function ($query) use ($parameters) {
                        return
                            $query->where('trans_name', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('trans_info', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('trans_text', 'LIKE', "%#{$parameters['filter']}%");
                    })
                    ->get()->map(function ($model) {
                        return $model->article_id;
                    })->toArray();
                if ($t_ids && !empty($t_ids)) {
                    $query = $query->where(function ($query) use ($t_ids, $parameters) {
                        return $query->whereIn('id', $t_ids)
                            ->orWhere(function ($query) use ($parameters) {
                                return
                                    $query->where('name', 'LIKE', "%{$parameters['filter']}%")
                                        ->orWhere('info', 'LIKE', "%{$parameters['filter']}%")
                                        ->orWhere('text', 'LIKE', "%#{$parameters['filter']}%");
                            });
                    });
                } else {
                    $query->where(function ($query) use ($parameters) {
                        return
                            $query->where('name', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('info', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('text', 'LIKE', "%#{$parameters['filter']}%");
                    });
                }
            }
        }
        return $query->createdBy();
    }

    public function paginateArticles(Request $request, $paginate = 12)
    {
        $parameters['category'] = $request->input('category');
        $parameters['filter'] = $request->input('filter');
        $parameters['tag'] = $request->input('tag');

        $language = new App\Modules\PieTranslator\Http\Services\Languages();
        $language = $language->getModel()->where('name', App::getLocale())->first();
        $collection = $this->articleFilter($parameters)
            ->paginate($paginate);
        return $collection->setCollection($collection->getCollection()->map(function($model) use ($language){
            return $model->getInfo(($language) ? $language->id : 1);
        }));

    }

    public function getPopular($take = 5)
    {
        return $this->getModel()->query()->active()->orderBy('views', 'desc')->take($take)->get();
    }

    public function getDiscussed($take = 5)
    {
        return $this->getModel()->query()
            ->with('comments')
            ->active()
            ->get()
            ->sortBy(function ($model) {
                return $model->comments->count();
            }, SORT_REGULAR, true)->take($take);
    }

    public function filterArticles(Request $request)
    {
        $filter = $request->input('filter');
        $query = $this->getModel()->query()->active();
        return $query->where('name', 'LIKE', "%{$filter}%")
            ->orWhere('info', 'LIKE', "%{$filter}%")->createdBy()->paginate(12);
    }

    public function getActual($inModel, $countNews = 6)
    {
        $count = 0;
        $ids = [];
        $moreViewedInCategory = $this->getModel()
            ->where('category_id', $inModel->category_id)
            ->where('id', '!=', $inModel->id)
            ->active()
            ->orderBy('views', 'desc')
            ->take($countNews % 4)
            ->get();

        if (!empty($moreViewedInCategory)) {
            foreach ($moreViewedInCategory as $model) {
                $ids[] = $model->id;
            }
            $count = $count + $moreViewedInCategory->count();

            $newInCategory = $this->getModel()
                ->where('category_id', $inModel->category_id)
                ->where('id', '!=', $inModel->id)
                ->whereNotIn('id', $ids)
                ->active()
                ->orderBy('created_at', 'desc')->take($countNews % 2)->get();
            if ($newInCategory) {
                $count = $count + $newInCategory->count();
            }
        }
        $other = $this->getModel()
            ->where('id', '!=', $inModel->id)
            ->active();

        if (isset($newInCategory) && !empty($newInCategory)) {
            foreach ($newInCategory as $model) {
                $ids[] = $model->id;
            }
            $other = $other->whereNotIn('id', $ids);
        }
        $other = $other
            ->orderBy('created_at', 'desc')
            ->take($countNews - $count)
            ->get();
        if ($other) {
            foreach ($other as $model) {
                $ids[] = $model->id;
            }
        }
        return $this
            ->getModel()
            ->whereIn('id', $ids)
            ->active()
            ->get()->map(function($model){
                return $model->getInfo();
            });
    }

    public function getRandomArticle($language = 1)
    {
        $result = App\Modules\PieArticle\Database\Models\Articles::all()->filter(function($model){
            return $model->status == App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE;
        })->random(1);
        $lang = $result->getTranslateArticle($language);
        $result = $result->getInfo();
        return array_merge($result,$lang);
    }
}