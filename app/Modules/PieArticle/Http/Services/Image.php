<?php
namespace App\Modules\PieArticle\Http\Services;

use App\Modules\PieBase\Http\Services\Files;

class Image extends Files
{
    protected $modelName = 'App\Modules\PieArticle\Database\Models\Cover';
    protected $orderBy = [];
    protected $hasStatus = false;
}