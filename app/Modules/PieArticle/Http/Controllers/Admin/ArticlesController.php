<?php
namespace App\Modules\PieArticle\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    protected $prefix = 'admin:news';
    protected $moduleName = 'pie_article';
    protected $serviceName = 'App\Modules\PieArticle\Http\Services\Article';
    const LAST_TIME = 604800;

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'users' => App::make('App\Modules\PieBase\Http\Services\User')->prepareSelect(),
            'categories' => App::make('App\Modules\PieArticle\Http\Services\Category')->prepareSelect(),
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'name' => 'required|min:2',
            'info' => 'required|min:3',
            'text' => 'required|min:5',
            'image_id' => '',
            'category_id' => 'required',
            'user_id' => 'required',
            'keywords' => '',
            'description' => '',
        ]);
        if ($request->method() != 'GET' && isset($request['cover'])) {
            $imageId = $this->saveFile($request);
            $request->merge(array('image_id' => $imageId));
        }
        $result = parent::add($request);
        if ($this->service->getModel() && $this->service->getModel()->id) {
            $this->saveTags($request, $this->service->getModel()->id);
        }
        return $result;
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'id' => 'required',
            'name' => 'required|min:2',
            'info' => 'required|min:3',
            'text' => 'required|min:5',
            'image_id' => '',
            'category_id' => 'required',
            'user_id' => 'required',
            'keywords' => '',
            'description' => '',
        ]);
        if ($request->input('tags')) {
            $this->saveTags($request, $id);
        }
        if ($request->hasFile('cover')) {
            $imageId = $this->saveFile($request);
            $request->merge(array('image_id' => $imageId));
        }
        return parent::edit($request, $id);
    }

    private function saveFile(Request $request)
    {
        $service = new App\Modules\PieArticle\Http\Services\Image($request->file('cover'));
        $service->saveFile($request->user()->id);
        return $service->getModel()->id;
    }

    private function saveTags(Request $request, $id)
    {
        if ($request->input('tags')) {
            App\Modules\PieArticle\Database\Models\Tag::query()->where('article_id', $id)->delete();
            foreach ($request->input('tags') as $tag) {
                App\Modules\PieArticle\Database\Models\Tag::create([
                    "article_id" => $id,
                    "tag" => $tag
                ]);
            }
        }
    }

    public function article(Request $request, $id)
    {
        $model = $this->service->getOne($id);
        if ($this->service->isErrors()) {
            abort(404);
        }
        $language = new App\Modules\PieTranslator\Http\Services\Languages();
        $lang = $language->getModel()->where('name', App::getLocale())->first();

        return view($this->getViewRoute('preview'), ['model' => $model, 'lang' => ($lang) ? $lang->id : 1,]);
    }

}