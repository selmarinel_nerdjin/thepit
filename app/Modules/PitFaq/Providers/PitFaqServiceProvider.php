<?php
namespace App\Modules\PitFaq\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PitFaqServiceProvider extends ServiceProvider
{
	/**
	 * Register the PitFaq module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PitFaq\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PitFaq module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pit_faq', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pit_faq', base_path('resources/views/vendor/pit_faq'));
		View::addNamespace('pit_faq', realpath(__DIR__.'/../Resources/Views'));
	}
}
