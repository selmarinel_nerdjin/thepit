@extends('pie_base::admin.layouts.edit')
@section('title_name', 'FAQ Question')
@section('head')
    <link rel="stylesheet" href="{{$app['pit_faq.assets']->getPath('js/ed/jquery.cleditor.css')}}">
@endsection
@section('form_body')
    <div class="form-group">
        {!! Form::label('Description') !!}
        {!! Form::textarea('description', $model->description, ['class'=>'form-control','required','rows'=>3] ) !!}
    </div>
@endsection

@section('scripts')
    <script src="{{$app['pit_faq.assets']->getPath('js/ed/jquery.cleditor.min.js')}}"></script>
    <script>
        $('button').click(function () {
            var self = $(this);
            $("#icon").val(self.attr("data-text"));
        })
    </script>
    <script>
        $(document).ready(function () {
            $("textarea").cleditor({
                controls: // controls to add to the toolbar
                "bold italic underline strikethrough subscript superscript | size " +
                "style | color removeformat | bullets numbering | outdent " +
                "indent | alignleft center alignright justify | undo redo | " +
                "rule image link unlink | cut copy paste pastetext | source",
                colors: // colors in the color popup
                "1abc9c 16a085 f1c40f f39c12 2ecc71 27ae60 e67e22 d35400 3498db 2980b9 " +
                "e74c3c c0392b 9b59b6 8e44ad ecf0f1 bdc3c7 34495e 2c3e50 95a5a6 7f8c8d " +
                "1a1a1a 2b2b2b 333333 3c3c3c 4d4d4d 5e5e5e 6f6f6f 787878 9a9a9a ffffff",
                useCSS: true
            });
        });
    </script>
@endsection