@extends('pie_base::admin.layouts.all')
@section('content')
    <div class="container">
        <h5>{{$search}}</h5>
        <form class="form-group" method="post" action="">
            <input type="text" name="query" class="form-control" placeholder="Input query">
            <button type="submit" class="btn btn-default">search</button>
        </form>
    </div>
    <div class="container">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
            @foreach($collection as $id=>$category)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{$category->first()->category->id}}">
                        <button data-toggle="collapse" data-target="#category{{$category->first()->category->id}}"
                           class="btn btn-default">
                            <i class="fa {{$category->first()->category->icon}}"></i>
                            {{$category->first()->category->name}}
                        </button>
                    </div>
                    <div class="panel-collapse collapse @if($search) in @endif"
                         id="category{{$category->first()->category->id}}">
                        @foreach($category as $model)
                            <div class="panel-body">
                                <div class="col-xs-11">
                                    <h4>
                                        {{$model->title}}
                                        <small>
                                            {!! $model->description !!}
                                        </small>
                                    </h4>
                                </div>
                                <div class="col-xs-1">
                                    <button class="btn btn-link" data-target="#answer{{$model->id}}"
                                            aria-controls="answer{{$model->id}}" type="button" data-toggle="collapse"
                                            aria-expanded="true">
                                        <i class="fa fa-arrow-circle-right" style="font-size: 2em"></i>
                                    </button>
                                </div>
                                <div class="col-xs-12">
                                    <div class="collapse @if($search) in @endif" id="answer{{$model->id}}">
                                        @foreach($model->answers as $answer)
                                            <div class="well">
                                                {!! $answer->description !!}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection