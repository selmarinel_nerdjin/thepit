@extends('pie_base::admin.layouts.edit')

@section('title_name', 'Категории')

@section('form_body')
    <div class="form-group">
        {!! Form::label('Name') !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control','required'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Icon') !!}
        {!! Form::text('icon', $model->icon, ['class'=>'form-control','required','id'=>'icon','readonly'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Order') !!}
        {!! Form::number("order",($model->order) ? : $model->getMaxOrder(),['class'=>'form-control','required']) !!}
    </div>
    <div class="form-group">
        <div class="btn-group btn-group-sm" role="group">
            @foreach($model->getIconsList() as $icon)
                <button type="button" class="btn btn-default" data-text="{{$icon}}" style="font-size: 2em">
                    <i class="fa {{$icon}}"></i>
                </button>
            @endforeach
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('button').click(function () {
            var self = $(this);
            $("#icon").val(self.attr("data-text"));
        })

    </script>
@endsection