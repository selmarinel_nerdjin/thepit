@extends('pie_base::admin.layouts.all')

@section('page_title', 'FAQ questions')

@section('tHead')
    <th>Title</th>
    <th>Short info</th>
    <th>Category</th>
    <th>Priority</th>
    <th>Status</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->title}}</td>
            <td>{{$model->short}}</td>
            <td>
                <i class="fa {{$model->tag->icon}}"></i> {{$model->tag->name}}
            </td>
            <td>{{$model->order}}</td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-raised btn-success" href="{{$getRoute('edit', ['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn {{($model->status)?"btn-raised btn-warning":"btn-raised btn-info"}}"
                   href="{{$getRoute('active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                @if($model->status != $model::STATUS_ACTIVE)
                    <a class="btn btn-danger" href="{{$getRoute('delete', ['id'=>$model->id])}}">
                        <i class="fa fa-trash"></i>
                    </a>
                @endif
            </td>
        </tr>
    @endforeach
@endsection


