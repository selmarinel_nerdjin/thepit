<script type="text/x-template" id="faq-template">
    <element class="faqer"
             :class="collapsed ? 'faqer-collapsed' : ''">
        <div class="header">
            <div class="pull-left">
                FAQer
            </div>
            <div class="pull-right">
                <b class="close" v-on:click="changeState()">
                    _
                </b>
            </div>
        </div>
        <div class="body">
            <div class="category" v-for="category in collection_data">
                <div class="title">
                    @{{category.category }}
                </div>
                <div class="questions_list">
                    <div v-for="question in category.data" class="question-answer-block"
                         v-on:click="openQuestion(question)">
                        <div class="question" v-html="question.question">
                        </div>
                        <div class="extended" v-html="question.extended">
                        </div>
                        <div class="answer" v-html="question.answer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="asker">
            <input type="text" v-model="query">
            <input type="button" value="search" v-on:click="ajaxData()">
        </div>
    </element>
</script>
<script>
    Vue.component('faqer', {
        template: '#faq-template',
        props: {
            collection: Array,
            collection_data: Array
        },
        data: function () {
            return {
                collapsed: true,
                query: ""
            }
        },
        computed: {
            filteredData: function () {
                var query = this.query && this.query.toLowerCase();
                var data = this.collection;
                if (query) {
                    data = data.filter(function (row) {
                        return Object.keys(row).some(function (key) {
                            return String(row[key]).toLowerCase().indexOf(query) > -1
                        })
                    })
                }
                return data
            }

        },
        filters: {
            capitalize: function (str) {
                return str.charAt(0).toUpperCase() + str.slice(1)
            }
        },
        methods: {
            ajaxData: function () {
                var self = this;
                $.ajax({
                    url: "{{route("site:api:faq")}}",
                    type: "post",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        query: self.query
                    }
                }).done(function(response){
                   self.collection_data = response.collection;
                });
            },
            changeState: function () {
                this.collapsed = !this.collapsed;
            },
            openQuestion: function (question) {
                $.alert({
                    title: question.question,
                    content: "<h3>" + question.extended + "</h3>" + question.answer,
                    type: 'orange',
                    boxWidth: '30%',
                    useBootstrap: false,
                    backgroundDismiss: true,
                    escapeKey: true,
                    titleClass: "big_title"
                })
            }
        }
    });

    // bootstrap the demo
    var fullData = [];
    $.ajax({
        url: "{{route("site:api:faq")}}",
        type: "post",
        dataType: "json",
        data: {
            "_token": "{{ csrf_token() }}"
        }
    }).done(function (response) {
        var demo = new Vue({
            el: '#demo',
            data: {
                gridData: [
                    {name: 'Chuck Norris', power: Infinity},
                    {name: 'Bruce Lee', power: 9000},
                    {name: 'Jackie Chan', power: 7000},
                    {name: 'Jet Li', power: 8000}
                ],
                fullData: response.collection
            }
        });
    });

</script>
