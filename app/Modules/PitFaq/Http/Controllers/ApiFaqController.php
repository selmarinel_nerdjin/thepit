<?php

namespace App\Modules\PitFaq\Http\Controllers;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ApiFaqController extends Controller
{

    public function getFaqList(Request $request)
    {
        $faq = new App\Modules\PitFaq\Http\Services\Faq\Question();

        return $this->sendOk(["collection" => $faq->getApiCollection($request->input('query'))]);
    }


}