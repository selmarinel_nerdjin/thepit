<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 22:59
 */

namespace App\Modules\PitFaq\Http\Controllers\Admin\Faq;

use App;
use App\Modules\PieBase\Http\Controllers\Controller;
use App\Modules\PitFaq\Http\Services\Faq\Question;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    protected $prefix = 'admin:faq';
    protected $moduleName = 'pit_faq';
    protected $serviceName = Question::class;

    public function search(Request $request)
    {
        $query = $request->input('query');
        $response = $this->service->getFilter($query);
        return $this->sendOk(['data' => $response]);
    }

    public function getInformation(Request $request)
    {
        $this->service->setWith(['category']);
        $collection = $this->service->getAll();
        return $this->sendOk(['collection' => $collection]);
    }

    public function faqAction(Request $request)
    {
        $query = $request->input('query');
        return view($this->getViewRoute(), [
            'collection' => $this->service->filter($query),
            'search' => $query
        ]);
    }
}