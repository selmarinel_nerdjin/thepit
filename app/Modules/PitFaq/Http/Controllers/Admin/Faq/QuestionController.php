<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 14:06
 */

namespace App\Modules\PitFaq\Http\Controllers\Admin\Faq;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use App\Modules\PitFaq\Http\Services\Faq\Category;
use App\Modules\PitFaq\Http\Services\Faq\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    protected $prefix = 'admin:faq:question';
    protected $moduleName = 'pit_faq';
    protected $serviceName = Question::class;

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'categories' => App::make(Category::class)->prepareSelect(),
        ];
    }

    protected function saveAnswer(Request $request, $id = null)
    {
        if ($request->input('answers') && !empty($request->input('answers'))) {
            $id = ($id) ?: $request->input('id');
            $answerService = new App\Modules\PitFaq\Http\Services\Faq\Answer();
            foreach ($request->input('answers') as $key => $answer) {
                $answerService->saveAnswer($id, $answer, $request->input('answers_id')[$key]);
            }
        }
    }

    public function add(Request $request)
    {
        $this->setRules([
            'title' => 'required|min:2',
            'description' => 'required|min:3',
            'order' => 'required',
            'category_id' => 'required|numeric|exists:faq_categories,id',
        ]);
        $result = parent::add($request);
        if ($request->method() == 'POST') {
            $this->saveAnswer($request, $this->service->getModel()->id);
        }
        return $result;
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'id' => 'required',
            'order' => 'required',
            'title' => 'required|min:2',
            'description' => 'required|min:3',
            'category_id' => 'required|numeric|exists:faq_categories,id',
        ]);
        if ($request->method() == 'POST') {
            $this->saveAnswer($request);
        }
        return parent::edit($request, $id);
    }
}