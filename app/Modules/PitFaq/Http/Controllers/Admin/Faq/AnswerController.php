<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 14:06
 */

namespace App\Modules\PitFaq\Http\Controllers\Admin\Faq;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use App\Modules\PitFaq\Http\Services\Faq\Answer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    protected $prefix = 'admin:faq:answer';
    protected $moduleName = 'pit_faq';
    protected $serviceName = Answer::class;

    public function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'description' => 'required|min:3',
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'id' => 'required',
            'description' => 'required|min:3',
        ]);
        return parent::edit($request, $id);
    }

    public function activate(Request $request, $id)
    {
        $this->service->getOne($id);
        $this->service->setStatus();
        return $this->sendOk(['message' => 'Answer hide']);
    }
}