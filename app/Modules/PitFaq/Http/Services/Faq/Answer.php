<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 14:09
 */

namespace App\Modules\PitFaq\Http\Services\Faq;

use App\Modules\PitFaq\Database\Models\Faq\Answer as Model;
use App\Modules\PieBase\Http\Services\Base;

class Answer extends Base
{
    protected $modelName = Model::class;

    public function eraseOldData($question_id)
    {
        $this->getModel()->query()->where('question_id', $question_id)->update(['status' => Model::STATUS_NOT_CHK]);
    }

    public function saveAnswer($question_id, $data, $answer_id)
    {
        $model = $this->getModel();
        if($answer_id){
            $model = $this->getOne($answer_id);
        }
        $model->fill([
            'question_id' => $question_id,
            'description' => $data
        ]);
        return $model->save();
    }
}