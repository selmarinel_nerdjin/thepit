<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 14:09
 */

namespace App\Modules\PitFaq\Http\Services\Faq;

use App\Modules\PitFaq\Database\Models\Faq\Category as Model;
use App\Modules\PieBase\Http\Services\Base;

class Category extends Base
{
    protected $modelName = Model::class;

    public function prepareSelect()
    {
        $categories = [];
        $this->setWhere(['status'=>\App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE]);
        foreach($this->getAll() as $category){
            $categories[$category->id] = $category->name;
        }

        return $categories;
    }
}