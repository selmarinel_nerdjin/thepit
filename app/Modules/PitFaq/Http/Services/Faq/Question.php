<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 14:09
 */

namespace App\Modules\PitFaq\Http\Services\Faq;

use App\Modules\PitFaq\Database\Models\Faq\Question as Model;
use App\Modules\PieBase\Http\Services\Base;
use Illuminate\Database\Eloquent\Collection;

class Question extends Base
{
    protected $modelName = Model::class;

    public function getFilter($query)
    {
        $query = trim(preg_replace('|\s+|', ' ', $query));
        $words = explode(' ', $query);
        $newQuery = $this->getModel()->query();
        $newQuery = $newQuery->where('title', 'like', "%$words[0]%")
            ->orWhere('description', 'like', "%$words[0]%");
        foreach ($words as $word) {
            $newQuery = $newQuery->orwhere('title', 'like', "%$word%")
                ->orwhere('description', 'like', "%$word%");
        }
        $collection = $newQuery->active()->get();
        $counts = [];
        foreach ($collection as $item) {
            $counts[$item->id] = 0;
            foreach ($words as $word) {
                if ($word) {
                    $counts[$item->id] += substr_count($item->title, $word) + substr_count($item->description, $word);
                }
                $item->counts = $counts[$item->id];
            }
        }
        $result = new Collection();
        foreach ($collection->sortByDesc('counts') as $item) {
            $result->push($item);
        }
        return $result->sortBy("order");
    }

    /**
     * @param string $query
     * @return static
     */
    public function filter($query = '')
    {
        $this->setWith(['category']);
        $collection = $this->getFilter($query)->groupBy('category.id');
        return $collection->sortBy(function ($model, $key) {
            return $model->first()->category->order;
        });
    }

    public function getApiCollection($query)
    {
        $query = trim(preg_replace('|\s+|', ' ', $query));
        $words = explode(' ', $query);
        $newQuery = $this->getModel()->query()
            ->where("title", 'like', "%$query%")
            ->orWhere("description", "like", "%$query%");
        foreach ($words as $word) {
            $newQuery = $newQuery->orwhere('title', 'like', "%$word%")
                ->orwhere('description', 'like', "%$word%");
        }
        $collection = $newQuery->with(["category", "answers"])->active()->orderBy("order")->get();
        $result = [];
        foreach ($collection as $model) {
            $result[$model->category_id]["category"] = $model->category->name;
            if (!isset($result[$model->category_id]["data"])) {
                $result[$model->category_id]["data"] = [];
            }
            array_push($result[$model->category_id]["data"], [
                "question" => $model->title,
                "extended" => $model->description,
                "answer" => $model->answers->first()->description
            ]);
        }
        return $result;
    }
}