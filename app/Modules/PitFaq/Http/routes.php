<?php

Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function () {
    Route::group(['middleware' => 'AdminAuth'], function () {
        Route::group(['prefix' => 'faq', 'middleware' => 'AdminAuthenticate'], function () {
            $asPrefix = ":faq";
            Route::any('/', ['as' => $asPrefix . ':index','uses' => 'Admin\Faq\FaqController@faqAction']);
            Route::group(['prefix' => 'category'], function () use ($asPrefix) {
                $asAction = ':category';
                Route::get('/', ['as' => $asPrefix . $asAction . ':index', 'uses' => 'Admin\Faq\CategoryController@index']);
                Route::any('/add', ['as' => $asPrefix . $asAction . ':add', 'uses' => 'Admin\Faq\CategoryController@add']);
                Route::any('/edit/{id}', ['as' => $asPrefix . $asAction . ':edit', 'uses' => 'Admin\Faq\CategoryController@edit']);
                Route::get('/{id}/active', ['as' => $asPrefix . $asAction . ':active', 'uses' => 'Admin\Faq\CategoryController@active']);
                Route::any('{id}/delete', ['as' => $asPrefix . $asAction . ':delete', 'uses' => 'Admin\Faq\CategoryController@delete']);
            });
            Route::group(['prefix' => 'question'], function () use ($asPrefix) {
                $asAction = ':question';
                Route::get('/', ['as' => $asPrefix . $asAction . ':index', 'uses' => 'Admin\Faq\QuestionController@index']);
                Route::any('/add', ['as' => $asPrefix . $asAction . ':add', 'uses' => 'Admin\Faq\QuestionController@add']);
                Route::any('/edit/{id}', ['as' => $asPrefix . $asAction . ':edit', 'uses' => 'Admin\Faq\QuestionController@edit']);
                Route::get('/{id}/active', ['as' => $asPrefix . $asAction . ':active', 'uses' => 'Admin\Faq\QuestionController@active']);
                Route::any('{id}/delete', ['as' => $asPrefix . $asAction . ':delete', 'uses' => 'Admin\Faq\QuestionController@delete']);
            });
            Route::group(['prefix' => 'answer'], function () use ($asPrefix) {
                $asAction = ':answer';
                Route::get('/', ['as' => $asPrefix . $asAction . ':index', 'uses' => 'Admin\Faq\AnswerController@index']);
                Route::any('/add', ['as' => $asPrefix . $asAction . ':add', 'uses' => 'Admin\Faq\AnswerController@add']);
                Route::any('/edit/{id}', ['as' => $asPrefix . $asAction . ':edit', 'uses' => 'Admin\Faq\AnswerController@edit']);
                Route::get('/{id}/active', ['as' => $asPrefix . $asAction . ':active', 'uses' => 'Admin\Faq\AnswerController@active']);
                Route::post('/{id}/activate', ['as' => $asPrefix . $asAction . ':activate', 'uses' => 'Admin\Faq\AnswerController@activate']);
                Route::any('{id}/delete', ['as' => $asPrefix . $asAction . ':delete', 'uses' => 'Admin\Faq\AnswerController@delete']);
            });
        });
    });
});
Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
    Route::group(['prefix' => '/api', 'middleware' => 'webAdmin'], function () {
        $asPrefix = "site";
        Route::group(['as' => $asPrefix . ':api', 'middleware' => ['ware']], function () use ($asPrefix) {
            Route::post("/faq",['as' => ":faq", 'uses' => "ApiFaqController@getFaqList"]);
        });
    });
});