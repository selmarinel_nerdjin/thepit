<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstallFaq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150);
            $table->string('icon',150);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::create('faq_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->integer('category_id')->references('id')->on('faq_categories');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::create('faq_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('question_id')->references('id')->on('faq_questions');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_answers');
        Schema::dropIfExists('faq_questions');
        Schema::dropIfExists('faq_categories');
    }
}
