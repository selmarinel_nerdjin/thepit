<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 13:56
 */

namespace App\Modules\PitFaq\Database\Models\Faq;


use App\Modules\PieBase\Database\Models\Base;

class Category extends Base
{
    protected $table = 'faq_categories';

    protected $fillable = [
        'name',
        'icon',
        'order',
        'status'
    ];

    const ICON_LIST = [
        1 => 'fa-archive',
        2 => 'fa-area-chart',
        3 => 'fa-car',
        4 => 'fa-barcode',
        5 => 'fa-bell',
        6 => 'fa-beer',
        7 => 'fa-bicycle',
        8 => 'fa-book',
        9 => 'fa-calendar',
        10 => 'fa-camera',
        11 => 'fa-child',
        12 => 'fa-clock-o',
        13 => 'fa-coffee',
        14 => 'fa-credit-card',
        15 => 'fa-eye',
        16 => 'fa-female',
        17 => 'fa-fire',
        18 => 'fa-gamepad',
        19 => 'fa-users',
        20 => 'fa-heart',
        22 => 'fa-info',
        23 => 'fa-location-arrow',
        25 => 'fa-map-marker',
        26 => 'fa-money',
        27 => 'fa-music',
        28 => 'fa-tags',
        29 => 'fa-tasks',
        30 => 'fa-taxi',
        31 => 'fa-thumbs-up',
        32 => 'fa-trash-o',
        33 => 'fa-wifi',
        34 => 'fa-wrench',
        35 => 'fa-wheelchair',
        36 => 'fa-ambulance',
    ];

    public function defaultCategory()
    {
        $this->name = 'default';
        $this->icon = 'fa-info';
        return $this;
    }

    public function getIconsList()
    {
        return self::ICON_LIST;
    }

    public function getMaxOrder()
    {
        return $this->newQuery()->max("order") + 1;
    }
}