<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 13:57
 */

namespace App\Modules\PitFaq\Database\Models\Faq;

use App;
use App\Modules\PieBase\Database\Models\Base;

class Question extends Base
{
    protected $fillable =[
        'title',
        'description',
        'category_id',
        'status',
        'order'
    ];

    protected $table = 'faq_questions';
    
    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id');
    }
    
    public function getTagAttribute()
    {
        if($this->category){
            return $this->category;
        }
        return App::make(Category::class)->defaultCategory();
    }

    public function getShortAttribute()
    {
        return mb_substr(strip_tags($this->attributes['description']),0,25);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class,'question_id','id')->active();
    }

    public function getMaxOrder()
    {
        return $this->newQuery()->max("order") + 1;
    }
}