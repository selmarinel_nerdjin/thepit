<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 13:57
 */

namespace App\Modules\PitFaq\Database\Models\Faq;


use App\Modules\PieBase\Database\Models\Base;

class Answer extends Base
{
    protected $fillable =[
        'description',
        'question_id',
        'status'
    ];
    
    protected $table = 'faq_answers';

    public function getShortAttribute()
    {
        return mb_substr(strip_tags($this->attributes['description']),0,25);
    }

    public function question()
    {
        return $this->hasOne(Question::class,'id','question_id');
    }
}