<?php
namespace App\Modules\PieComments\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieCommentsServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieComments module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieComments\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieComments module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_comments', realpath(__DIR__.'/../Resources/Lang'));
		View::addNamespace('pie_comments', base_path('resources/views/vendor/pie_comments'));
		View::addNamespace('pie_comments', realpath(__DIR__.'/../Resources/Views'));
	}
}
