<?php
namespace App\Modules\PieComments\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller {
    protected $prefix = 'admin:comments';
    protected $moduleName = 'pie_comments';
    protected $serviceName = App\Modules\PieComments\Http\Services\Comments::class;

    protected function prepareData($model = null){
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
        ];
    }
}