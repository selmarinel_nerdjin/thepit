<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'pie_comments'], function() {
	Route::get('/', function() {
		dd('This is the PieComments module index page.');
	});
});

Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function() {
	Route::group(['middleware' => 'AdminAuthenticate'], function () {
		Route::group(['prefix' => 'comments'], function () {
			$asAction = ':comments';
			Route::get('/', ['as' => $asAction . ':index', 'uses' => 'Admin\CommentsController@index']);
			Route::get('/{id}/active', ['as' => $asAction . ':active', 'uses' => 'Admin\CommentsController@active']);
			Route::any('{id}/delete', ['as' => $asAction . ':delete', 'uses' => 'Admin\CommentsController@delete']);
		});
	});
});