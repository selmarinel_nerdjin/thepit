<?php
namespace App\Modules\PieComments\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;

class Comments extends Base
{
    protected $model;
    protected $modelName = App\Modules\PieComments\Database\Models\Comments::class;

    protected $orderBy = ['created_at' => 'desc'];
    
    public function oneArticle($id)
    {
        $model = $this->getModel();
        $this->setWhere(['status' => $model::STATUS_ACTIVE]);
        return $this->getOne($id);
    }

    public function getNewest($take = 5)
    {
        return $this->getModel()->query()->active()->orderBy('created_at', 'desc')->take($take)->get();
    }


}