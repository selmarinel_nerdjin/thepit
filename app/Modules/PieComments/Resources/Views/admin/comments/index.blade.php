@extends('pie_base::admin.layouts.all')

@section('page_title') Comments @endsection

@section('tHead')
    <th>Comments text</th>
    <th>{{trans('pie_base::main.table_status')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->role_id == $model::STATUS_DELETED) ? "warning" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->short}}</td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn {{($model->status)?"btn-raised btn-warning":"btn-raised btn-info"}}"
                   href="{{route('admin:comments:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                <a class="btn btn-danger" href="{{route('admin:comments:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection