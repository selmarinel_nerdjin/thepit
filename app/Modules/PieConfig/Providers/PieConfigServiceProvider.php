<?php
namespace App\Modules\PieConfig\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieConfigServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieConfig module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieConfig\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieConfig module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_config', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_config', base_path('resources/views/vendor/pie_config'));
		View::addNamespace('pie_config', realpath(__DIR__.'/../Resources/Views'));
	}
}
