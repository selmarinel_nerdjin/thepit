<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
    Route::group(['prefix' => 'admin', 'middleware' => ['webAdmin']], function () {
        $asPrefix = 'admin:';
        Route::group(['prefix' => 'config', 'middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
            $asAction = 'config:';
            Route::get('/', ['as' => $asPrefix . $asAction . 'index', 'uses' => 'Admin\ConfigController@index']);
            Route::any('/edit/{id}', ['as' => $asPrefix . $asAction . 'edit', 'uses' => 'Admin\ConfigController@edit']);
        });
    });
});