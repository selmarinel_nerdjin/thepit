<?php

namespace App\Modules\ThePit\Http\Services;

use App\Modules\PieBase\Http\Services\Base;
use App\Modules\ThePit\Database\Models\Quote;

class QuoteService extends Base
{

    protected $modelName = Quote::class;

    /**
     * @return string
     */
    public function getRandom()
    {
        $count = $this->getModel()->query()->active()->get()->count();
        if(!$count){
            return "";
        }
        $rand = rand(0,$count);
        $model = $this->getModel()->query()->active()->offset($rand)->take(1)->get();
        if($model && $model->first() && $model->first()->id)
        {
            return $model->first()->quote;
        }
        return $this->getRandom();

    }

    public function save($data)
    {
        $model = $this->getModel();
        $model->fill($data);
        return $model->save();
    }
}