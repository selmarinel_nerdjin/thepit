<?php

namespace App\Modules\ThePit\Http\Controllers;

use App;
use App\Modules\PieArticle\Http\Services\Article;
use App\Modules\PieArticle\Http\Services\Category;
use App\Modules\PieMedia\Http\Services\Media;
use App\Modules\PieTranslator\Http\Services\Languages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use App\Modules\PieBase\Http\Services\User as User;
use App\Modules\PieStatic\Http\Services\StaticPie;
use Illuminate\Support\Facades\Validator;
use VK\VK;

class ThePitController extends BasicController
{
    protected $prefix = 'main';
    protected $moduleName = 'the_pit';


    /**
     * -----------------------API------------------------------
     */

    /**
     * @param Request $request
     * @return App\Modules\PieBase\Http\Requests\Response
     */
    public function newsApi(Request $request)
    {
        $articles = new Article();
        $request->session()->forget(['tag', 'category']);
        return $this->sendOk([
            'data' => $articles->paginateArticles($request, $this->config->findBySlug('paginate_count'))
        ]);
    }

    public function mediaApi(Request $request)
    {
        $media = new Media();
        return $this->sendOk([
            'data' => $media->paginateMedia($this->config->findBySlug('paginate_media')),
        ]);
    }

    public function quoteApi(Request $request)
    {
        $quote = new App\Modules\ThePit\Http\Services\QuoteService();
        return $this->sendOk([
            'data' => $quote->getRandom()
        ]);
    }

    public function quoteAppendApi(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['quote' => 'required|min:5']
        );
        if ($validator->fails()) {
            return $this->sendWithErrors($validator->messages());
        }
        $quote = new App\Modules\ThePit\Http\Services\QuoteService();
        $quote->save([
            'quote' => $request->input('quote')
        ]);
        return $this->sendOk(['success']);

    }

    public function newsActualApi(Request $request, $id)
    {
        $articles = new Article();
        $model = $articles->oneArticle($id);
        if (!$model) {
            return $this->sendWithErrors('Article not found', 404);
        }
        return $this->sendOk([
            'data' => $articles->getActual($model)
        ]);
    }


    public function addCommentAction(Request $request, $id)
    {
        $this->setRules([
            'comment_text' => 'required',
        ]);
        $message = $request->input('comment_text');
        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        if (!$message) {
            return $this->sendWithErrors('Check your text for html tags');
        }
        $comment = new App\Modules\PieComments\Http\Services\Comments();
        $comment->getModel()->fill(
            [
                'user_id' => $request->user()->id,
                'article_id' => $id,
                'comment_text' => $message,
            ]
        );
        $comment->getModel()->save();
        return $this->sendOk(['message' => 'Success', 'data' => $comment->getModel()->getInfo()]);
    }

    public function loadCommentAction(Request $request, $id)
    {
        $page = $request->input('page');
        $offset = $request->input('offset');
        $comment = new App\Modules\PieComments\Http\Services\Comments();
        $comment->setWhere(['article_id' => $id, 'status' => App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE]);
        $count = $comment->getAll()->count();
        $collection = $comment->getAll($this->config->findBySlug('comments_count'), $offset);
        return $this->sendOk(['collection' => $collection->map(function ($model) {
            return $model->getInfo();
        }),
            'count' => $count,
            'limit' => $this->config->findBySlug('comments_count'),
            'page' => $page + 1
        ]);
    }

    /**
     * --------------------API------------------------
     */


    /**
     * @param Request $request
     * @return mixed
     */
    public function ruAction(Request $request)
    {
        Session::put('locale', 'ru');
        App::setLocale('ru');
        return redirect()->to('/');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function uaAction(Request $request)
    {
        Session::put('locale', 'ua');
        App::setLocale('ua');
        return redirect()->to('/');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function enAction(Request $request)
    {
        Session::put('locale', 'en');
        App::setLocale('en');
        return redirect()->to('/');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function indexAction(Request $request)
    {
        if(Session::get('locale')){
            return view($this->getViewRoute('index'), [
                'menu' => $this->getMenu()
            ]);
        }
        return view($this->getViewRoute('localize'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function infoAction(Request $request)
    {
        return view($this->getViewRoute('info'), ['menu' => $this->getMenu()]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function contactsAction(Request $request)
    {
        return view($this->getViewRoute('contacts'), ['menu' => $this->getMenu()]);
    }

    public function categoryRedirect(Request $request, $category = null)
    {
        $request->session()->put('category', $category);
        return redirect()->route('site:news');
    }

    public function tagRedirect(Request $request, $tag = null)
    {
        $request->session()->put('tag', $tag);
        return redirect()->route('site:news');
    }


    /**
     * @param Request $request
     * @param null $category
     * @return mixed
     */
    public function newsAction(Request $request, $category = null)
    {
        $categories = new Category();
        $tags = new App\Modules\PieArticle\Http\Services\Tags();
        return view($this->getViewRoute('news'), [
            'categories' => $categories->getAll(),
            'tags' => $tags->getTags(),
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function oneArticleAction(Request $request, $id)
    {
        $article = new Article();
        $language = new Languages();
        $lang = $language->getModel()->where('name', App::getLocale())->first();
        $model = $article->oneArticle($id);
        if (!$model) {
            return abort(404);
        }
        $language = $model->getTranslateArticle(($lang) ? $lang->id : 1);

        $model->name = $language['name'];
        $model->info = $language['info'];
        $model->text = $language['text'];

        $model->updateViews();
        return view($this->getViewRoute('one'), [
            'model' => $model,
            'popular' => $article->getActual($model, $this->config->findBySlug('actual_news')),
            'lang' => ($lang) ? $lang->id : 1,
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function mediaAction(Request $request)
    {
        $media = new Media();
        return view($this->getViewRoute('media'), [
            'collection' => $media->paginateMedia($this->config->findBySlug('paginate_media')),
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);

    }

    /**
     * @param $s
     * @return string
     */
    protected function translit($s)
    {
        $s = (string)$s;
        $s = strip_tags($s);
        $s = str_replace(array("\n", "\r"), " ", $s);
        $s = preg_replace("/\s+/", ' ', $s);
        $s = trim($s);
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
        $s = strtr($s, [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'j',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shch',
            'ы' => 'y',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            'ъ' => '',
            'ь' => ''
        ]);
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
        $s = str_replace(" ", "-", $s);
        return $s;
    }

    public function logoutAction(Request $request)
    {
        try {
            $request->session()->forget('token');
        } catch (\Exception $exception) {
        }
        return redirect()->to("/news");
    }

    public function facebookAuth()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback(Request $request)
    {
        try {
            $socialize = Socialite::driver('facebook')->user();
            if (!$socialize) {
                return abort(401);
            }
            $user['email'] = $socialize->email;
            $user['name'] = $socialize->name;
            $user['avatar'] = $socialize->avatar;
            return $this->authSocialUser($request,$user);
        } catch (\Exception $exception) {
            return abort(404);
        }
    }

    private function authSocialUser(Request $request, $user)
    {
        if (!empty($user)) {
            $service = new User();
            $userModel = $this->checkUser($service, $user['email'], $user['name'], $user['avatar']);
            $service->auth(['login' => $userModel->login, 'password' => '1']);
            if (!$userModel->token) {
                $userModel->createToken($userModel);
            }
            $request->session()->put('token', $userModel->token->getApiKey());
            return redirect()->to("/news");
        }
        return redirect()->to("/news");
    }

    public function vkAuth(Request $request)
    {
        $vk_config = array(
            'app_id' => "5567209",
            'api_secret' => "vojmx8iQhLDa83Cv0wdW",
            'callback_url' => 'http://thepit.in.ua/auth/vk',
            'api_settings' => 'users'
        );

        $vk = new VK($vk_config["app_id"], $vk_config["api_secret"]);

        if (!isset($_REQUEST['code'])) {
            $authorize_url = $vk->getAuthorizeURL(
                $vk_config['api_settings'], $vk_config['callback_url']);
            return redirect($authorize_url);
        } else {
            $access_token = $vk->getAccessToken($_REQUEST['code'], $vk_config['callback_url']);
            $userVk = $vk->api('users.get', array(
                'uid' => $access_token["user_id"],
                'fields' => 'uid,first_name,last_name,screen_name,photo_100',
            ));
            if ($userVk["response"] && $userVk["response"][0]) {
                $userVk = $userVk["response"][0];
                $user = [
                    "email" => "{$userVk['screen_name']}_{$userVk['uid']}@vk.com",
                    "name" => "{$userVk['first_name']} {$userVk['last_name']}",
                    "avatar" => $userVk["photo_100"]
                ];
                return $this->authSocialUser($request,$user);
            } else {
                abort(401);
            }
        }
        return redirect()->to("/news");
    }

    private function checkUser(User $service, $email, $name, $avatar = null)
    {
        $user = $service->getModel()->query()->where('email', $email)->orWhere('login', $this->translit($name))->first();
        if (!$user) {
            $login = explode(' ', $name);
            $user = $service->getModel()->create([
                'first_name' => $login[0],
                'last_name' => $login[1],
                'email' => $email,
                'login' => $this->translit($name),
                'password' => '1',
                'avatar' => $avatar,
                'role_id' => 1,
                'status' => 1
            ]);
        }
        if ($user->avatar != $avatar) {
            $user->fill(['avatar' => $avatar]);
            $user->save();
        }
        return $user;
    }

    public function loginAction(Request $request)
    {
        $service = new User();
        $user['email'] = $request->input('auth_name');
        $service->auth(['login' => $this->checkUser($service, $user['email'], $user['email'])->login,
            'password' => $request->input('auth_pass')]);
        if (!$service->isErrors()) {
            $request->session()->put('token', $service->getModel()->token->getApiKey());
        }
        return $this->sendOk(['goto' => $request->capture()->server('HTTP_REFERER')]);
    }

    public function slugAction(Request $request, $slug)
    {
        $page = new StaticPie();
        /**
         * @var $model App\Modules\PieStatic\Database\Models\PiePage
         */
        $model = $page->getPageBySlug($slug);
        if (!$model) {
            abort(404);
        }
        $language = new Languages();
        $lang = $language->getModel()->where('name', App::getLocale())->first();
        $articleService = new Article();
        return view($this->getViewRoute('slug'), [
            'translate' => $model->getTranslatePage(isset($lang->id) ? $lang->id : 1),
            'menu' => $this->getMenu(),
            'lang' => ($lang) ? $lang->id : 1,
            'sponsors' => $this->getSponsors(),
            'article' => $articleService->getRandomArticle(($lang) ? $lang->id : 1)
        ]);
    }

    public function faqAction(Request $request)
    {
        $faq = new App\Modules\PitFaq\Http\Services\Faq\Question();
        return view($this->getViewRoute('faq'), [
            'collection' => $faq->filter($request->input('query')),
            'search' => $request->input('query'),
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);
    }

    public function likeAction(Request $request)
    {
        $like = new App\Modules\PieArticle\Http\Services\Like();
        $this->setRules([
            'user_id' => "required|exists:users,id",
            'article_id' => "required|exists:articles,id"
        ]);
        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        $response = $like->tick($request->input("user_id"), $request->input("article_id"));
        if ($response) {
            $article = new Article();
            return $this->sendOk([
                "data" => $article->getOne($request->input('article_id'))->likes->count(),
                "like" => $response->status != App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE
            ]);
        }
        return $this->sendWithErrors("Something went wrong");

    }
}