<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 03.10.2016
 * Time: 16:06
 */

namespace App\Modules\ThePit\Http\Controllers;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Http\Controllers\Controller;
use App\Modules\PieSponsors\Http\Services\Sponsor;
use App\Modules\PieStatic\Http\Services\StaticPie;
use App\Modules\PieTranslator\Http\Services\Languages;

class BasicController extends Controller
{
    protected function getMenu()
    {
        $static = new StaticPie();
        $language = new Languages();
        $language = $language->getModel()->where('name', App::getLocale())->first();
        return $static->getMenu((isset($language->id)) ? $language->id : 1);
    }

    protected function getSponsors()
    {
        $sponsors = new Sponsor();
        $sponsors->setWhere(['status' => Base::STATUS_ACTIVE]);
        return $sponsors->getAll();

    }
}