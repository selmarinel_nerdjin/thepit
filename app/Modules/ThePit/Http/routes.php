<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
    Route::group(['prefix' => '/', 'middleware' => 'webAdmin'], function () {
        Route::group(['as' => 'site:', 'middleware' => ['ware']], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'ThePitController@indexAction']);
            Route::get('/ru', ['as' => 'index:ru', 'uses' => 'ThePitController@ruAction']);
            Route::get('/en', ['as' => 'index:en', 'uses' => 'ThePitController@enAction']);
            Route::get('/ua', ['as' => 'index:ua', 'uses' => 'ThePitController@uaAction']);
            Route::get('/info', ['as' => 'info', 'uses' => 'ThePitController@infoAction']);
            Route::get('/contacts', ['as' => 'contacts', 'uses' => 'ThePitController@contactsAction']);
            Route::any('/news', ['as' => 'news', 'uses' => 'ThePitController@newsAction']);
            Route::post('/news', ['as' => 'news:post', 'uses' => 'ThePitController@newsRedirect']);
            Route::get('/news/category/{category}', ['as' => 'news:category', 'uses' => 'ThePitController@categoryRedirect']);
            Route::get('/news/tag/{tag}', ['as' => 'news:tag', 'uses' => 'ThePitController@tagRedirect']);
            Route::get('/news/{id}', ['as' => 'news:article', 'uses' => 'ThePitController@oneArticleAction']);
            Route::get('/media', ['as' => 'media', 'uses' => 'ThePitController@mediaAction']);

            Route::get('/auth/facebook', ['as' => 'socialite.auth.facebook', 'uses' => 'ThePitController@facebookAuth']);
            Route::get('/facebook/callback', ['as' => 'socialite.callback.facebook', "uses" => "ThePitController@facebookCallback"]);

            Route::get('/auth/vk', ['as' => 'socialite.auth.vk', 'uses' => 'ThePitController@vkAuth']);
            Route::get('/vk/callback', ['as' => 'socialite.callback.vk', 'uses' => 'ThePitController@vkCallback']);

            Route::get('/logout', ['as' => 'socialite:logout', 'uses' => 'ThePitController@logoutAction']);
            Route::post('/login', ['as' => 'socialite:login', 'uses' => 'ThePitController@loginAction']);

            Route::group(['prefix' => '/api', 'as' => 'api:'], function () {
                Route::get('comments/{id}/load', ['as' => 'load:comments', 'uses' => 'ThePitController@loadCommentAction']);
                Route::any('like', ['as' => 'like', 'uses' => 'ThePitController@likeAction']);
                Route::get('news', ['as' => 'news', 'uses' => 'ThePitController@newsApi']);
                Route::get('media', ['as' => 'media', 'uses' => 'ThePitController@mediaApi']);
                Route::get('quote', ['as' => 'quote', 'uses' => 'ThePitController@quoteApi']);
                Route::post('quote/append', ['as' => 'quote:append', 'uses' => 'ThePitController@quoteAppendApi']);
                Route::get('news/{id}/actual', ['as' => 'news:actual', 'uses' => 'ThePitController@newsActualApi']);
                Route::get('category/{id}', ['as' => 'category', 'uses' => 'ThePitController@categoryApi']);
                Route::post('/add/{id}/comment', ['as' => 'add:comment', 'uses' => 'ThePitController@addCommentAction']);

            });
            /**
             * -------------------
             */
            Route::any('/faq', ['as' => 'faq', 'uses' => 'ThePitController@faqAction']);
            Route::get('/{slug}', ['as' => 'slug', 'uses' => 'ThePitController@slugAction']);
        });
    });
});