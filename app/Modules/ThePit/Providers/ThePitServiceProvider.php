<?php
namespace App\Modules\ThePit\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ThePitServiceProvider extends ServiceProvider
{
	/**
	 * Register the ThePit module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\ThePit\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the ThePit module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('the_pit', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('the_pit', base_path('resources/views/vendor/the_pit'));
		View::addNamespace('the_pit', realpath(__DIR__.'/../Resources/Views'));
	}
}
