@extends('the_pit::layouts/blank')
@section('title')
    Main Page | The PIT
@endsection
@section('keywords','The PIT')
@section('description','The PIT')
@section('styles')
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('/css/bootstrap-material-design.min.css')}}">
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('/css/ripples.min.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/page.css')}}">
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('/css/loader.css')}}">
    @yield('css')
    <style>
        @-webkit-keyframes wow {
            from {
                transform: scale(1);
            }
            to {
                transform: scale(1.5);
            }
        }

        .upper {
            position: fixed;
            bottom: 10px;
            right: 10px;
            font-size: 2em;
            z-index: 1717;
        }

        .upper a {
            text-decoration: none;
            color: #2B2A28;
            text-shadow: 0 0 13px #dc000b;
        }

        .upper a i {
            transition: .5s;
        }

        .upper a:hover i {
            animation: wow 1s infinite alternate;
            -webkit-animation: wow 1s infinite alternate;
        }

        .navbar, .navbar.navbar-default {
            background-color: inherit;
            color: #2B2A28;
        }

        iframe {
            max-width: 100%;
        }

        .navbar.navbar-fixed-top {
            background-color: rgba(220, 122, 35, 0.9);
        }

        .navbar-fixed-top img {
            max-height: 35px;
        }

        .buy {
            display: none !important;
        }

        .navbar-fixed-top .buy {
            display: block !important;
        }

        .navbar-fixed-top .nav {
            padding-top: 0;
        }

        .btn-fucker-nav {
            box-shadow: 0 0 1px #282a2b !important;
            height: 40px;
            font-size: 1.5em;
            line-height: 40px;
            padding: 2px 15px;
            transform: rotate(0deg);
        }

        .btn-fucker-nav:hover {
            transform: scale(1.1);
        }
    </style>
@endsection
@section('content')
    <div class="upper">
        <a href="#header_main">
            <i class="fa fa-arrow-circle-o-up"></i>
        </a>
    </div>
    <header id="header_main">
        <nav class="navbar navbar-transparent" style="z-index: 1001;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a class="navbar-brand" href="{{route('site:index')}}"><img src="{{$app['pit']}}" height="85"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        @include('the_pit::layouts.block.menu_list')
                        <li class="buy">
                            {{--<a href="{{route("concert:ua")}}" target="_blank" style="padding: 10px"--}}
                            <a href="{{\App\Modules\PitPayments\Http\Controllers\PaymentController::CONCERT_UA}}" target="_blank" style="padding: 10px"
                               class="btn btn-fucker btn-fucker-nav page-scroll" id="buy">
                                {{trans('the_pit::main.body_buy')}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="header"></div>
    <section id="main">
        @yield('page')
    </section>
    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-2 col-sm-4 col-sm-offset-1 col-xs-offset-3 col-xs-6 text-center"
                     style="z-index:10">
                    <img src="{{$app['big_pit']}}" class="img-responsive">
                    <span class="pit_text">TRY TO SURVIVE</span>
                    <span class="lang-block-page">
                    <a href="{{route("site:index:en")}}" class="page-scroll lang page-lang">en</a>
                    <a href="{{route("site:index:ru")}}" class="page-scroll lang page-lang">ru</a>
                    <a href="{{route("site:index:ua")}}" class="page-scroll lang page-lang">ua</a>
                </span>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-offset-0 col-sm-offset-1 col-xs-12 text-center"
                     style="z-index:10">
                    <h2 class="pit_text sponsors_text col-xs-12 bronx">{{trans('the_pit::main.page_sponsors')}}</h2>
                    @if(isset($sponsors) && $sponsors && $sponsors->first())
                        @foreach($sponsors as $sponsor)
                            <a class="center" href="{{$sponsor->link}}" target="_blank">
                                <img src="{{$sponsor->getCover()}}"
                                     class="img-responsive sponsors_block col-lg-4 col-md-4 col-sm-6 col-xs-6">
                            </a>
                        @endforeach
                    @else
                        <h2 class="col-xs-12">
                            No sponsors jet
                        </h2>
                    @endif
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 text-center" style="z-index:10">
                    <h1 class="pit_text subscribe_text bronx">{{trans('the_pit::main.page_subsribe')}}</h1>
                    <ul class="list-inline">
                        @include("the_pit::layouts/block/links")
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="text-center">
                Copyright &copy; <a href="http://selmarinel.github.io" target="_blank"
                                    style="text-decoration: none; color:black">Selmarinel</a>. All right reserved
            </div>
        </div>
    </footer>
    <section class="popup" id="buy_popup">
        <div class="container container-a2">
            <div style="width: 100%;
    text-align: center;">
                <div class="title">
                    <div class="text-center">
                        {{--<a href="{{route("concert:ua")}}" target="_blank" style="display: inline-block;">--}}
                        <a href="{{\App\Modules\PitPayments\Http\Controllers\PaymentController::CONCERT_UA}}" target="_blank" style="display: inline-block;">
                            <img src="https://www.concert.ua/img/logo.png" class="img-responsive"
                                 alt="Concert.ua ThePIT">
                        </a>
                    </div>
                    <span class="btn btn-fucker" id="close_popup"><i class="fa fa-times"></i> Close</span>
                </div>
            </div>
        </div>
    </section>
    {{--<div class="b-popup" id="page-preloader" style="z-index: 2999">--}}
    {{--<div class="b-popup-content">--}}
    {{--<div id="container">--}}
    {{--<div class="big-circle circle">--}}
    {{--<div class="inr-circle circle"></div>--}}
    {{--<div class="inr-circle circle"></div>--}}
    {{--<div class="inr-circle circle"></div>--}}
    {{--</div>--}}
    {{--<div class="top-bar bar"></div>--}}
    {{--<div class="mid-bar bar"></div>--}}
    {{--<div class="btm-bar bar"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection
@section('scripts')
    <script src="{{$app['the_pit.assets']->getPath('/js/moment.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/ripples.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/material.min.js')}}"></script>
    @yield('script')
    <script>
        $(document).ready(function () {
            var $nav = $("nav");
            $(window).scroll(function () {
                if ($(this).scrollTop() > $nav.height() && !$nav.hasClass('navbar-fixed-top')) {
                    $nav.fadeOut('fast', function () {
                        $(this).addClass("navbar-fixed-top")
                            .fadeIn('slow');
                    });
                } else if ($(this).scrollTop() <= $nav.height() && $nav.hasClass('navbar-fixed-top')) {
                    $nav.removeClass("navbar-fixed-top");
                }
            });


            $('#buy').on('click', function () {
                $('#buy_popup').fadeIn('slow');
            });
            $("#close_popup").on('click', function () {
                $('#buy_popup').fadeOut('slow');
            });
            $(".upper").on("click", "a", function (event) {
                event.preventDefault();
                var id = $(this).attr('href');
                if (id) {
                    var top = $(id).offset().top;
                } else {
                    var top = $(body).offset().top;
                }
                $('body,html').animate({scrollTop: top}, 1000);
            });
        });

    </script>
@endsection
