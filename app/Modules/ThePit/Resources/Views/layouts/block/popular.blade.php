<div class="grid-item col-lg-12 col-md-4 col-sm-6 col-xs-12">
    <h4 class="popular_title">{{trans('the_pit::main.news_popular')}}</h4>
    <ul class="list-group">
        @foreach($popular as $model)
            <li class="list-group-item">
                <?php $translate = $model->getTranslateArticle($lang); ?>
                <a href="{{route('site:news:article',['id'=>$model->id])}}" title="The PIT | {{$translate['name']}}">
                    <div class="popular-body">
                        <div class="img-thumbnail popular_img popular{{$model->id}}">
                        </div>
                        <span class="model_name">{{$translate['name']}}</span>
                        <span class="model_info">{{$translate['info']}}</span>
                    </div>
                </a>
                <div class="popular-footer">
                    <span class="article_date"
                          data-text="{{$model->created_at->setTimeZone('Europe/Kiev')}}">{{$model->created_at}}</span>
                </div>
                <div class="popular-header-group">
                    @if($model->category)
                        <a href="{{route('site:news',['category'=>$model->category->id])}}" class="popular-category popular_category{{$model->id}} tag">
                            {{$model->category->name}}
                        </a>
                    @endif
                    <span class="popular-views">
                        <i class="fa fa-eye"></i> {{$model->views}}&nbsp;
                        <i class="fa fa-thumbs-up"></i> {{$model->likes->count()}}
                        <i class="fa fa-comments-o"></i> {{$model->comments->count()}}
                    </span>
                </div>
            </li>
        @endforeach
    </ul>
</div>