@if(isset($article))
    <?php $translate = $article->getTranslateArticle($lang); ?>
    <div class="grid-item article-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{route('site:news:article',['id'=>$article->id])}}" title="The PIT | {{$translate['name']}}">
            <div class="img article{{$article->id}}"></div>
            @if($article->category)
                <a href="{{route('site:news',['category'=>$article->category->id])}}" class="tag article_category{{$article->id}}">
                    {{$article->category->name}}
                </a>
            @endif
            <div class="article-information">
                <span class="article_date date-a"
                      data-text="{{$article->created_at->setTimeZone('Europe/Kiev')}}">{{$article->created_at}}</span>
                <span class="author">{{$article->getUser()->getFullName()}}</span>
                <span class="article-title">{{$translate['name']}}</span>
                <span class="article-info">{{$translate['info']}}</span>
                <span class="views">
                    <i class="fa fa-eye"></i> {{$article->views}}
                </span>
                <span class="comments">
                    <i class="fa fa-comments-o"></i> {{$article->comments->count()}}
                </span>
                <span class="comments comments-a">
                    <i class="fa fa-thumbs-up"></i> {{$article->likes->count()}}
                </span>
            </div>
        </a>
    </div>
@endif