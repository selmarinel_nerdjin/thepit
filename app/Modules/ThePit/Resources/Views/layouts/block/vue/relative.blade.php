<script type="text/x-template" id="pitItem">
    <a class="list-group-item" v-bind:href="'/news/'+item.id" :title="'The PIT | '+item.name">
        <div class="popular-body">
            <div class="img-thumbnail popular_img popular" :style="styleArticleObject">
            </div>
            <span class="model_name">@{{ item.name }}</span>
            <span class="model_info">@{{ item.info }}</span>
        </div>
    </a>
</script>
<script>
    function loadList(url,self) {
        $.ajax({
            url: url,
            type: "get",
            dataType: "json",
            success: function (response) {
                self.list = response.data;
            }
        })
    }
    Vue.component('pit_list_item', {
        template: "#pitItem",
        props: {
            item: Object,
        },
        computed: {
            styleArticleObject: function () {
                return {
                    'background-image': 'url(' + this.item.cover + ')'
                }
            },
            styleCategoryObject: function () {
                return {
                    'background-color': this.item.category.color
                }
            },
        }
    });
</script>