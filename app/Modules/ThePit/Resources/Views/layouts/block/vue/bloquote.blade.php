<script type="text/x-template" id="quote">
    <div class="blockquoted">
        <blockquote v-html="quote" v-if="show">
        </blockquote>
        <div class="reload-block" v-on:click="reload">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
</script>
<script>
    function loadQuote(self) {
        return $.ajax({
            url: "{{route('site:api:quote')}}",
            dataType: "json",
            type: "get",
            success: function (response) {
                if (response.data) {
                    self.load_quote = response.data;
                }
            },
            error: function () {
                loadQuote(self);
            }
        });
    }
    Vue.component('pie_quote', {
        template: "#quote",
        props: {
            show: Boolean,
            quote: String
        },
        beforeUpdate: function () {
            this.show = true;
        },
        created: function () {
            this.show = true;
        },
        methods: {
            reload: function () {
                loadQuote(this.$parent)
            },
            addQuote: function () {
                var self = this;
                $.confirm({
                    'title': "Add Quote",
                    "content": "<form class='form-vertical' method='POST' action='{{route('site:api:quote:append')}}'>" +
                    "<div class='form-group'>" +
                    "<label>Quote: </label>" +
                    "<textarea name='quote' class='form-control' style='color: #DC7A28;' rows='3' required placeholder='Input here'></textarea>" +
                    "</div>" +
                    "<input type='hidden' name='_token' value='{{ csrf_token() }}'>" +
                    "</form>",
                    type: 'purple',
                    typeAnimated: true,
                    closeIcon: true,
                    buttons: {
                        create: {
                            text: 'Create',
                            btnClass: 'btn-green',
                            action: function () {
                                var form = this.$content.find("form");
                                var data = form.serializeArray();
                                var errors = [];
                                $.each(data, function (k, v) {
                                    if (!v.value) {
                                        errors.push("Required field " + v.name);
                                    }
                                });
                                if (errors.length != 0) {
                                    $.alert({
                                        title: "Error",
                                        content: errors.join('</br>'),
                                        type: 'red'
                                    });
                                    return false;
                                }
                                $.ajax({
                                    url: form.attr('action'),
                                    type: form.attr('method'),
                                    dataType: "json",
                                    data: form.serializeArray(),
                                    success: function () {
                                        $.alert({
                                            title: "Saved",
                                            content: "Quote added",
                                            type: "green"
                                        });
                                        loadQuote(self.$parent)
                                    },
                                    error: function () {
                                        $.alert({
                                            title: "Error",
                                            content: "Quote Not saved",
                                            type: 'red'

                                        })
                                    }
                                });
                            }
                        },
                        cancel: function () {
                            return true;
                        }
                    }
                })
            }
        }
    });
</script>