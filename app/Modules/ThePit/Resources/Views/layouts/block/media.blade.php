@if(isset($media))
    <div class="grid-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
        @if($media->type == \App\Modules\PieMedia\Database\Models\Media::PHOTO)
            <a href="{{route('site:photo',['id' => $media->id])}}">
                <img src="{{$media->getFile(450)}}" class="img-responsive">
            </a>
            <div class="article-information">
                <span class="article_date date-a"
                      data-text="{{$media->created_at->setTimeZone('Europe/Kiev')}}">{{$media->created_at}}</span>
                <span class="article-title">{{$media->name}}</span>
            </div>
        @elseif($media->type == \App\Modules\PieMedia\Database\Models\Media::VIDEO)
            <iframe class="iframe-media" src="{!! ($media->link)? $media->link->link :App('full_fill') !!}"
                    frameborder="0"
                    allowfullscreen></iframe>
            <div class="article-information">
                <span class="article_date date-a"
                      data-text="{{$media->created_at->setTimeZone('Europe/Kiev')}}">{{$media->created_at}}</span>
                <span class="article-title">{{$media->name}}</span>
            </div>
        @elseif($media->type == \App\Modules\PieMedia\Database\Models\Media::CONTENT)
            @if($media->link && !$media->link->isYouTubeVideo())
                <a class="media-link" href="{{route('site:text',['id' => $media->link->id])}}" title="{{$media->name}}">
                    <div>
                        <i class="fa fa-file"></i>
                        <br/>
                        <span class="label">{{$media->link->link}}</span>
                    </div>
                </a>
                <div class="article-information">
                    <div class="article_date date-a"
                         data-text="{{$media->created_at->setTimeZone('Europe/Kiev')}}">{{$media->created_at}}</div>
                    <span class="article-title">{{$media->name}}</span>
                </div>
            @else
                <div class="no">
                    NO!
                </div>
                <img src="{{$media->getFile(450)}}" class="img-responsive">
            @endif
        @endif
    </div>
@endif