@if(isset($menu) && $menu)
    @foreach($menu as $item)
        <li>
            <a class="page-scroll bronx" href="{{route('site:slug',$item->slug)}}"
               title="The PIT | {{$item->info}}">{{$item->title}}</a>
        </li>
    @endforeach
@else
    <li>
        <a class="page-scroll bronx" href="{{route('site:info')}}">{{trans('the_pit::main.menu_info')}}</a>
    </li>
    <li>
        <a class="page-scroll bronx" href="{{route('site:contacts')}}">{{trans('the_pit::main.menu_contacts')}}</a>
    </li>
@endif
<li>
    <a class="page-scroll bronx" href="{{route('site:news')}}"
       title="The PIT | {{trans('the_pit::main.menu_news')}}">{{trans('the_pit::main.menu_news')}}</a>
</li>
<li>
    <a class="page-scroll bronx" href="{{route('site:media')}}"
       title="The PIT | {{trans('the_pit::main.menu_media')}}">{{trans('the_pit::main.menu_media')}}</a>
</li>
<li>
    <a class="page-scroll bronx" href="{{route('site:faq')}}"
       title="The PIT | FAQ">F.A.Q.</a>
</li>
@if(Request::user())
    @if(Request::user()->canAdministrate())
        <li>
            <a class="page-scroll bronx" href="{{route('admin:index')}}"><i class="fa fa-cog"></i></a>
        </li>
    @endif
@endif
