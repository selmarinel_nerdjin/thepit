@extends('the_pit::layouts/page')
@section('title'){{trans('pie_base::main.menu_media')}} | The PIT @endsection
@section('keywords')The PIT, Media @endsection
@section('css')
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
    <style>
        #media {
            display: flex;
            align-content: flex-start;
            flex-wrap: wrap;
        }

        .grid-item .img {
            min-height: 300px;
        }
        .full-fill{
            display: table;
            min-height: 300px;
            width: 100%;
        }
        .ribbon {
            text-transform: uppercase;
            position: relative;
            width: 150px;
            background-color: #DC7A23; /* цвет ленты */
            font: bold 16px Sans-Serif;
            color: #282a2b;    /* цвет шрифта */
            text-align: center;
            text-shadow: 1px 1px 0 rgba(255,255,255,0.1);
            padding: 7px 0;
            top: 25px;
            left: -35px;
            box-shadow: 0 2px 3px;
            -webkit-transform: rotate(-45deg);
            -moz-transform:    rotate(-45deg);
            -ms-transform:     rotate(-45deg);
            -o-transform:      rotate(-45deg);
        }
        .ribbon-wrapper {
            width:105px;
            height: 105px;
            overflow: hidden;
            position: absolute;
            top: 7px;
            left: 2px;
        }
    </style>
@endsection
@section('page')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title_main">{{trans('pie_base::main.menu_media')}}</h1>
                @if(isset($collection))
                    <div class="grid" id="media">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 block" v-for="item in media">
                            <pie_media :item="item">
                            </pie_media>
                        </div>
                        @endif
                    </div>
            </div>
        </div>
        @endsection
        @section('script')
            <script src="{{$app['pie_task_manager.assets']->getPath('js/vue.min.js')}}"></script>
            <script src="{{$app['the_pit.assets']->getPath('/js/moment.min.js')}}"></script>
            <script type="text/x-template" id="template">
                <div class="grid-item article-item panel panel-default">
                    <time class="article_date date-a">
                        @{{ momentoDate }}
                    </time>
                    <div class="img" v-bind:style="styleItemObject">
                        <div class="ribbon-wrapper" v-if="item.type==4"><div class="ribbon">Podcast</div></div>
                        <a v-bind:href="'/photo/'+item.id" :title="'The PIT | '+item.name" class="full-fill" v-if="item.type == 1">
                        </a>
                        <iframe class="iframe-media" :src="(item.link)?item.link.link:''"
                                frameborder="0"
                                allowfullscreen v-if="item.link"></iframe>
                    </div>
                    <div class="article-information panel-footer">
                        <div class="article-title">
                            @{{ item.name }}
                        </div>
                    </div>
                </div>
            </script>
            <script>
                $(document).ready(function () {
                    Vue.component('pie_media', {
                        template: "#template",
                        props: {
                            item: Object
                        },
                        computed: {
                            styleItemObject: function () {
                                return {
                                    'background-image': 'url(' + this.item.cover + ')'
                                }
                            },
                            momentoDate: function () {
                                var time = moment(this.item.created_at).locale('<?=(App::getLocale() == 'ru') ? App::getLocale() : 'en'?>');
                                if (time.isValid()) {
                                    return time.fromNow();
                                } else {
                                    return 'Never';
                                }
                            }
                        }
                    });
                    function loadData(self, page) {
                        $.ajax({
                                    url: "{{route('site:api:media')}}",
                                    dataType: "json",
                                    type: "get",
                                    data: {
                                        page: page
                                    },
                                    success: function (response) {
                                        self.media = response.data.data;
                                        self.pagination = {
                                            total: response.data.total,
                                            per_page: response.data.per_page,
                                            from: response.data.current_page,
                                            to: response.data.to,
                                            current_page: response.data.current_page,
                                            last_page: response.data.last_page
                                        };
                                    },
                                    error: function () {
                                        loadData(self);
                                    }
                                }
                        );
                    }

                    new Vue({
                        el: "#media",
                        data: {
                            media: {},
                            searchString: "",
                            pagination: {
                                total: 0,
                                per_page: 7,
                                from: 1,
                                to: 0,
                                current_page: 1,
                                last_page: 1
                            },
                            searchEnable: false
                        },
                        created: function () {
                            this.fetchItems(this.pagination.current_page);
                            var self = this;
                            setInterval(function () {
                                self.fetchItems(self.pagination.current_page);
                            }, 30 * 6 * 1000);
                        },
                        methods: {
                            fetchItems: function (page, category) {
                                if (!category) {
                                    category = this.category
                                }
                                loadData(this, page, category);
                            },
                            fetchAll: function () {
                                this.tag = null;
                                this.category = null;
                                this.searchString = "";
                                this.searchEnable = false;
                                loadData(this, 1);
                            },
                            changePage: function (page) {
                                this.pagination.current_page = page;
                                this.fetchItems(page);
                            }
                        }
                    });
                });
            </script>
@endsection