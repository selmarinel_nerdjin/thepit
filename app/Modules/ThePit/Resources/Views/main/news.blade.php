@extends('the_pit::layouts/page')
@section('title'){{trans('the_pit::main.menu_news')}} | The PIT @endsection
@section('keywords')The PIT, News @endsection
@section('css')
    <link rel="stylesheet" href="vendor/flipclock/flipclock.css">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
    <link rel="stylesheet" href="{{$app['pie_task_manager.assets']->getPath('css/jquery-confirm.min.css')}}"
          xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <style>
        .btn.active {
            -webkit-box-shadow: 1px 3px 10px -4px #000, 1px -3px 10px -4px #000;
            -moz-box-shadow: 1px 3px 10px -4px #000, 1px -3px 10px -4px #000;
            box-shadow: 1px 3px 10px -4px #000, 1px -3px 10px -4px #000;
        }

        .loadingContent {
            height: 50%;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
        }

        .loadingContent h2 {
            width: 100%;
        }

        iframe {
            overflow: hidden;
            height: 204px;
            width: 100% !important;
            max-width: 100%;
        }

        #vk_groups, #vk_groups iframe {
            width: 100% !important;
            height: 240px;
        }

        .btn-facebook {
            background-color: #3b5998;
            color: #F7f7f7 !important;
        }

        .btn-system {
            background-color: #DC7A23;
            color: #282a2b !important;
            padding: 0;
            font-size: 14px;
        }

        .btn-vk {
            background-color: #45668e;
            color: #F7f7f7 !important;
        }

        .btn-facebook:hover {
            color: #090909 !important;
        }

        .btn-vk:hover {
            color: #090909 !important;
        }
    </style>
@endsection
@section('page')
    <div class="container" id="app" v-ref:app>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page_title_main">{{trans('the_pit::main.menu_news')}}</h1>
            </div>

            <div class="btn-toolbar col-xs-12">
                <div class="btn-group">
                    <div class="btn btn-all" v-on:click="fetchAll">
                        {{trans('the_pit::main.news_all')}}
                    </div>
                    @foreach($categories->take(5) as $category)
                        <div class="btn" v-on:click="filterCategory({{$category->id}})"
                             :class="{active:(category == {{$category->id}})}">
                            {{$category->name}}
                        </div>
                    @endforeach
                    <div class="btn" v-on:click="showSearch">
                        <i class="fa fa-search"></i>
                    </div>
                </div>

                <div class="form-group" v-if="searchEnable">
                    <div class="input-group">
                        <input type="text" v-model="searchString" class="form-control"
                               placeholder="{{trans('the_pit::main.news_filter')}}">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-fab btn-fab-mini" v-on:click="filterArticles">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div id="grid" class="row">
                    <div class="col-lg-9 col-md-8 col-xs-12">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 block" v-for="item in articles">
                            <pie_article
                                    :article_model="item"
                                    :filter="searchString">
                            </pie_article>
                        </div>
                        <transition name="fade">
                            <div class="loadContent" v-if="articleLoading">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </transition>
                        <div class="col-xs-12 text-center loadingContent" v-if="!articles.length">
                            <h2 v-if="articleLoading">We are loading!</h2>
                            <h2 v-if="!articleLoading">
                                Sorry, but we did not find anything
                            </h2>
                            <img class="img-circle img-responsive" src="/images/vault.gif" style="margin:0 auto">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                        {{--<div class="panel panel-default">--}}
                            {{--<div class="panel-heading">--}}
                                {{--Time to beginning--}}
                            {{--</div>--}}
                            {{--<div class="panel-body">--}}
                                {{--<div class="your-clock"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="panel panel-default" style="display:none;">
                            <div class="panel-heading">
                                {{trans('the_pit::main.page_categories')}}
                            </div>
                            <div class="panel-body">
                                @foreach($categories as $category)
                                    <div class="btn btn-sm" v-on:click="filterCategory({{$category->id}})">
                                        {{$category->name}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('the_pit::main.page_tags')}}
                                <kbd v-if="tag">
                                    #@{{ tag }}
                                </kbd>
                            </div>
                            <div class="panel-body">
                                @foreach($tags as $tag)
                                    <kbd v-on:click="tagArticles('{{$tag->tag}}')">
                                        #{{$tag->tag}}
                                    </kbd>
                                @endforeach
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <pie_quote :quote="load_quote">
                                </pie_quote>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('the_pit::main.auth')}}
                            </div>
                            <div class="panel-body text-center">
                                @if(!Request::user())
                                    <a href="{{route('site:socialite.auth.facebook')}}" class="facebook-link">
                                        <div class="btn btn-facebook">
                                            <div class="facebook-div">
                                                <i class="fa fa-facebook"></i>
                                            </div>
                                            <div class="facebook-div-text">
                                                Facebook
                                            </div>
                                        </div>
                                    </a>
                                    {{--<a href="{{route('site:socialite.auth.vk')}}" class="facebook-link">--}}
                                    {{--<div class="btn btn-vk">--}}
                                    {{--<div class="facebook-div">--}}
                                    {{--<i class="fa fa-vk"></i>--}}
                                    {{--</div>--}}
                                    {{--<div class="facebook-div-text">--}}
                                    {{--VK--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</a>--}}
                                    <a class="facebook-link">
                                        <div class="btn btn-system" v-on:click="loginShow()">
                                            <div class="facebook-div">
                                                <img src="{{$app['pit']}}" class="img-responsive" width="25">
                                            </div>
                                            <div class="facebook-div-text" style="float: right">
                                                In System
                                            </div>
                                        </div>
                                    </a>
                                    <form method="post" action="{{route('site:socialite:login')}}" id="auth_form_dom"
                                          class="panel-heading"
                                          v-if="loginEnable">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group-alt">
                                            <input type="text" name="auth_name" id="auth_name"
                                                   required placeholder="{{trans('the_pit::main.form_login_email')}}">

                                        </div>
                                        <div class="form-group-alt">
                                            <input type="password" name="auth_pass" id="auth_pass"
                                                   placeholder="{{trans('the_pit::main.form_password')}}">
                                        </div>
                                        <div class="text-center" style="margin-top: 1em">
                                            <button type="button" class="btn btn-raised btn-warning" id="auth_login"
                                                    v-on:click="loginAction()">
                                                {{trans('the_pit::main.page_login')}}
                                            </button>
                                        </div>
                                    </form>
                                @else
                                    <b>TRY TO SURVIVE</b>
                                    <br/>
                                    {{Request::user()->getFullName()}}
                                    <br/>
                                    <a href="{{route('site:socialite:logout')}}"
                                       class="btn btn-raised btn-xs btn-danger">Logout</a>
                                @endif
                            </div>
                        </div>
                        {{--<div class="panel panel-default">--}}
                        {{--<div id="vk_groups"></div>--}}
                        {{--</div>--}}
                        <div class="panel panel-default">
                            <div class="fb-page" data-href="https://www.facebook.com/ThePitPostApocalypse"
                                 data-tabs="timeline"
                                 data-small-header="false"
                                 data-height="350"
                                 data-adapt-container-width="true"
                                 data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/ThePitPostApocalypse"
                                            class="fb-xfbml-parse-ignore"><a
                                            href="https://www.facebook.com/ThePitPostApocalypse">The Pit -
                                        Post-apocalyptic festival</a></blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <ul class="pagination">
                        <li v-if="pagination.current_page > 1">
                            <a href="#" aria-label="Previous"
                               @click.prevent="changePage(pagination.current_page - 1)">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li v-for="page in pagesNumber"
                            v-bind:class="[ page == isActived ? 'active' : '']">
                            <a href="#"
                               @click.prevent="changePage(page)">@{{ page }}</a>
                        </li>
                        <li v-if="pagination.current_page < pagination.last_page">
                            <a href="#" aria-label="Next"
                               @click.prevent="changePage(pagination.current_page + 1)">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9&appId=216155222107623";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script src="{{$app['pie_task_manager.assets']->getPath('js/vue.min.js')}}"></script>
    <script src="{{$app['pie_task_manager.assets']->getPath('js/jquery-confirm.js')}}"></script>
    <script src="vendor/flipclock/flipclock.min.js"></script>

    <script type="text/x-template" id="template">
        <transition name="fade">
            <div class="grid-item article-item panel panel-default">
                <a v-bind:href="'/news/'+article_model.id" :title="'The PIT | '+article_model.name">
                    <div class="img" v-bind:style="styleArticleObject"></div>
                </a>
                <category class="tag btn-raised" v-bind:style="styleCategoryObject" :title="article_model.category.name"
                          v-on:click="goCategory(article_model)">
                    @{{ article_model.category.name }}
                </category>
                <time class="article_date date-a">
                    @{{ momentoDate }}
                </time>
                <div class="article-information panel-footer">
                    <div class="author">@{{ userFullName }}</div>
                    <div class="article-title">
                        @{{ article_model.name }}
                    </div>
                    <div class="article-info">
                        @{{ article_model.info }}
                    </div>
                    <div class="information wobble-vertical">
                        <div class="views">
                            <i class="fa fa-eye"></i> @{{ article_model.views }}
                        </div>
                        <div class="comments">
                            <i class="fa fa-comments-o"></i> @{{ article_model.comments.length }}
                        </div>
                        <div class="comments comments-a">
                            <i class="fa fa-thumbs-up"></i>@{{ article_model.likes.length }}
                        </div>
                    </div>
                </div>
            </div>
        </transition>
    </script>
    @include("the_pit::layouts/block/vue/bloquote")
    {{--<script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>--}}
    <script>
        $(function () {
            $.material.init();
//            VK.Widgets.Group("vk_groups", {mode: 3, width: "320"}, 96458141);
        });

        function loadData(self, page) {
            $.ajax({
                    url: "{{route('site:api:news')}}",
                    dataType: "json",
                    type: "get",
                    data: {
                        page: page,
                        category: self.category,
                        filter: self.searchString,
                        tag: self.tag
                    },
                    success: function (response) {
                        self.articleLoading = false;
                        self.articles = response.data.data;
                        self.pagination = {
                            total: response.data.total,
                            per_page: response.data.per_page,
                            from: response.data.current_page,
                            to: response.data.to,
                            current_page: response.data.current_page,
                            last_page: response.data.last_page
                        };
                    },
                    error: function () {
                        loadData(self);
                    }
                }
            );
        }
        Vue.component('pie_article', {
            template: "#template",
            props: {
                article_model: Object,
                category: Number,
                filter: String,
            },
            computed: {
                styleArticleObject: function () {
                    return {
                        'background-image': 'url(' + this.article_model.cover + ')'
                    }
                },
                styleCategoryObject: function () {
                    return {
                        'background-color': this.article_model.category.color
                    }
                },
                userFullName: function () {
                    return this.article_model.user.first_name + " " + this.article_model.user.last_name;
                },
                momentoDate: function () {
                    var time = moment(this.article_model.created_at).locale('<?=(App::getLocale() == 'ru') ? App::getLocale() : 'en'?>');
                    if (time.isValid()) {
                        return time.fromNow();
                    } else {
                        return 'Never';
                    }
                }
            },
            methods: {
                goCategory: function (item) {
                    this.$parent.category = item.category.id
                    this.$parent.fetchItems(1);
                },
            }
        });
        new Vue({
            el: "#app",
            data: {
                articles: {},
                searchString: "",
                load_quote: "",
                articleLoading: true,
                category: "{{Session::get('category')}}",
                tag: "{{Session::get('tag')}}",
                pagination: {
                    total: 0,
                    per_page: 7,
                    from: 1,
                    to: 0,
                    current_page: 1,
                    last_page: 1
                },
                searchEnable: false,
                loginEnable: false,
                clock: ""
            },
            created: function () {
                this.fetchItems(this.pagination.current_page);
                var self = this
                setInterval(function () {
                    self.fetchItems(self.pagination.current_page);
                }, 30 * 6 * 1000)
                loadQuote(this);
            },
            mounted: function () {
                // Grab the current date
                var currentDate = new Date();

                // Set some date in the future. In this case, it's always Jan 1
                var futureDate = new Date("Jul 28 2017 19:00:00");

                // Calculate the difference in seconds between the future and current date
                var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;


                // Instantiate a coutdown FlipClock
                this.clock = $('.your-clock').FlipClock(diff, {
                    clockFace: 'DailyCounter',
                    countdown: true,
                    showSeconds: false
                });
            },
            computed: {
                isActived: function () {
                    return this.pagination.current_page;
                },
                pagesNumber: function () {
                    if (!this.pagination.to) {
                        return [];
                    }
                    var from = this.pagination.current_page - this.offset;
                    if (from < 1) {
                        from = 1;
                    }
                    var to = from + (this.offset * 2);
                    if (to >= this.pagination.last_page) {
                        to = this.pagination.last_page;
                    }
                    var pagesArray = [];
                    from = 1;
                    to = this.pagination.last_page;
                    while (from <= to) {
                        pagesArray.push(from);
                        from++;
                    }
                    return pagesArray;
                },
            },
            methods: {
                loginAction: function () {
                    var $form = $("#auth_form_dom");
                    $.ajax({
                        url: $form.attr('action'),
                        type: 'post',
                        dataType: 'json',
                        data: $form.serializeArray(),
                        success: function (response) {
                            if (response.goto != undefined) {
                                setTimeout(function () {
                                    window.location.href = response.goto;
                                }, 1000);
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    return false;
                },
                fetchItems: function (page, category) {
                    if (!category) {
                        category = this.category
                    }
                    loadData(this, page, category);
                },
                fetchAll: function () {
                    this.tag = null;
                    this.category = null;
                    this.searchString = "";
                    this.searchEnable = false;
                    loadData(this, 1);
                },
                changePage: function (page) {
                    this.pagination.current_page = page;
                    this.fetchItems(page);
                },
                showSearch: function () {
                    if (!this.searchString) {
                        this.searchEnable = !this.searchEnable;
                    }

                },
                filterCategory: function (category) {
                    this.tag = null;
                    this.searchString = null;
                    this.category = category;
                    this.fetchItems(1)
                },
                filterArticles: function () {
                    this.tag = null;
                    this.category = null;
                    this.fetchItems(1);
                    if (!this.searchString) {
                        this.searchEnable = false;
                    }
                },
                tagArticles: function (tag) {
                    this.category = null;
                    this.searchString = null;
                    this.tag = tag;
                    this.fetchItems(1);
                },
                loginShow: function () {
                    this.loginEnable = !this.loginEnable;
                }
            }
        });
    </script>
@endsection