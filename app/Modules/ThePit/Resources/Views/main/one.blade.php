@extends('the_pit::layouts/page')
@section('css')
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/page.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/uno.css')}}">
    <link rel="stylesheet" href="{{$app['pie_task_manager.assets']->getPath('css/jquery-confirm.min.css')}}"
          xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <link href="https://fonts.googleapis.com/css?family=Audiowide" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pangolin" rel="stylesheet">
    <link rel="stylesheet" href="{{$app['pit_faq.assets']->getPath('css/faqer.css')}}">
    <style>
        .category-color {
            background-color: {{$model->category->color}} ;
        }
    </style>
    <style>
        .reply_form {
            zoom: 1;
            position: relative;
            padding: 10px;
        }

        .post_field_user {
            float: left;
            position: relative;
        }

        .post_field_user_link {
            display: block;
            height: 28px;
        }

        .post_field_user_image {
            left: 20px;
            border-radius: 50%;
            width: 30px;
        }

        .reply_field_wrap {
            margin-left: 48px;
            height: auto;
        }

        textarea {
            width: 100%;
            background-color: transparent;
            border: transparent;
            padding: 5px 10px;
        }

        textarea::-webkit-input-placeholder {
            color: #DC7A23;
        }

        textarea::-moz-placeholder {
            color: #DC7A23;
        }

        /* Firefox 19+ */
        textarea:-moz-placeholder {
            color: #DC7A23;
        }

        /* Firefox 18- */
        textarea:-ms-input-placeholder {
            color: #DC7A23;
        }

        textarea {
            height: 75px;
            width: 100%;
            background-color: transparent;
            border: transparent;
            padding: 5px 10px;
            resize: vertical;
        }

        .mdl-badge {
            margin-right: 10px;
            position: relative;
            font-weight: normal;
            font-style: normal;
            font-size: 24px;
            line-height: 1;
            letter-spacing: normal;
            text-transform: none;
            display: inline-block;
            white-space: nowrap;
            word-wrap: normal;
            direction: ltr;
            -webkit-font-feature-settings: 'liga';
            -webkit-font-smoothing: antialiased;
        }

        .mdl-badge[data-badge]:after {
            right: -10px;
            content: attr(data-badge);
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-content: center;
            -ms-flex-line-pack: center;
            align-content: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            position: absolute;
            top: -11px;
            font-family: "Roboto", "Helvetica", "Arial", sans-serif;
            font-weight: 600;
            font-size: 12px;
            width: 22px;
            height: 22px;
            border-radius: 50%;
            background: #DC7A23;
            color: #fff;
        }
    </style>
@endsection
@section('keywords')The PIT, {{$model->keywords}}@endsection
@section('description'){{$model->description}}@endsection
@section('title'){{$model->name}} | The PIT @endsection
@section('author'){{$model->getUser()->getFullName()}}@endsection
@section('page')
    <section id="app">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page_title_main">{{$model->name}}</h1>
                </div>
                <div class="col-lg-9 col-md-8 col-xs-12 well">
                    <div class="page-header">
                        <h1 class="">{{$model->info}}</h1>
                        <div class="article_date" data-text="{{$model->created_at}}">
                            {{$model->created_at}}
                        </div>
                        <div style="display: inline-block; margin-top: -10px; float: left; margin-left: 10px">
                            <a href="{{route('site:news:category',$model->category->id)}}"
                               class="tag text-left category-color">
                                {{$model->category->name}}
                            </a>
                        </div>
                        <div class="info-block">
                            <div class="like section">
                                <div id="like" v-on:click="like">
                                    <i class="fa fa-thumbs-up
                                @if(Request::user() && $model->getLikeByUser(Request::user()))liked @endif"></i>
                                </div>
                                <div id="likes_count">
                                    {{$model->likes->count()}}
                                </div>
                            </div>
                            <div class="view section">
                                <i class="fa fa-eye"></i> {{$model->views}}
                            </div>
                        </div>
                    </div>
                    <div class="page-blocker">
                        <div class="page-image">
                            <img src="{{$model->getCover(850)}}" alt="{{$model->name}}" v-on:click="bigPic">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="text">
                        {!! $model->text !!}
                    </div>
                    <hr>
                    <div class="tags">
                        @foreach($model->tags as $tag)
                            <a href="{{route('site:news:tag',$tag->tag)}}" class="tag tag-a">#{{$tag->tag}}</a>
                        @endforeach
                    </div>
                    {{--Comments--}}
                    <section class="comments">
                        <transition name="fade">
                            <div class="loadContent" v-if="loading">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </transition>

                        {{--comments list--}}
                        <section id="comment_block" v-for="comment in comments.collection">
                            <transition name="fade">
                                <pit_comment
                                        v-if="!loading"
                                        :comment="comment">
                                </pit_comment>
                            </transition>
                        </section>
                        <div class="text-center">
                            <button class="btn btn-raised btn-sm mdl-badge" v-bind:data-badge="more"
                                    v-if="more" v-on:click="moreComments">
                                More
                            </button>
                        </div>
                        {{--Append comment--}}
                        <div class="">
                            @if(Request::user())
                                <div class="reply_form">
                                    <input type="hidden" id="" value="">
                                    <div class="post_field_user">
                                        <a class="post_field_user_link" href="/selmarinel">
                                            <img class="post_field_user_image"
                                                 src="https://pp.vk.me/c836528/v836528832/4f0e/Z-5xKT3qR2Q.jpg">
                                        </a>
                                    </div>
                                    <div class="reply_field_wrap">
                                        <textarea type="text" placeholder="Comment"
                                                  v-model="comment_text">

                                        </textarea>
                                    </div>
                                    <div style="text-align: right">
                                        <button class="btn btn-raised btn-sm btn-fucker btn-fucker-a"
                                                v-if="comment_text"
                                                v-on:click="sendComment">
                                            Send
                                        </button>
                                    </div>
                                </div>
                            @else
                                <div class="text-center">
                                    <div class="btn btn-fucker btn-fucker-a" id="auth">
                                        Log in
                                    </div>
                                </div>
                            @endif
                        </div>
                    </section>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <transition name="fade">
                                <div class="loadContent" v-if="loading">
                                    <i class="fa fa-refresh fa-spin"></i>
                                </div>
                            </transition>
                            <pie_quote :quote="load_quote">
                            </pie_quote>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{trans('the_pit::main.page_see_more')}}
                        </div>
                        <div class="panel-body panel-body-a">
                            <transition name="fade">
                                <div class="loadContent" v-if="loading">
                                    <i class="fa fa-refresh fa-spin"></i>
                                </div>
                            </transition>
                            <pit_list_item v-for="item in list"
                                           :item="item">
                            </pit_list_item>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="b-popup" id="loading">
        <div class="b-popup-content">
            <div id="container">
                <div class="big-circle circle">
                    <div class="inr-circle circle"></div>
                    <div class="inr-circle circle"></div>
                    <div class="inr-circle circle"></div>
                </div>
                <div class="top-bar bar"></div>
                <div class="mid-bar bar"></div>
                <div class="btm-bar bar"></div>
            </div>
        </div>
    </div>
    <section class="popup" id="auth_form">
        <div class="container container-a2">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-8">
                <div class="panel panel-pit">
                    <div class="panel-heading">
                        <strong>LOG IN to <img src="{{$app['pit']}}" width="50"></strong>
                        <span id="auth_form_close" class="btn btn-closer"><i class="fa fa-times"></i></span>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 text-center">
                            <a href="{{route('site:socialite.auth.facebook')}}" class="facebook-link">
                                <div class="btn btn-facebook">
                                    <div class="facebook-div">
                                        <i class="fa fa-facebook"></i>
                                    </div>
                                    <div class="facebook-div-text">
                                        Facebook
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 padding-5"></div>
                        <div class="col-xs-12">
                            <form method="post" action="{{route('site:socialite:login')}}" id="auth_form_dom">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group-alt">
                                    <input type="text" name="auth_name" id="auth_name"
                                           required placeholder="{{trans('the_pit::main.form_login_email')}}">

                                </div>
                                <div class="form-group-alt">
                                    <input type="password" name="auth_pass" id="auth_pass"
                                           placeholder="{{trans('the_pit::main.form_password')}}">
                                </div>
                                <div class="text-center" style="margin-top: 1em">
                                    <button type="submit" class="btn btn-login" id="auth_login">
                                        {{trans('the_pit::main.page_login')}}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--FAQER--}}
    <div id="demo">
        <faqer
                :collection="gridData"
                :collection_data="fullData"
        >
        </faqer>
    </div>
@endsection
@section('script')
    <script src="{{$app['the_pit.assets']->getPath('/js/masonry.pkgd.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/in_js.js')}}"></script>
    <script src="{{$app['pie_task_manager.assets']->getPath('js/vue.min.js')}}"></script>
    <script src="{{$app['pie_task_manager.assets']->getPath('js/jquery-confirm.js')}}"></script>
    @include("the_pit::layouts/block/vue/bloquote")
    @include("the_pit::layouts/block/vue/relative")
    @include("pit_faq::faqer/main")
    <script type="text/x-template" id="pitComment">
        <div class="reply reply_dived clear">
            <div class="reply_wrap clear_fix">
                <div class="reply_image">
                    <img v-bind:src="comment.user.avatar" width="75" height="75" class="reply_img" alt="">
                </div>
                <div class="reply_content">
                    <div class="reply_author">
                        <div class="author-a" data-from-id="101962832">
                            @{{ comment.user.name }}
                        </div>
                    </div>
                    <div class="reply_text">
                        <div class="wall_reply_text">
                            @{{ comment.text }}
                        </div>
                    </div>
                    <div class="reply_footer clear_fix" id="wpe_bottom101962832_3490">
                        <div class="reply_date">
                            <span class="rel_date"> @{{ date }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <script>
        function loadComments(self) {
            $.ajax({
                url: "{{route('site:api:load:comments',$model->id)}}",
                data: {
                    offset: self.offset
                },
                type: "get",
                dataType: "json",
                success: function (response) {
                    self.loading = false;
                    var collection = (self.comments.collection)?self.comments.collection:[];
                    self.comments = response;
                    self.comments.collection = collection.concat(self.comments.collection);
                },
                error: function () {
                    loadComments(self);
                }
            })
        }
        Vue.component("pit_comment", {
            template: "#pitComment",
            props: {
                comment: Object,
            },
            computed: {
                date: function () {
                    var time = moment(this.comment.created_at).locale('<?=(App::getLocale() == 'ru') ? App::getLocale() : 'en'?>');
                    if (time.isValid()) {
                        return time.fromNow();
                    } else {
                        return 'Never';
                    }
                }
            }
        })
    </script>
    <script>
        new Vue({
            el: "#app",
            data: {
                load_quote: "",
                list: Object,
                comments: Object,
                loading: true,
                comment_text: "",
                offset: 0
            },
            computed: {
                more: function () {
                    if (this.comments && this.comments.collection) {
                        if (this.comments.collection.length < this.comments.count) {
                            this.offset = this.comments.collection.length;
                            return this.comments.count - this.comments.collection.length;
                        }
                    }
                    return false;
                }
            },
            created: function () {
                loadQuote(this);
                loadList("{{route('site:api:news:actual',$model->id)}}", this);
                loadComments(this);
            },
            methods: {
                bigPic: function () {
                    $.dialog({
                        "title": "{{$model->name}}",
                        "content": "<img class='full' src='{{$model->getCover()}}'>" +
                        "<div>{{$model->info}}</div>",
                        backgroundDismiss: true,
                        columnClass: 'col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-12'
                    });
                },
                like: function () {
                    @if(Request::user())
                        $.ajax({
                        url: "{{route("site:api:like")}}",
                        dataType: "json",
                        type: "get",
                        data: {
                            "user_id": "{{ Request::user()->id }}",
                            "article_id": "{{$model->id}}"
                        },
                        success: function (response) {
                            if (response.data || response.data === 0) {
                                $("#likes_count").text(response.data);
                            }
                            if (response.like) {
                                $("#like").find('i').removeClass("liked")
                            } else {
                                $("#like").find('i').addClass("liked")
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    @else
                        $("#auth_form").fadeIn('slow');
                    @endif
                },
                sendComment: function () {
                    var self = this;
                    if (!this.comment_text) {
                        return $.alert('error');
                    }
                    $.ajax({
                        url: "{{route('site:api:add:comment',$model->id)}}",
                        data: {
                            comment_text: self.comment_text,
                            _token: "{{csrf_token()}}"
                        },
                        type: "post",
                        dataType: "json",
                        success: function (response) {
                            self.comments.collection = [response.data].concat(self.comments.collection);
                            self.comment_text = "";
                        }
                    })

                },
                moreComments: function () {
                    loadComments(this);
                }
            }
        });
    </script>
    <script>
        $(document).ready(function () {
            $.material.init();
            $(".article_date").each(function (k, v) {
                var time = moment($(v).attr('data-text')).locale('<?=(App::getLocale() == 'ru') ? App::getLocale() : 'en'?>');
                if (time.isValid()) {
                    $(v).html(time.format("dddd, MMMM Do YYYY, HH:mm:ss"));
                } else {
                    $(v).html('Never');
                }
            });
            $("#auth").on('click', function () {
                $("#auth_form").fadeIn('slow');
            });
            $("#auth_form_close").on('click', function () {
                $("#auth_form").fadeOut('slow');
            });
            $("#auth_form_dom").on('submit', function () {
                var $form = $(this);
                var $title = $("#auth_form").find('.title');
                $.ajax({
                    url: $form.attr('action'),
                    type: 'post',
                    dataType: 'json',
                    data: $form.serializeArray(),
                    success: function (response) {
                        if (response.goto != undefined) {
                            $title.empty()
                            $title.append("<h1>Success</h1><small>Wait few seconds</small>")
                            setTimeout(function () {
                                window.location.href = response.goto;
                            }, 1000);
                        }
                        console.log(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                return false;
            });
        });
    </script>
@stop