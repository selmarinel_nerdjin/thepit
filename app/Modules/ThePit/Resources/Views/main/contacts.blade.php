@extends('the_pit::layouts/page')
@section('page')
    <div class="container">
        <div class="row">
            <h1 class="page_title_main">Contacts</h1>
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <h3 style="font-family: 'Open sans', sans-serif;">You have a phone? </h3>
                <div style="font-family: 'Open sans', sans-serif; font-size: 1.5em;">Then you can call the number
                    <strong class="thumbnail">+38 (093) 802 10 64</strong> Or<strong class="thumbnail">+38 (095) 302 65
                        67</strong></div>
                <p><br/><br/></p>
                <h3 style="font-family: 'Open sans', sans-serif;">Your phone is being repaired, or you do not know what
                    is this "phone"? </h3>
                <div style="font-family: 'Open sans', sans-serif; font-size: 1.5em;">Then you can freely write an e-mail
                    to us on <a href="https://vk.com/write?email=eventsneverland@gmail.com" target="_blank">eventsneverland@gmail
                        .com</a></div>
                <p><br/><br/></p>
                <h3 style="font-family: 'Open sans', sans-serif;">E-mail is not for you? </h3>
                <div style="font-family: 'Open sans', sans-serif; font-size: 1.5em;">Not a problem, you can contact us
                    using social networks!
                </div>
                <div class="social-icon">
                    <ul class="list-group">
                        <li class="list-group-item"><a href="https://www.facebook.com/ThePitPostApocalypse"> <em
                                        class="fa fa-facebook-square" style="font-size: 1.5em;"> Official page in
                                    Facebook</em> </a></li>
                        <li class="list-group-item"><a href="https://vk.com/the_pit_kiev"> <em class="fa fa-vk"
                                                                                               style="font-size: 1.5em;">
                                    Official page in VK</em> </a></li>
                        <li class="list-group-item"><a href="https://www.facebook.com/groups/1483685125279609"> <em
                                        class="fa fa-facebook" style="font-size: 1.5em;"> Group for communication</em>
                            </a></li>
                    </ul>
                </div>
                <h3 style="font-family: 'Open sans', sans-serif;">You just got out of the bunker and didn't know what
                    are we talking about (phones, email , network)?</h3>
                <div style="font-family: 'Open sans', sans-serif; font-size: 1.5em;">Do not be upset ! There is another
                    option! Coordinates! 50.535906 , 30.832242 <br/> АAlso, you can see where we are on the map below.
                    <br/><br/></div>
            </div>
        </div>
    </div>
    <section id="mapSection" class="embed-container maps" style="margin-bottom: -50px;">
        <div id="map" style="width: 100%; display: block; height: 450px;pointer-events: none;
    position: relative;
    overflow: hidden;
    transform: translateZ(0px);
    background-color: rgb(229, 227, 223);">
            <iframe style="border: 0;"
                    src="https://www.google.com/maps/embed/v1/place?q=50.535906%2C%2030.832242&amp;key=AIzaSyCCylOs0NLUvkmvH8R2rsZ89WNmu6P_KTI"
                    width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </section>
@endsection