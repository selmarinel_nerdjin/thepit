@extends('the_pit::layouts/blank')
@section('title')
    The PIT | Main page
@endsection
@section('keywords')
    The PIT
@endsection
@section('description')
    The PIT
@endsection
@section('styles')
    <link href="https://fonts.googleapis.com/css?family=Mr+Dafoe|Reenie+Beanie" rel="stylesheet">
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('/css/loader.css')}}">
    <style>
        .b-popup{
            width: 100%;
            min-height: 100%;
            margin: 0;
            overflow: hidden;
            position: fixed;
            background-color: #282a2b;
            top: 0px;
            right: 0px;
            z-index: 50;
        }
        .b-popup .b-popup-content{
            margin: 10% auto 0px;
        }
        body {
            height: 100vh;
            display: flex;
            flex-wrap: wrap;
            background-color: #2a2b28;
            background-image: url("/images/bg-simple.png");
            z-index: -1;
        }

        footer {
            position: absolute;
            bottom: 0;
            height: 15vh;
            text-align: center;
            width: 100%;
        }

        footer img {
            width: 100px;
        }

        .middle {
            height: 80vh;
            display: block;
            width: 100%;
            background-color: #DC7A23;
            background-image: url("images/bg-fire.jpg");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            align-self: center;
            position: relative;
            overflow: auto;
            transition: .25s;
            background-blend-mode: luminosity;
        }

        .video {
            position: absolute;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: -1;
            opacity: .7;
        }

        .selector {
            color: #fafbf8;
            font-family: 'Reenie Beanie', cursive;
            text-shadow: 0 0 1px #0a0b08;
            margin-top: 10vh;
        }

        .selector h3 {
            font-size: 2em;
        }

        .language {
            margin-top: 10vh;
            font-size: 1.75rem;
            line-height: 1.75rem;
            font-family: "Arial Black";
            font-weight: bold;
            text-shadow: 1px 1px 10px #0a0b08;
        }

        .language a {
            color: #fafbf8 !important;
            text-decoration: none !important;
        }

        .language a:hover {
            color: #fffff8 !important;
        }

        @media (min-width: 480px) {
            .language {
                font-size: 2.75rem;
                line-height: 2.75rem;
            }
        }

        @media (min-width: 768px) {
            .language {
                font-size: 4.75rem;
                line-height: 4.75rem;
            }
        }

    </style>
@endsection

@section('content')
    <div class="middle">
        <div class="selector col-lg-8 col-lg-offset-2 text-center">
            <h3 class="text-center">Select your language</h3>
            <div class="col-sm-4 col-xs-12 language">
                <a href="{{route("site:index:en")}}" target="_self">
                    ENGLISH
                </a>
            </div>
            <div class="col-sm-4 col-xs-12 language">
                <a href="{{route("site:index:ru")}}" target="_self">
                    RUSSIAN
                </a>
            </div>
            <div class="col-sm-4 col-xs-12 language">
                <a href="{{route("site:index:ua")}}" target="_self">
                    UKRAINIAN
                </a>
            </div>
        </div>
    </div>

    <div class="b-popup" id="loading">
        <div class="b-popup-content">
            <div id="container">
                <div class="big-circle circle">
                    <div class="inr-circle circle"></div>
                    <div class="inr-circle circle"></div>
                    <div class="inr-circle circle"></div>
                </div>
                <div class="top-bar bar"></div>
                <div class="mid-bar bar"></div>
                <div class="btm-bar bar"></div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>

        $(window).on('load', function () {
            var $preloader = $('#loading'),
                $svg_anm   = $preloader.find('#container');
            $svg_anm.fadeOut();
            $preloader.delay(500).fadeOut('slow');
        });

        $(document).ready(function () {
            function getRandomArbitrary(max, min) {
                return Math.random() * (max - min) + min;
            }

            setInterval(function () {
                $(".middle").css({
                    "opacity": getRandomArbitrary(1, .8),
                });
            }, 100)
        })

    </script>
@endsection
