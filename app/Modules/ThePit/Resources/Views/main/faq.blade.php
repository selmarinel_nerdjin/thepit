@extends('the_pit::layouts/page')
@section('title','The PIT | Main page')
@section('keywords','postapocaliptic')
@section('description','postapocaliptic')
@section('css')
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
    <style>
        .well img {
            max-width: 100%;
        }

        .filter input {
            width: 100%;
            background-color: transparent;
            border: transparent;
            padding: 5px 10px;
            resize: vertical;
        }

        .filter {
            display: block;
            background-color: #2b2a28;
            padding-top: 5px;
            padding-bottom: 5px;
            margin-bottom: 5px;
            border-radius: 5px;
        }

        .btn-fucker-b {
            color: #2b2a28 !important;
        }

        .ask {
            color: #DC7A23;
            font-size: 1.5em;
        }

        .ask span {
            font-weight: 600;
        }

        .panel-default {
            box-shadow: 0 0 5px #2b2a28;
            border-color: #2a2a2a;
        }

        .panel-default > .panel-heading {
            background-color: #2b2a28;
            border-color: #2b2a28;
        }

        .panel-default > .panel-heading > button {
            font-size: 1.1em;
            color: #DC7A23;
            background-color: #2a2a2a;
            border-color: transparent;
            font-family: Bronx;
        }

        .panel-default .btn-link {
            font-size: 2em;
            color: #2b2a28;
            text-shadow: 0 0 10px #DC7A23;
            transition: .5s;
        }

        .panel-default .btn-link:hover {
            transform: scale(1.5);
            animation: wow 1s infinite alternate;
            -webkit-animation: wow 1s infinite alternate;
        }
        .btn-fucker-b{
            margin: 5px;
        }
        .form-group{
            margin: 0;
        }
    </style>
@endsection
@section('page')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title_main">F.A.Q.</h1>
            </div>
            <div class="col-xs-12 filter">
                {!! Form::open(['url' => route('site:faq'), 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data','method'=>'POST']) !!}
                @if($search)
                    <div class="col-xs-12">
                        <div class="ask bronx">You asked about <span>"{{$search}}"</span></div>
                    </div>
                @endif
                <div class="col-md-10 col-sm-9 col-xs-12">
                    {!! Form::text('query',"",['class'=>"form-control","placeholder"=>"Question?"]) !!}
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12">
                    {!! Form::submit("Search",['class'=>"btn btn-fucker btn-fucker-a btn-fucker-b"]) !!}
                </div>
                {!! Form::close() !!}
            </div>

            <div class="col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
                    @foreach($collection as $id=>$category)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading{{$category->first()->category->id}}">
                                <button data-toggle="collapse"
                                        data-target="#category{{$category->first()->category->id}}"
                                        class="btn btn-raised btn-default">
                                    <i class="fa {{$category->first()->category->icon}}"></i>
                                    {{$category->first()->category->name}}
                                </button>
                            </div>
                            <div class="panel-collapse collapse in"
                                 id="category{{$category->first()->category->id}}">
                                @foreach($category as $model)
                                    <div class="panel-body">
                                        <div class="col-xs-11">
                                            <h4>
                                                {{$model->title}}
                                                <small>
                                                    {!! $model->description !!}
                                                </small>
                                            </h4>
                                        </div>
                                        <div class="col-xs-1">
                                            <button class="btn btn-link" data-target="#answer{{$model->id}}"
                                                    aria-controls="answer{{$model->id}}" type="button"
                                                    data-toggle="collapse"
                                                    aria-expanded="true">
                                                <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="collapse @if($search) in @endif" id="answer{{$model->id}}">
                                                @foreach($model->answers as $answer)
                                                    <div class="well">
                                                        {!! $answer->description !!}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $.material.init()
    })

</script>
@endsection