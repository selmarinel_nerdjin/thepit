@extends('the_pit::layouts/page')
@section('title'){{$translate->title}} | The PIT @endsection
@section('keywords')The PIT, {{$translate->keywords}}@endsection
@section('description'){{$translate->description}}@endsection
@section('title'){{$translate->title}} | The PIT @endsection
@section('css')
    <link rel="stylesheet" href="vendor/flipclock/flipclock.css">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/page.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/uno.css')}}">
    <style>
        .f-video-editor {
            display: block;
            width: 100%;
        }

        video {
            max-width: 100%;
        }

        .well {
            background-color: transparent !important;
            padding: 10px !important;
        }
        .model_name{
            text-decoration: none;
            color: #0a0b08;
        }
        .model_info{
            text-decoration: none;
            color: #0a0b08;
        }
    </style>
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
@endsection
@section('page')
    <div class="container" id="app">
        <div class="row">
            <h1 class="page_title_main">{{$translate->title}}</h1>
            <div id="grid" class="row">
                <div class="col-lg-9 col-md-8 col-xs-12 well">
                    @if($translate->display_image)
                        <img src="{{$translate->getCover(450)}}" class="img-respomsive">
                    @endif
                    <div>{!! $translate->content !!}</div>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-12">
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading">--}}
                            {{--Time to beginning--}}
                        {{--</div>--}}
                        {{--<div class="panel-body">--}}
                            {{--<div class="your-clock"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <pie_quote :quote="load_quote">
                            </pie_quote>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="fb-page" data-href="https://www.facebook.com/ThePitPostApocalypse"
                             data-tabs="timeline"
                             data-small-header="false"
                             data-height="350"
                             data-adapt-container-width="true"
                             data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/ThePitPostApocalypse"
                                        class="fb-xfbml-parse-ignore"><a
                                        href="https://www.facebook.com/ThePitPostApocalypse">The Pit -
                                    Post-apocalyptic festival</a></blockquote>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <a href="{{route('site:news:article',['id'=>$article['id']])}}"
                           title="The PIT | {{$article['name']}}">
                            <div class="panel-heading">
                                <img src="{{$article['cover']}}" class="img-responsive">
                            </div>
                            <div class="panel-body">
                                <span class="model_name">{{$article['name']}}</span>
                                <span class="model_info">{{$article['info']}}</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('script')
    <div id="fb-root"></div>
    <script src="{{$app['pie_task_manager.assets']->getPath('js/vue.min.js')}}"></script>
    <script src="vendor/flipclock/flipclock.min.js"></script>
    @include("the_pit::layouts/block/vue/bloquote")
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9&appId=216155222107623";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
        $(function () {
            $.material.init();
        });

        new Vue({
            el: "#app",
            data: {
                load_quote: "",
                searchEnable: false,
                loginEnable: false,
                clock: null
            },
            created: function () {
                loadQuote(this);
            },
            mounted: function () {
                // Grab the current date
                var currentDate = new Date();

                // Set some date in the future. In this case, it's always Jan 1
                var futureDate = new Date("Jul 28 2017 19:00:00");

                // Calculate the difference in seconds between the future and current date
                var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;


                // Instantiate a coutdown FlipClock
                this.clock = $('.your-clock').FlipClock(diff, {
                    clockFace: 'DailyCounter',
                    countdown: true,
                    showSeconds: false
                });
            },
        });
    </script>
@endsection
