@extends('the_pit::layouts/page')
@section('page')
    <div class="container">
        <div class="row">
            <h1 class="page_title_main">Information</h1>
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <p><img alt="" class="fr-dii fr-fil" src="{{$app['pit']}}">The Pit - the only festival in the genre of
                    post-apocalypse on the territory of Ukraine. An event that combines the worlds of Mad Max, Fallout,
                    Metro 2033,
                    as well as many others together. Wasteland, survival, crazy games, the atmosphere of existence after
                    the end of
                    the world - that's what The Pit have prepared for you . Blast off from civilization, forget about
                    the existence
                    of pesky offices, endless crowds on the streets, trivial and boring home decor, primitive weekends.
                    Ready to
                    experience something extraordinary? Welcome to The Pit! </p>

                <h3>Promo <i class="fa fa-video-camera"></i></h3>

                <div class="embed-responsive embed-responsive-16by9"><span class="f-video-editor fr-fvn"
                                                                           contenteditable="false">
                        <video controls poster="/images/bg.jpg">
                            <source src="/video/catz.mp4" type="video/mp4">
                        </video>
        </span></div>

                <h3>What to expect in these days:</h3>

                <ul class="list-group">
                    <li class="list-group-item">HALF-DEAD MUSIC- the best underground bands for the atmosphere and
                        courage;
                    </li>

                    <li class="list-group-item">MAD DJs - with them you'll be dancing till the morning;</li>

                    <li class="list-group-item">ANTI-CASINO - place where you definitely won't get bored.;</li>

                    <li class="list-group-item">POST-NUCLEAR KITCHEN - insanely delicious and democratically;</li>

                    <li class="list-group-item">ATOMIC BAR - this you have not exactly tried;</li>

                    <li class="list-group-item">GLADIATOR FIGHTS - men and women;</li>

                    <li class="list-group-item">CHILLOUT- rest, hookah, Zen, Zohan party;</li>

                    <li class="list-group-item">POST-APOCALYPTIC DRESS CODE - come with your suit or purchase in the
                        shop at the
                        entrance.
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection