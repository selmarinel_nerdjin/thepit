@extends('the_pit::layouts/blank')
@section('title','The PIT | Main page')
@section('keywords','postapocaliptic')
@section('description','postapocaliptic')
@section('styles')
    <style>
        .navbar-transparent {
            background-color: transparent;
        }

        .fa-reorder {
            font-size: 2em;
            color: #DC7A23;
        }

        .page-scroll {
            color: #DC7A23;
            font-size: 1.5em;
            font-weight: bold;
            transition: .5s;
            background: rgba(0, 0, 0, .7);
            text-align: center;
        }

        .page-scroll:hover {
            color: #DC7A23;
            text-shadow: 2px 2px #2B2A28;
        }

        .try {
            font-size: .5em;
        }

        .navbar-fixed-top {
            padding: 0;
        }

        @media (min-width: 768px) {
            .try {
                font-size: 1em;
            }

            .navbar-fixed-top {
                padding: 50px;
            }

            .page-scroll {
                background-color: transparent !important;
            }

            .page-scroll:hover {
                transform: scale(1.2);
            }
        }

        html, body {
            height: 100%;
        }

        body {
            background-color: #2B2A28;
            font-weight: 100;
            margin: 0;
            padding: 0;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: auto;
        }

        .locale-flag img {
            filter: sepia(100%);
            -webkit-filter: sepia(100%);
            transition: .5s;
        }

        .locale-flag img:hover {
            filter: sepia(0%);
            -webkit-filter: sepia(0%);
            transform: scale(1.1);
        }

        @media (max-width: 768px) {
            .navbar-right {
                display: none;;
            }
        }
    </style>
    <style>
        .info {
            position: fixed;
            right: 0;
            display: none;
        }

        .info-nav {
            display: block;
        }

        @media (min-width: 786px) {
            .info {
                display: block;
            }

            .info-nav {
                display: none !important;
            }
        }

        .info img {
            max-height: 150px;
        }

        #info_section {
            overflow: scroll;
            height: 100vh;
            top: 0;
        }

        .information-text {
            font-size: 18px;
            text-align: left;
        }

        ::-webkit-scrollbar {
            width: 5px;
            background-color: rgba(43, 42, 40, 0.8);
        }

        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 10px;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
            background-color: #DC7A23;
        }
    </style>
@endsection
@section('content')
    @if(!Session::get('locale'))
        <section class="popup" style="display:block">
            <div class="container container-a2">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-sm-offset-0 col-xs-12"
                         style="padding: 25px">
                        <a href="{{route("site:index:en")}}" target="_self" class="locale-flag">
                            <img src="./images/en.png" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>
                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-sm-offset-0 col-xs-12"
                         style="padding: 25px">
                        <a href="{{route("site:index:ru")}}" target="_self" class="locale-flag">
                            <img src="./images/ru.png" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>
                </div>
            </div>
        </section>
    @endif
    <video autoplay loop muted class="bgvideo" id="bgvideo" poster="/images/bg.jpg">
        <source src="/video/video.mp4" type="video/mp4">
    </video>

    <nav class="navbar navbar-transparent navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <i class="fa fa-reorder"></i>
                </button>
                <span class="navbar-brand" href="#"></span>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @include('the_pit::layouts.block.menu_list')
                    <li class="info-nav info-button-popup"><a class="page-scroll bronx">Info</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <a href="{{route("site:index:en")}}" class="page-scroll lang">en</a>
                    <a href="{{route("site:index:ru")}}" class="page-scroll lang">ru</a>
                    <a href="{{route("site:index:ua")}}" class="page-scroll lang">ua</a>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container container-a1">
        <div class="content">
            <div class="title col-xs-10 col-xs-offset-2">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <img src="{{$app['big_pit']}}" width="190" class="img-responsive">
                </div>
                <div class="col-xs-12" style="font-size: 18px">
                    {{(Request::user())?Request::user()->getFullName():''}}
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12 try">
                    TRY TO SURVIVE
                </div>
                <div class="col-xs-12">
{{--                    <a href="{{route("concert:ua")}}" target="_blank" class="btn btn-fucker"--}}
                    <a href="{{\App\Modules\PitPayments\Http\Controllers\PaymentController::CONCERT_UA}}" target="_blank" class="btn btn-fucker"
                       id="buy">{{trans('the_pit::main.body_buy')}}</a>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-transparent navbar-fixed-bottom">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-right" style="padding-right: 100px">
                    @include("the_pit::layouts/block/links")
                </ul>
            </div>
        </div>
    </nav>

    <section class="info">
        <a class="info-button-popup">
            <img src="/images/info.png" alt="info">
        </a>
    </section>
    <section class="popup" id="buy_section">
        <div class="container container-a2">
            <div style="width: 100%;
    text-align: center;">
                <div class="title">
                    <div class="text-center">
{{--                        <a href="{{route("concert:ua")}}" target="_blank" style="display: inline-block;">--}}
                        <a href="{{\App\Modules\PitPayments\Http\Controllers\PaymentController::CONCERT_UA}}" target="_blank" style="display: inline-block;">
                            <img src="https://www.concert.ua/img/logo.png" class="img-responsive"
                                 alt="Concert.ua ThePIT">
                        </a>
                    </div>
                    <span class="btn btn-fucker" id="close_popup"><i class="fa fa-times"></i> Close</span>
                </div>
            </div>
        </div>
    </section>
    <section class="popup" id="info_section">
        <div class="container">
            <div style="width: 100%;
    text-align: center;">
                <div class="title">
                    <h1>About festival</h1>
                    <div class="information-text">
                        Кодекс:<br/>
                        1. Любой находящийся здесь автоматически соглашается с тем, что добровольно пошел на то, чтоб
                        сделать свою жизнь другой, и не имеет никаких претензий к Совету.<br/>
                        2. Любой находящийся здесь забывает о любой вражде и конфликтах (политика, религия), оставляя их
                        за пределами территории The Pit, вообще мы любим и уважаем друг-друга.<br/>
                        3.Любой находящийся здесь имеет право попросить о помощи у других, и сам обязуется оказывать
                        помощь, если это в его силах.<br/>
                        <br/>
                        Правила:<br/>
                        1. Уважать, мать его, Совет! И всё что он доносит до вас. Не забывать Кодекс.<br/>
                        2. Любите ближнего своего! Мы все выжившие и ценим дружеские отношения. Это важно. Большой стаей
                        легче выжить.<br/>
                        3. Не трогать Ямных Отродий в принципе, и других людей, если они не хотят этого.<br/>
                        4. Токсических, огнеопасных, взрывоопасных веществ не проносить.<br/>
                        5. Холодняк - нахер! Если у вас это часть костюма, то залепить все режущие кромки армированным
                        скотчем. И не размахивать во все стороны оголённым оружием.<br/>
                        6. Любые реплики огнестрела должны быть только репликами. Никаких акумов и шаров в приводах,
                        никаких патронов в револьверах.<br/>
                        7. Никому не давать своё оружие.<br/>
                        8. Настоятельно НЕ рекомендуем пить что-то из посуды незнакомых вам людей.<br/>
                        9. Свой алкоголь только в антуражных ёмкостях. И никакой стеклотары.<br/>
                        10. Не шариться в закрытых зонах, если вас не ведут через них сами организаторы.<br/>
                        11. Участвуйте в конкурсах, играх, казино на свой страх и риск. Но если вы решили в чём-то
                        участвовать, то участвуйте до конца. Вы всегда можете узнать о рисках заранее.<br/>
                        12. Никаких споров и ссор — всё это остается за территорией Города. Сюда входят как религиозные
                        убеждения, политические и личные. В общем все.<br/>
                        13. Если вы нашли человека в неадекватном состоянии, который не помнит как двигаться и
                        разговаривать, сообщите об этом охране или организаторам.<br/>
                        14. Никакого экстремизма на полигоне. Ни кричалок, ни флагов, ни других символов. У нас будут
                        гости из разных стран. Если вас это не устраивает - не приходите. Мы фестиваль друзей.<br/>
                        15. Не разводить костры без разрешения. Разрешение можно получить от главного по пожарищам и и
                        поджогам. Связаться с Пироманом можно через организаторов, охрану или персонал фестиваля.<br/>
                        16. За территорией фестиваля Дикая Пустошь, там вы можете надеяться только на себя. Настоятельно
                        рекомендуем не покидать Город в костюмах, не надо пугать цивилов.<br/>
                        17. На полигон не допускаются люди в состоянии сильного алкогольного или наркотического
                        опьянения. Нам не нужны неадекваты.<br/>
                        18. Не мусори! Пустошь не простит! Увидел мусор — поднял и отнёс до мусорного пакета.<br/>
                        19. Следите за своим здоровьем. Если у вас есть хронические проблемы, например астма —
                        позаботьтесь об личной аптечке.<br/>
                        20. Мы не несём ответственности за ваши вещи.<br/>
                        21. Соблюдайте законы Украины!<br/>
                        22. Не приветствуются собаки в Городе. У них есть зубы, и они ими пользуются, что бы вы там не
                        рассказывали.<br/>
                        23. Любая коммерческая деятельность должна быть оговорена с Советом.<br/>
                        24. За нарушение каждого правила вам дают предупреждение. Если вы его игнорируете — вы выходите
                        с территории The Pit без возврата средств и без входного браслета.<br/>
                    </div>
                    <span class="btn btn-fucker" id="close_info_popup"><i class="fa fa-times"></i> Close</span>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            var context = new window.AudioContext(); //
            var buffer, source, destination;
            var loadSoundFile = function (url) {
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);
                xhr.responseType = 'arraybuffer';
                xhr.onload = function (e) {
                    context.decodeAudioData(this.response,
                        function (decodedArrayBuffer) {
                            buffer = decodedArrayBuffer;
                        }, function (e) {
                            console.log('Error decoding file', e);
                        });
                };
                xhr.send();
            }

// функция начала воспроизведения
            var play = function () {
                if (!context.createGain)
                    context.createGain = context.createGainNode;
                this.gainNode = context.createGain();
                source = context.createBufferSource();
                source.buffer = buffer;
                source.connect(this.gainNode);
                this.gainNode.connect(context.destination);
                if (!source.start)
                    source.start = source.noteOn;
                source.start(0);
                gainNode.gain.value = 0.2
            }

// функция остановки воспроизведения
            var stop = function () {
                source.stop(0);
            }

            loadSoundFile('/video/1.mp3');
            /**
             * Todo Temporary code
             */

            $('#buy').on('click', function () {
                $('#buy_section').fadeIn('slow');
            });
            $("#close_popup").on('click', function () {
                $('#buy_section').fadeOut('slow');
            });
            $(".info-button-popup").on('click', function () {
                $("#info_section").fadeIn('slow');
                play();
            });
            $("#close_info_popup").on('click', function () {
                $("#info_section").fadeOut('slow');
                stop();
            });

        });

    </script>
@endsection



