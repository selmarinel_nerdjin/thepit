<?php
return [
    'menu_info' => 'Info',
    'menu_news' => 'News',
    'menu_contacts' => 'Contacts',
    'menu_media' => 'Media',
    'body_buy' => 'Buy Ticket',
    'news_all' => 'All News',
    'news_filter' => 'Filter',
    'news_category_select' => 'Select Category',
    'news_find_button' => 'Find',
    'news_popular' => 'Popular news',
    'page_sponsors' => 'Sponsors & Partners',
    'page_send_button' => 'Send',
    'page_subsribe' => 'Subscribe',
    'page_close' => 'close',
    'page_auth' => 'Authorization',
    'page_leave_comment' => 'Leave a comment',
    'page_comments' => 'Comments',
    'page_more' => 'More',
    'page_login' => 'Login',
    'page_see_more' => 'See more',
    'form_login_email' => 'login/email',
    'form_password' => 'password',
    'page_details' => 'Details',
    'auth' => "Authorization",
    'page_categories' => "Categories",
    'page_tags' => "Tags",
];