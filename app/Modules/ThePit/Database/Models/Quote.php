<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.10.2016
 * Time: 13:56
 */

namespace App\Modules\ThePit\Database\Models;

use App\Modules\PieBase\Database\Models\Base;

class Quote extends Base
{
    protected $table = 'quotes';

    protected $fillable = [
        'quote',
    ];
}