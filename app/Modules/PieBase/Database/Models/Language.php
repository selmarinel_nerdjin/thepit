<?php

namespace App\Modules\PieBase\Database\Models;

class Language extends Base {
    public $timestamps = false;

    protected $table = 'languages';
    protected $fillable = array(
        'name',
        'short_name',
        'status'
    );
}