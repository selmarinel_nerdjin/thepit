<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\PieBase\Database\Models\Role;
use App\Modules\PieBase\Database\Models\User;

class InstallPB extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('password_resets');
		Schema::dropIfExists('roles');

		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->integer('file_cover_id');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('login')->unique();
			$table->tinyInteger('status')->default(0);
			$table->tinyInteger('role_id')->unsigned()->default(0);
			$table->rememberToken();
			$table->timestamps();
		});
		Schema::create('password_resets', function (Blueprint $table) {
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at');
		});
		Schema::create("roles", function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->tinyInteger('is_admin')->default(0)->unsigned();
			$table->tinyInteger('status')->default(1)->unsigned();
		});
		Schema::create('tokens', function (Blueprint $table) {
			$table->char('token', 32)->primary();
			$table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
		Schema::create("files", function (Blueprint $table) {
			$table->increments("id");
			$table->string('url', 255);
			$table->tinyInteger('type')->default(0);
		});

		Role::create(['id' => 1,  'name' => 'User']);
		Role::create(['id' => 2,  'name' => 'Moderator', 'is_admin'=> 1]);
		Role::create(['id' => 3,  'name' => 'Admin', 'is_admin' => 1]);
		User::create([
			'email' 		=> 'selmarinel@gmail.com',
			'login' 		=> 'selmarinel',
			'first_name'	=> 'God',
			'last_name'		=> 'Admin',
			'password' 		=> '1q2w3e4r',
			'role_id'			=> 3
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('password_resets');
		Schema::dropIfExists('roles');
		Schema::dropIfExists('tokens');
		Schema::dropIfExists('files');
	}
}
