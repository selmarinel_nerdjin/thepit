<?php

namespace App\Modules\PieBase\Http\Controllers\Admin;

use App\Modules\PieArticle\Http\Services\Article;
use App\Modules\PieBase\Http\Services\User;
use App\Modules\PieComments\Http\Services\Comments;

class MainController extends Controller{

    public function index(){
        $articles = new Article();
        $users = new User();
        $articles->getDiscussed();
        $comments = new Comments();
        return view($this->getViewRoute(),[
            'popular' => $popular = $articles->getPopular($this->config->findBySlug('admin_padding')),
            'discussed' => $articles->getDiscussed($this->config->findBySlug('admin_padding')),
            'users' => $users->getLastUsers($this->config->findBySlug('admin_padding')),
            'comments' => $comments->getNewest(10)
        ]);
    }
}
