<?php
namespace App\Modules\PieBase\Http\Middleware;

use App;
use Closure;
use Illuminate\Support\Facades\Session;

class LangMiddleware{
    public function handle($request, Closure $next)
    {
        if($request->getHttpHost() == config('app.url')) {
            return $next($request);
        }

        return $next($request);
    }
}