<?php
namespace App\Modules\PieBase\Http\Middleware;

use App;
use Closure;
use App\Modules\PieBase\Http\Services\User as Service;
use Illuminate\Support\Facades\Session;

class Ware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $service = new Service();
        $user = $service->authByToken($request->session()->get('token'));
        $lang = Session::get('locale', function() { return 'en'; });
        App::setLocale($lang);
        if($user) {
            $request->setUserResolver(function() use ($service){ return $service->getModel();});
            return $next($request);
        }
        return $next($request);
    }
}
