<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
    Route::get('/pie', function () {
        return view('pie_base::info', ['module' => 'Basic Module']);
    });
    Route::group(['prefix' => 'admin', 'middleware' => ['webAdmin']], function () {
        $asPrefix = 'admin';
        Route::group(['middleware' => 'AdminAuth'], function () use ($asPrefix) {
            Route::any('login', ['as' => $asPrefix . ':login', 'uses' => 'Admin\UserController@login']);
            Route::get('logout', ['as' => $asPrefix . ':logout', 'uses' => 'Admin\UserController@logout']);
        });
        Route::group(['middleware' => 'JournalistAuth'], function () use ($asPrefix) {
            Route::get('/', ['as' => $asPrefix . ':index', 'uses' => 'Admin\MainController@index']);
        });
        Route::group(['middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
            Route::group(['prefix' => 'users'], function () use ($asPrefix) {
                $adAction = ':users';
                Route::get('/', ['as' => $asPrefix . $adAction . ':index', 'uses' => 'Admin\UserController@index']);
                Route::get('/{id}/delete', ['as' => $asPrefix . $adAction . ':delete', 'uses' => 'Admin\UserController@delete']);
                Route::any('/add', ['as' => $asPrefix . $adAction . ':add', 'uses' => 'Admin\UserController@add']);
                Route::any('/{id}/edit', ['as' => $asPrefix . $adAction . ':edit', 'uses' => 'Admin\UserController@edit']);
                Route::get('/{id}/active', ['as' => $asPrefix . $adAction . ':active', 'uses' => 'Admin\UserController@active']);
            });
            Route::group(['prefix' => 'roles'], function () use ($asPrefix) {
                $adAction = ':roles';
                Route::get('/', ['as' => $asPrefix . $adAction . ':index', 'uses' => 'Admin\RoleController@index']);
                Route::get('/{id}/delete', ['as' => $asPrefix . $adAction . ':delete', 'uses' => 'Admin\RoleController@delete']);
                Route::get('/{id}/active', ['as' => $asPrefix . $adAction . ':active', 'uses' => 'Admin\RoleController@active']);
                Route::any('/add', ['as' => $asPrefix . $adAction . ':add', 'uses' => 'Admin\RoleController@add']);
                Route::any('/{id}/edit', ['as' => $asPrefix . $adAction . ':edit', 'uses' => 'Admin\RoleController@edit']);
            });
        });
    });
});