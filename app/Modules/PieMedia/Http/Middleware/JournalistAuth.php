<?php
namespace App\Modules\PieMedia\Http\Middleware;

use App\Modules\PieBase\Database\Models\User;
use Closure;
use App\Modules\PieBase\Http\Services\User as Service;

class JournalistAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $service = new Service();
        /**
         * @var User $user
         */
        $user = $service->authByToken($request->session()->get('token'));
        if($user && ($user->isAdmin() || $user->isJournalist())) {
            $request->setUserResolver(function() use ($service){ return $service->getModel();});
            return $next($request);
        }
        if ($request->ajax()) {
            return response('Unauthorized.', 404);
        }
        return redirect(route('admin:login'));
    }
}
