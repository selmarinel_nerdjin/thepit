<?php
namespace App\Modules\PieMedia\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class Media extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieMedia\Database\Models\Media';
    protected $hasStatus = true;

    public function paginateMedia($paginate = 6)
    {
        $collection = $this->getModel()->query()->active()->createdBy()->paginate($paginate);
        return $collection->setCollection($collection->getCollection()->map(function($model){
            return $model->getInfo();
        }));
    }

    CONST TYPES = [
        '1' => "Photo",
        '2' => "Video",
        '4' => 'Podcast'
    ];

    public function prepareSelectTypes()
    {
        $types = [];
        foreach (self::TYPES as $key=>$type) {
            $types[$key] = $type;
        }
        return $types;
    }
}