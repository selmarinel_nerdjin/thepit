<?php
namespace App\Modules\PieMedia\Http\Services;

use App\Modules\PieBase\Http\Services\Files;

class File extends Files
{
    protected $modelName = 'App\Modules\PieMedia\Database\Models\File';
    protected $orderBy = [];
    protected $hasStatus = false;
}