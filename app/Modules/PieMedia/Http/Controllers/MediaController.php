<?php
namespace App\Modules\PieMedia\Http\Controllers;

use App\Modules\PieMedia\Database\Models\LinksToMedia;
use App\Modules\ThePit\Http\Controllers\BasicController;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Session;

/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 24.07.2016
 * Time: 10:46
 */
class MediaController extends BasicController
{
    protected $prefix = 'site:media';
    protected $moduleName = 'pie_media';
    protected $serviceName = 'App\Modules\PieMedia\Http\Services\Media';

    private function setLocale()
    {
        $locale = Session::get('locale', function () {
            return 'en';
        });
        App::setLocale($locale);
    }

    public function loadContent(Request $request, $id)
    {
        $link = LinksToMedia::query()->find($id);
        if (!$link) {
            abort(404);
        }
        $this->setLocale();
        return view($this->getViewRoute('content'), [
            'link' => "http://" . $_SERVER['HTTP_HOST'] . $link->link,
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);
    }

    public function loadPhoto(Request $request, $id)
    {
        $media = $this->service->getOne($id);
        if (!$media) {
            abort(404);
        }
        $this->setLocale();
        return view($this->getViewRoute('photo'), [
            'model' => $media,
            'lang' => 'en',
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);
    }
}