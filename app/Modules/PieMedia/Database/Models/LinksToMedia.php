<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 24.07.2016
 * Time: 22:57
 */

namespace App\Modules\PieMedia\Database\Models;


use App\Modules\PieBase\Database\Models\Base;

class LinksToMedia extends Base
{
    public $timestamps = true;
    protected $table = 'links_for_media';

    protected $fillable = array(
        'media_id',
        'link',
    );

    /**
     * @param string $value
     * @return string
     */
    public function saveYouTubeLink($value)
    {
        if (strripos($value, 'v=')) {
            $uid = explode('?v=', $value);
            if (!empty($uid) && $uid[1]) {
                return 'https://www.youtube.com/embed/' . $uid[1];
            }
        }
        return $value;
    }

    public function setLinkAttribute($value)
    {
        if (strripos($value, 'youtube.com')) {
            $this->attributes['link'] = $this->saveYouTubeLink($value);
        } else {
            $this->attributes['link'] = $value;
        }
    }

    /**
     * @return bool
     */
    public function isYouTubeVideo($value = '')
    {
        if(!$value){
            $value = $this->link;
        }
        if(strripos($value, 'youtube.com')){
            return true;
        }
        return false;
    }
}