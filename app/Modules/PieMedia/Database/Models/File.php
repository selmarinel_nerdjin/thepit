<?php
namespace App\Modules\PieMedia\Database\Models;

use App\Modules\PieBase\Database\Models\File as BaseFile;

class File extends BaseFile
{
    protected $path = 'media/';
}
