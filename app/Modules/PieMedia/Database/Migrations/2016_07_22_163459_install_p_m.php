<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\PieBase\Database\Models\Role;

class InstallPM extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('article_id')->nullable();
			$table->string('name');
			$table->integer('file_id')->references('id')->on('files');
			$table->timestamps();
		});
		Schema::table('roles',function(Blueprint $table){
			$table->boolean('is_journalist')->default(0);
		});
		Role::create([
			'id' => 4,
			'name' => 'Journalist',
			'is_admin' => 0,
			'is_journalist' => 1
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('media');
	}
}
