@extends('the_pit::layouts/page')
@section('title')
    {{$model->name}} | The PIT
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" media="all"
          href="{{$app['pie_media.assets']->getPath('/css/jgallery.min.css?v=1.5.6')}}"/>
    <style>
        .cover {
            position: fixed;
            top: 0;
            left: 0;
            z-index: -1;
            -webkit-filter: blur(5px) opacity(25%) sepia(1) brightness(1.5);
            filter: blur(10px) opacity(25%) grayscale(1) brightness(2.1);
            width: 100%;
        }

        .jgallery {
            background: rgba(0, 0, 0, .6) !important;
        }

        .jgallery-container {
            background: transparent !important;
        }
    </style>
@endsection
@section('page')
    <div style="margin: 0 auto; display: table;">
        <img src="{{$model->getFile(650)}}" class="img-responsive cover">
    </div>
    <div class="container">
        <div class="row">
            @if($model->article)
                <?php $translate = $model->article->getTranslateArticle($lang); ?>
                <div class="col-xs-12">
                    <h1 class="page_title_main">{{$translate['name']}}</h1>
                </div>
                <div class="col-lg-offset-1 col-lg-10">
                    <div class="page-header">
                        <h1>{{$translate['info']}}</h1>
                        <span class="article_date" data-text="{{$model->article->created_at}}" style="float: right;margin-top: -10px;">
                            {{$model->article->created_at}}
                        </span>
                    </div>
                    <div style="margin: 0 auto; display: table;">
                        <img src="{{$model->article->getCover(650)}}" class="img-responsive" style="max-height: 450px;">
                        <div class="">
                            <a href="{{route('site:news',['category'=>$model->article->category->id])}}"
                               class="tag text-left"
                               style="background-color: {{$model->article->category->color}};">
                                {{$model->article->category->name}}
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <a href="{{route('site:news:article',['id'=>$model->article->id])}}">
                            <span class="btn btn-fucker btn-fucker-a">{{trans('the_pit::main.page_details')}}</span>
                        </a>
                    </div>
                </div>
            @else
                <div class="col-xs-12">
                    <h1 class="page_title_main">{{$model->name}}</h1>
                </div>
                <div class="page-header">
                    <h1>&nbsp;</h1>
                    <span class="article_date" data-text="{{$model->created_at}}" style="float: right;margin-top: -10px;">
                        {{$model->created_at}}
                    </span>
                </div>
            @endif
        </div>
    </div>
    <div class="clearfix"></div>
    <section id="gallerySection">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2><i class="fa fa-comments"></i> {{trans('pie_base::main.photo_gallery')}}</h2>
                    <div id="gallery">
                        @if($model->files)
                            @foreach($model->files as $file)
                                <a href="{{$file->getCover()}}"><img src="{{$file->getCover()}}" class="img-responsive"></a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script src="{{$app['pie_media.assets']->getPath('/js/jgallery.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#gallery').jGallery({
                mode: 'standard',
                height: '550px',
                canChangeMode: true,
                canZoom: true,
                maxMobileWidth: 767,
                thumbnails: true,
                swipeEvents: true,
                width: '100%',
                preloadAll: false,
                tooltipFullScreen: 'Full screen',
                browserHistory: true,
                hideThumbnailsOnInit: true
            });
        });
    </script>
@endsection