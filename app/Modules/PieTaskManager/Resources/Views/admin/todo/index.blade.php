@extends('pie_base::admin.layouts.default')
@section('head')
    <link rel="stylesheet" href="{{$app['pie_task_manager.assets']->getPath('css/jquery-confirm.min.css')}}"
          xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('js/lib/trumbowyg/ui/trumbowyg.min.css')}}">
    <style xmlns:v-bind="http://www.w3.org/1999/xhtml">
        #todo {
            width: 100%;
            display: table;
        }

        .list-group .list-group-item .row-action-primary i {
            width: 32px;
            height: 32px;
            line-height: 32px;
            font-size: 16px;
        }

        .list-group .list-group-item .row-content {
            min-height: inherit;
        }

        .task {
            display: inline-block;
            width: 100%;
            background-color: #020031;
            color: #fff;
            margin: 5px;
            box-shadow: 0 0 5px #000;
            padding: 5px;
            border-radius: 2px;
            transition: .5s;
        }

        .task:hover {
            box-shadow: 0 0 10px #2b282a;
        }

        .task img {
            float: left;
            padding-right: 5px;
            max-width: 150px;
        }

        .task time {
            display: block;
            width: 100%;
            text-align: right;
            padding: 5px;
        }

        .task priority {
            display: block;
            float: right;
        }

        .task priority i {
            font-size: 1.5em;
        }

        #list, #description {
            padding: 10px
        }

        .list-group .list-group-item .row-content .least-content {
            bottom: 0px;
            top: auto;
        }

        .list-group .list-group-item {
            padding: 0
        }

        .bg-success {
            background-color: #4caf50 !important;
        }

        .bg-info {
            background-color: #03a9f4 !important;
        }

        .bg-warning {
            background-color: #ff5722 !important;
        }

        .bg-danger {
            background-color: #f44336 !important;
        }

        img.circle {
            border-color: none;
            border-width: 0;
            border-style: solid;
            box-sizing: border-box;
        }

        #edit {
            position: absolute;
            display: none;
            top: 5px;
            right: 15px;
        }

        #taskBody:hover #edit {
            display: block;
        }

        @-webkit-keyframes wow {
            from {
                transform: scale(1);
            }
            to {
                transform: scale(2);
            }
        }

        .fa-hand-o-left {
            animation: wow 1s infinite alternate;
            -webkit-animation: wow 1s infinite alternate;
        }

        .row-picture time {
            position: absolute;
            right: 5px;
            top: 5px;
        }

        .form-group-sm {
            margin-top: 0;
        }

        .fade-enter-active, .fade-leave-active {
            transition: opacity .5s
        }

        .fade-enter, .fade-leave-active {
            opacity: 0
        }

        #todo[v-cloack] {
            display: none;
        }

        .brand {
            font-size: 2em;
            font-family: Roboto;
            margin: 0 5px;
        }

        .brand span {
            padding: 5px;
            display: inline-flex;
            width: 40px;
            height: 40px;
            text-align: center;
            background-color: #8e44ad;
            color: #fff;
            border-radius: 50%;
            justify-content: center;
            align-items: center;
            text-shadow: 0 0 10px #000;
            box-shadow: 0 0 2px #000;
        }

        .chip {
            box-sizing: border-box;
            display: flex;
            font-family: Roboto, sans-serif;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            cursor: pointer;
            text-decoration: none;
            margin: 4px;
            padding: 0;
            outline: none;
            font-size: inherit;
            font-weight: inherit;
            background-color: rgb(224, 224, 224);
            border-radius: 16px;
            white-space: nowrap;
        }

        .chip div {
            color: rgb(255, 255, 255);
            display: inline-flex;
            align-items: center;
            justify-content: center;
            font-size: 16px;
            margin-right: -4px;
            user-select: none;
        }

        .chip span {
            color: rgba(0, 0, 0, 0.870588);
            font-size: 14px;
            font-weight: 400;
            line-height: 40px;
            padding-left: 12px;
            padding-right: 12px;
            white-space: nowrap;
            user-select: none;
        }

        .list-group-item-heading {
            font-size: 12px;
        }

        .my_icon {
            padding: 5px;
            border-radius: 50%;
            color: #fff;
            font-size: 14px;
        }
    </style>
@endsection
@section('content')
    <section id="todo" v-cloak>
        <div class="container">
            <div class="panel panel-heading">
                <ul class="nav nav-pills">
                    <li v-on:click="toggleList" class="btn btn-fab btn-fab-mini btn-default">
                        <i class="material-icons">menu</i>
                    </li>
                    <li>
                        <span class="brand">TO<span>DO</span></span>
                    </li>
                    <li>
                        <div class="form-group form-horizontal" style="margin: 0; padding: 0;">
                            <input type="text" v-model="searchString" class="form-control" placeholder="Find Task">

                        </div>
                    </li>
                    <li>
                        <div class="btn btn-success chip" type="button" v-on:click="newTask">
                            <div>
                                <button class="btn btn-fab btn-fab-mini btn-success">
                                    <i class="material-icons">add circle</i>
                                </button>
                            </div>
                            <span>Add Task</span>
                        </div>
                    </li>
                    <li>
                        <div type="button" class="chip" v-on:click="togglePanels()" v-bind:key="panels">
                            <div class="btn btn-fab btn-fab-mini"
                                 v-bind:class="(!panels)? 'btn-danger' : 'btn-info' ">
                                <transition name="fade" mode="out-in">
                                    <small>@{{ togglePanel }}</small>
                                </transition>
                            </div>
                            <span>
                                Panels
                            </span>
                        </div>
                    </li>
                    <li>
                        <div type="button" class="chip" v-on:click="mineTasks()">
                            <div class="btn btn-fab btn-fab-mini btn-warning">
                                M
                            </div>
                            <span>
                                @{{(mine)?"All tasks": "Only my tasks" }}
                            </span>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
        <div class="panel panel-heading container-fluid" v-if="selected.id">
            <h3>
                <i class="material-icons">assignment</i> @{{ selected.name }}
            </h3>
            Priority:
            <priority v-bind:class="'label-'+selected.color" class="label">
                <i class="fa fa-arrow-circle-o-up"></i>
                @{{ selected.priority }}
            </priority>
            <section v-if="(selected.user_id == {{Request::user()->id}})">
                <label>Actions: </label>
                <button class="btn btn-raised btn-sm btn-info" v-on:click="startTask(selected)"
                        v-if="selected.state == 0">
                    start
                </button>
                <button class="btn btn-raised btn-sm btn-warning" v-on:click="stopTask(selected)"
                        v-if="selected.state == 1 ">
                    stop
                </button>
                <button class="btn btn-raised btn-sm btn-success" v-on:click="completeTask(selected)"
                        v-if="selected.state == 1">
                    complete
                </button>
                <button class="btn btn-raised btn-sm btn-danger" v-on:click="reOpenTask(selected)"
                        v-if="selected.state == 2">
                    Re open
                </button>
            </section>
        </div>

        <div v-bind:class="[ toggle ? 'col-lg-2 col-md-2 col-sm-2 col-xs-2' :'col-xs-1' ]" class="panel panel-heading">
            <ul id="list" class="list-group">
                <li v-for="task in FilteredTasks" class="list-group-item">
                    <div class="list-group-item" v-on:click="openTask(task)">
                        <h5>
                            <i class="material-icons my_icon" v-bind:class="'bg-'+task.color" style="">
                                @{{ task.state_class }}
                            </i>
                            <b>@{{task.name}}</b>
                        </h5>
                        <h5 v-if="toggle">
                            @{{ task.info }}
                            <br/>
                            <label class="label label-default">
                                @{{ task.user_name}}
                            </label>
                        </h5>
                    </div>
                </li>

            </ul>
        </div>
        <div v-bind:class="[ toggle ? 'col-lg-10 col-md-10 col-sm-10 col-xs-10' :'col-xs-11' ]">
            <div class="panel-body row" id="taskBody">
                <div class="row"
                     v-bind:class="[ (panels && selected.id) ? 'col-lg-9 col-md-6 col-sm-12 col-xs-12' : 'col-xs-12']">
                    <div class="panel panel-heading" id="description" v-html="selected.description"></div>
                    <button v-if="({{Request::user()->role->isAdminRules()}} || selected.user_id == {{Request::user()->id}}) && selected.id"
                            class="btn btn-info btn-fab btn-fab-mini"
                            id="edit" v-on:click="editDescription(selected)">
                        <i class="material-icons">edit</i>
                    </button>
                    <div class="col-xs-12" v-if="selected.id">
                        <h4>Comments</h4>
                        <hr>
                        <div class="list-group" v-for="comment in selected.comments">
                            <div class="list-group-item">
                                <div class="row-picture">
                                    <img class="circle" v-bind:src="comment.user_avatar" alt="icon">
                                    <time v-bind:data-text="comment.created_at">
                                        <var></var>
                                    </time>
                                </div>
                                <div class="row-content">
                                    <h4 class="list-group-item-heading">@{{ comment.user_name }}</h4>

                                    <p class="list-group-item-text">@{{ comment.comment }}</p>
                                </div>
                            </div>
                            <div class="list-group-separator"></div>
                        </div>
                        <form class="form-horizontal well well-sm">
                            <div>
                                <img class="img-circle" src="{{Request::user()->getCover(50)}}" height="50px"
                                     width="50px" alt="icon">
                                {{Request::user()->getFullName()}}
                            </div>
                            <div class="form-group form-group-sm">
                                <p class="list-group-item-text">
                                    <textarea class="form-control" rows="3" v-model="comment">

                                    </textarea>
                                </p>
                            </div>
                            <div class="text-right">
                                <transition name="fade">
                                    <button type="button" class="btn btn-raised btn-default" v-if="comment"
                                            v-on:click="addComment(selected)">
                                        Comment
                                    </button>
                                </transition>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-heading" v-if="selected.id && panels">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <img class="circle img-responsive" v-bind:src="selected.user_avatar" alt="icon"
                                     width="50">
                                <h4 class="list-group-item-heading">Assigned to:</h4>
                                <p class="list-group-item-text"> @{{ selected.user_name }}</p>
                            </div>
                            <div class="list-group-item">
                                <button class="btn btn-success btn-xs btn-raised" v-on:click="assignTask(selected)"
                                        v-bind:class="[(selected.user_id != {{Request::user()->id}})?'':'hidden']">
                                    Assign to me
                                </button>
                                <button class="btn btn-danger btn-xs btn-raised" v-on:click="declineTask(selected)"
                                        v-bind:class="[(selected.user_id == {{Request::user()->id}})?'':'hidden']">
                                    Decline
                                </button>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Dates
                            </div>
                            <div class="panel-body">
                                <time>
                                    @{{ (selected.state == 2) ? "Done within " + selected.progress : selected.progress}}
                                </time>
                                <hr/>
                                <time v-bind:data-text="selected.due_date_at">
                                    Due: <var></var>
                                </time>
                                <br/>
                                <time v-bind:data-text="selected.started_at">
                                    Started at: <var></var>
                                </time>
                                <br/>
                                <time v-bind:data-text="selected.completed_at">
                                    Completed at: <var></var>
                                </time>
                                <hr/>
                                <time v-bind:data-text="selected.created_at">
                                    Created at: <var></var>
                                </time>
                                <br/>
                                <time v-bind:data-text="selected.updated_at">
                                    Updated at: <var></var>
                                </time>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="t"></div>
@endsection
@section('scripts')
    <script src="{{$app['pie_task_manager.assets']->getPath('js/vue.min.js')}}"></script>
    <script src="{{$app['pie_task_manager.assets']->getPath('js/jquery-confirm.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/moment.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/trumbowyg.min.js')}}"></script>
    <script>

        function momentoForDates() {
            $.each($("time"), function (i, v) {
                var time = moment($(v).attr('data-text')).locale('<?=(App::getLocale() == 'ru') ? App::getLocale() : 'en'?>');
                if (time.isValid()) {
                    $(v).find("var").text(time.format('DD MMMM YYYY, HH:mm:ss'));
                } else {
                    $(v).find("var").text('Never');
                }
            });
        }

        function loadConfiguration(self) {
            ($.ajax({
                url: "{{route('admin:todo:api:configuration')}}",
                type: "GET",
                dataType: "json",
                success: function (response) {
                    self.panels = (response.config.panels == 1);
                    self.toggle = (response.config.list == 1);
                },
                error: function () {
                    $.alert({
                        type: "red",
                        title: "Error",
                        content: "Something went wrong. Your config wasn't load"
                    })
                }
            }))
        }
        function loadData(self) {
            $.ajax({
                url: "{{route('admin:todo:api:load')}}",
                dataType: "json",
                type: "get",
                data: {
                    open: '{!! isset($_GET['open']) !!}',
                    mine: (self.mine) ? 1 : 0
                },
                success: function (response) {
                    if (response.tasks) {
                        self.tasks = response.tasks;
                    }
                }
            });
        }

        $(document).ready(function () {
            new Vue({
                        el: '#todo',
                        data: {
                            tasks: {},
                            selected: {
                                name: "Nothing",
                                description: '' +
                                '<blockquote>' +
                                '<p>Please, select any task from the left menu.</p>' +
                                '<i class="fa fa-hand-o-left" aria-hidden="true"></i> ' +
                                '</blockquote>'
                            },
                            toggle: true,
                            panels: true,
                            mine: false,
                            searchString: "",
                            comment: "",
                            progress: ""
                        },
                        computed: {
                            FilteredTasks: function () {

                                var self = this;
                                if (!this.searchString) {
                                    return self.tasks;
                                }

                                this.searchString = this.searchString.trim().toLowerCase();
                                return self.tasks.filter(function (task) {
                                    if (task.name.toLowerCase().indexOf(self.searchString) !== -1) {
                                        return task;
                                    }
                                });
                            },
                            togglePanel: function () {
                                if (this.panels) {
                                    return "On";
                                } else {
                                    return "Off"
                                }
                            }
                        },
                        beforeCreate: function () {
                            loadData(this);
                            loadConfiguration(this);
                        },
                        updated: function () {
                            momentoForDates();
                        },
                        methods: {
                            toggleList: function () {
                                this.toggle = !this.toggle;
                                $.ajax({
                                    url: "{{route('admin:todo:api:list')}}",
                                    type: "GET",
                                    data: {
                                        value: (this.toggle) ? 1 : 0
                                    }
                                });
                            },
                            togglePanels: function () {
                                this.panels = !this.panels;
                                $.ajax({
                                    url: "{{route('admin:todo:api:panel')}}",
                                    type: "GET",
                                    data: {
                                        value: (this.panels) ? 1 : 0
                                    }
                                });
                            },
                            newTask: function () {
                                var parent = this;
                                $.confirm({
                                    title: 'Create new Task',
                                    content: "<form class='newTask' method='POST' action='/admin/todo/api/save'>" +
                                    "<div class='form-group'>" +
                                    "<label>Task name: </label>" +
                                    "<input name='name' type='text' class='form-control' required>" +
                                    "</div>" +
                                    "<div class='form-group'>" +
                                    "<label>Due date: </label>" +
                                    "<input name='due_date_at' type='date' class='form-control' required>" +
                                    "</div>" +
                                    "<div class='form-group'>" +
                                    "<label>Assign to: </label>" +
                                    "<select class='form-control users' name='user_id'></select>" +
                                    "</div>" +
                                    "<div>" +
                                    "<label>Priority: </label>" +
                                    "<select class='form-control' name='priority_id'>" +
                                    "<option value='1'>Lowest</value>" +
                                    "<option value='2'>Low</value>" +
                                    "<option value='3'>Medium</value>" +
                                    "<option value='4'>Highest</value>" +
                                    "</select>" +
                                    "</div>" +
                                    "<div class='form-group'>" +
                                    "<label>Task description: </label>" +
                                    "<textarea name='description' class='form-control' required></textarea>" +
                                    "</div>" +
                                    "<input type='hidden' name='_token' value='{{ csrf_token() }}'>" +
                                    "</form>",
                                    type: 'dark',
                                    typeAnimated: true,
                                    closeIcon: true,
                                    columnClass: 'col-xs-offset-2 col-xs-9',
                                    onContentReady: function () {
                                        var $list = this.$content.find('.users');
                                        $.ajax({
                                            url: "/admin/todo/api/users",
                                            type: "POST",
                                            dataType: "json",
                                            success: function (response) {
                                                $.each(response.users, function (k, v) {
                                                    $list.append("<option value=" + k + ">" + v + "</option>");
                                                });
                                                $list.find("option[value={{Request::user()->id}}]").attr('selected', true);
                                            }
                                        });
                                        this.$content.find('textarea').trumbowyg({
                                            btns: [
                                                ['viewHTML'],
                                                ['undo', 'redo'],
                                                ['formatting'],
                                                'btnGrp-design',
                                                ['link'],
                                                'btnGrp-justify',
                                                'btnGrp-lists',
                                                ['horizontalRule'],
                                                ['fullscreen']
                                            ]
                                        });
                                    },
                                    buttons: {
                                        create: {
                                            text: 'Create',
                                            btnClass: 'btn-green',
                                            action: function () {
                                                var form = this.$content.find("form");
                                                var data = form.serializeArray();
                                                var errors = [];
                                                $.each(data, function (k, v) {
                                                    if (!v.value) {
                                                        errors.push("Required field " + v.name);
                                                    }
                                                });
                                                if (errors.length != 0) {
                                                    $.alert({
                                                        title: "Error",
                                                        content: errors.join('</br>'),
                                                        type: 'red'
                                                    });
                                                    return false;
                                                }
                                                $.ajax({
                                                    url: form.attr('action'),
                                                    type: form.attr('method'),
                                                    dataType: "json",
                                                    data: form.serializeArray(),
                                                    success: function () {
                                                        loadData(parent);
                                                    },
                                                    error: function () {
                                                        $.alert({
                                                            title: "Error",
                                                            content: "Task Not saved",
                                                            type: 'red'

                                                        })
                                                    }
                                                });
                                                $.alert({
                                                    title: "Saved",
                                                    content: "New Task saved",
                                                    type: "green"
                                                })
                                            }
                                        },
                                        close: function () {
                                        }
                                    }
                                });
                            },
                            openTask: function (task) {
                                this.selected = task;
                            },
                            editDescription: function (task) {
                                var self = this;
                                console.log(Boolean({{Request::user()->role->isAdminrules()}}));
                                if (Boolean({{Request::user()->role->isAdminrules()}})
                                        || self.selected.user_id == {{(Request::user())?Request::user()->id:0   }}) {
                                    $.confirm({
                                        title: 'Edit Task description',
                                        type: 'dark',
                                        typeAnimated: true,
                                        closeIcon: true,
                                        columnClass: 'col-xs-offset-2 col-xs-8',
                                        content: "<form class='form-vertical'>" +
                                        "<div class='form-group'>" +
                                        "<textarea name='description' class='form-control' style='height:50vh' required>" +
                                        task.description + "</textarea>" +
                                        "</div>" +
                                        "<input type='hidden' name='_token' value='{{ csrf_token() }}'>" +
                                        "</form>",
                                        onContentReady: function () {
                                            this.$content.find('textarea').trumbowyg({
                                                btns: [
                                                    ['viewHTML'],
                                                    ['undo', 'redo'],
                                                    ['formatting'],
                                                    'btnGrp-design',
                                                    ['link'],
                                                    'btnGrp-justify',
                                                    'btnGrp-lists',
                                                    ['horizontalRule'],
                                                    ['fullscreen']
                                                ]
                                            });
                                        },
                                        buttons: {
                                            create: {
                                                text: 'Create',
                                                btnClass: 'btn-green',
                                                action: function () {
                                                    $.ajax({
                                                        url: "/admin/todo/api/update",
                                                        dataType: "json",
                                                        type: "post",
                                                        data: {
                                                            "description": this.$content.find('textarea').val(),
                                                            "id": task.id
                                                        },
                                                        success: function (response) {
                                                            self.selected = response.task;
                                                            loadData(self);
                                                            $.alert({
                                                                title: "Saved",
                                                                content: "Task updated",
                                                                type: "green"
                                                            });
                                                        },
                                                        error: function () {
                                                            $.alert({
                                                                title: "Error",
                                                                content: "Task Not updated",
                                                                type: 'red'

                                                            })
                                                        }
                                                    });
                                                }
                                            },
                                            close: function () {
                                            }
                                        }
                                    });
                                } else {
                                    $.alert({
                                        title: "Error",
                                        content: "You Have not permission to edit Task",
                                        type: "red"
                                    });
                                    return true;
                                }
                            },
                            stopTask: function (task) {
                                return manipulateTask(
                                        task,
                                        "STOP task?",
                                        "Do you want stop task?",
                                        'red',
                                        "/admin/todo/api/stop",
                                        this,
                                        ['enter']
                                );
                            },
                            reOpenTask: function (task) {
                                return manipulateTask(
                                        task,
                                        "Re-Open task?",
                                        "Do you want reopen task?",
                                        'red',
                                        "/admin/todo/api/re_open",
                                        this,
                                        ['enter']
                                );
                            },
                            completeTask: function (task) {
                                return manipulateTask(
                                        task,
                                        "Complete",
                                        "Complete Task? ",
                                        'green',
                                        "/admin/todo/api/complete",
                                        this,
                                        ['enter']
                                );
                            },
                            startTask: function (task) {
                                return manipulateTask(
                                        task,
                                        "Start task?",
                                        "Do you want begin task?",
                                        'purple',
                                        "/admin/todo/api/start",
                                        this,
                                        ['enter']
                                );
                            },
                            assignTask: function (task) {
                                return manipulateTask(
                                        task,
                                        "Assign Task?",
                                        "Do you want take this Task?",
                                        'purple',
                                        "/admin/todo/api/assign",
                                        this,
                                        ['enter']
                                );
                            },
                            declineTask: function (task) {
                                return manipulateTask(
                                        task,
                                        "Decline Task?",
                                        "Do you want decline this Task?",
                                        'red',
                                        "/admin/todo/api/decline",
                                        this,
                                        ['enter']
                                );
                            },
                            addComment: function (task) {
                                if (!this.comment) {
                                    $.alert({
                                        title: "Error",
                                        content: "Something went wrong",
                                        type: 'red'
                                    });
                                    return false;
                                }
                                var self = this;
                                $.ajax({
                                    url: "/admin/todo/api/comment",
                                    dataType: "json",
                                    type: "post",
                                    data: {
                                        'comment': this.comment,
                                        'task_id': task.id
                                    },
                                    success: function (response) {
                                        self.comment = "";
                                        self.selected = response.task;
                                        loadData(self);
                                    },
                                    error: function () {

                                    }
                                })

                            },
                            mineTasks: function () {
                                this.mine = !this.mine;
                                loadData(this);
                            }
                        }
                    }
            )
            ;
        });
        function manipulateTask(task, $title, $content, $color, $url, $self, $keys) {
            $.confirm({
                title: $title,
                content: $content,
                type: $color,
                typeAnimated: true,
                closeIcon: true,
                columnClass: 'col-xs-offset-5 col-xs-2',
                buttons: {
                    yes: {
                        title: "Yes",
                        keys: $keys,
                        action: function () {
                            $.ajax({
                                url: $url,
                                dataType: "json",
                                type: "post",
                                data: {
                                    "id": task.id
                                },
                                success: function (response) {
                                    $self.selected = response.task;
                                    loadData($self);
                                }
                            });

                        }
                    },
                    no: {
                        title: "No",
                        keys: ['esc'],
                        action: function () {

                        }
                    }
                }
            })
        }
    </script>
@stop
