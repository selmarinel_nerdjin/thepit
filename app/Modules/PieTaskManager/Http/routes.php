<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function () {
    Route::group(['middleware' => 'JournalistAuth'], function () {
        Route::group(['prefix' => 'todo'], function () {
            $asAction = ':todo';
            Route::get('/', ['as' => $asAction . ':index', 'uses' => 'ToDoController@index']);
            Route::group(['prefix' => 'api'], function () use ($asAction) {
                $api = ':api';
                Route::get('/load', ['as' => $asAction .$api. ':load', 'uses' => 'ToDoController@load']);
                Route::get('/configuration', ['as' => $asAction .$api. ':configuration', 'uses' => 'ToDoController@configuration']);

                Route::get('/p/s', ['as' => $asAction .$api. ':panel', 'uses' => 'ToDoController@savePanelState']);
                Route::get('/l/s', ['as' => $asAction .$api. ':list', 'uses' => 'ToDoController@saveListState']);

                Route::post('/save', ['as' => $asAction .$api. ':save', 'uses' => 'ToDoController@save']);
                Route::post('/update', ['as' => $asAction .$api. ':update', 'uses' => 'ToDoController@update']);
                Route::post('/start', ['as' => $asAction .$api. ':start', 'uses' => 'ToDoController@start']);
                Route::post('/stop', ['as' => $asAction .$api. ':stop', 'uses' => 'ToDoController@stop']);
                Route::post('/complete', ['as' => $asAction .$api. ':complete', 'uses' => 'ToDoController@complete']);
                Route::post('/re_open', ['as' => $asAction .$api. ':re_open', 'uses' => 'ToDoController@reOpen']);
                Route::post('/users', ['as' => $asAction .$api. ':users', 'uses' => 'ToDoController@users']);
                Route::post('/assign', ['as' => $asAction .$api. ':assign', 'uses' => 'ToDoController@assign']);
                Route::post('/decline', ['as' => $asAction .$api. ':decline', 'uses' => 'ToDoController@decline']);
                Route::post('/comment', ['as' => $asAction .$api. ':comment', 'uses' => 'ToDoController@comment']);
            });
        });
    });
});