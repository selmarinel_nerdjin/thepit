<?php
namespace App\Modules\PieTaskManager\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;

class TaskCommentsRepository extends Base
{
    protected $model;
    protected $modelName = App\Modules\PieTaskManager\Database\Models\TaskComment::class;
    protected $hasStatus = true;
    
    
}