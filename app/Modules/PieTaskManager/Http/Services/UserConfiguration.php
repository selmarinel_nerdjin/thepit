<?php
/**
 * Created by PhpStorm.
 * User: selmarinel
 * Date: 01.12.16
 * Time: 19:11
 */

namespace App\Modules\PieTaskManager\Http\Services;


use App\Modules\PieBase\Database\Models\User;
use App\Modules\PieBase\Http\Services\Base;
use App\Modules\PieTaskManager\Database\Models\Config;
use App\Modules\PieTaskManager\Database\Models\UserConfig;

class UserConfiguration extends Base
{
    protected $model;
    protected $modelName = UserConfig::class;
    protected $hasStatus = false;

    public function getConfigs(User $user)
    {
        return [
            'panels' => $this->getConfig($user, Config::PANELS)->value,
            'list' => $this->getConfig($user, Config::SHOW_LIST)->value,
            'self' => $this->getConfig($user, Config::TO_ME)->value,
        ];

    }

    protected function getConfig(User $user, $type)
    {
        return ($this->getModel()
            ->query()
            ->where('user_id', $user->id)
            ->where('config_id', $type)
            ->first()) ?: $this->defaultConfig($user, $type);
    }

    protected function defaultConfig(User $user, $type)
    {
        return $this->getModel()->create([
            'user_id' => $user->id,
            'config_id' => $type,
            'value' => 1
        ]);
    }

    protected function updateConfig(User $user, $type, $value = 1)
    {
        return $this->getConfig($user, $type)->update(['value' => ($value) ? 1 : 0]);
    }

    public function saveConfig(User $user, $type, $value)
    {
        return $this->updateConfig($user, $type, $value);
    }

}