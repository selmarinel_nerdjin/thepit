<?php
namespace App\Modules\PieTaskManager\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;
use Illuminate\Http\Request;


class TaskRepository extends Base
{
    protected $model;
    protected $modelName = App\Modules\PieTaskManager\Database\Models\Task::class;
    protected $hasStatus = false;

    public function load(Request $request)
    {
        $open = $request->input('open');
        $mine = $request->input('mine');
        $collection = $this->getModel()->query();
        if ($open) {
            $collection = $collection->where("completed_at", "=", "0000-00-00 00:00:00");
        }
        if($mine){
            $collection = $collection->where("user_id", $request->user()->id);
        }
        $collection = $collection->orderBy('due_date_at')->orderBy('priority_id','desc')->get();
        $result = [];
        foreach ($collection as $model) {
            $result[] = $model->getInfo();
        }
        return $result;
    }
}