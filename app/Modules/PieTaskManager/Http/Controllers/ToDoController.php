<?php
namespace App\Modules\PieTaskManager\Http\Controllers;

use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use App\Modules\PieBase\Http\Services\User;
use App\Modules\PieTaskManager\Database\Models\Config;
use App\Modules\PieTaskManager\Database\Models\Task;
use App\Modules\PieTaskManager\Database\Models\TaskComment;
use App\Modules\PieTaskManager\Http\Services\TaskRepository;
use App\Modules\PieTaskManager\Http\Services\UserConfiguration;
use Illuminate\Http\Request;

class ToDoController extends Controller
{
    protected $prefix = 'admin:todo';
    protected $moduleName = 'pie_task_manager';

    protected $serviceName = TaskRepository::class;

    public function index()
    {
        return view($this->getViewRoute());
    }

    public function configuration(Request $request)
    {
        $uConfig = new UserConfiguration();
        try {
            return $this->sendOk(['config' => $uConfig->getConfigs($request->user())]);
        } catch (\Exception $e) {
            return $this->sendWithErrors($e->getMessage());
        }
    }

    protected function saveState(\App\Modules\PieBase\Database\Models\User $user, $type, $value)
    {
        $uConfig = new UserConfiguration();
        try {
            $uConfig->saveConfig($user, $type, $value);
            return $this->sendOk(['success']);
        } catch (\Exception $e) {
            return $this->sendWithErrors($e->getMessage());
        }
    }

    public function savePanelState(Request $request)
    {
        return $this->saveState($request->user(),Config::PANELS,($request->input('value')) ? 1 : 0);
    }

    public function saveListState(Request $request)
    {
        return $this->saveState($request->user(),Config::SHOW_LIST,($request->input('value')) ? 1 : 0);
    }


    public function load(Request $request)
    {
        $collection = $this->service->load($request);
        return $this->sendOk(['tasks' => $collection]);
    }

    public function save(Request $request)
    {
        $this->setRules([
            'name' => 'required',
            'description' => 'required',
            'due_date_at' => 'required',
            'priority_id' => 'required',
            'user_id' => 'required'
        ]);
        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        if ($this->service->getModel()->fill($this->getRulesInput($request))->save()) {
            return $this->sendOk(['success']);
        }
        return $this->sendWithErrors('error');
    }

    public function update(Request $request)
    {
        $this->setRules([
            'id' => 'required|exists:tasks,id',
            'description' => 'required',
        ]);
        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        return $this->updateTask($request, ['description' => $request->input('description')]);
    }

    protected function timeAction(Request $request, Array $data)
    {
        $this->setRules([
            'id' => 'required|exists:tasks,id',
        ]);
        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        return $this->updateTask($request, $data);
    }

    public function start(Request $request)
    {
        return $this->timeAction($request, [
            'state' => Task::TASK_STARTED,
            'started_at' => (new \DateTime('now'))
                ->setTimezone(new \DateTimeZone("Europe/Kiev"))
                ->format("Y-m-d H:i:s")]);
    }

    public function stop(Request $request)
    {
        return $this->timeAction($request, [
            'state' => Task::TASK_OPEN,
            'started_at' => "0000-00-00 00:00:00"]);
    }

    public function complete(Request $request)
    {
        return $this->timeAction($request, [
            'state' => Task::TASK_COMPLETED,
            'completed_at' => (new \DateTime('now'))
                ->setTimezone(new \DateTimeZone("Europe/Kiev"))
                ->format("Y-m-d H:i:s")
        ]);
    }

    public function reOpen(Request $request)
    {
        return $this->timeAction($request, [
            'state' => Task::TASK_OPEN,
            'completed_at' => "0000-00-00 00:00:00"
        ]);
    }

    public function assign(Request $request)
    {
        return $this->timeAction($request, [
            'user_id' => $request->user()->id,
        ]);
    }

    public function decline(Request $request)
    {
        return $this->timeAction($request, [
            'user_id' => 0,
        ]);
    }

    protected function updateTask(Request $request, Array $data)
    {
        $model = $this->service->getOne($request->input('id'));

        if ($model) {
            if ($model->fill($data)->save()) {
                return $this->sendOk(['success', 'task' => $model->getInfo()]);
            }
        }
        return $this->sendWithErrors('error');
    }

    public function users(Request $request)
    {
        $users = new User();
        return $this->sendOk(['users' => $users->prepareSelect()]);
    }

    public function comment(Request $request)
    {
        $this->setRules([
            'task_id' => 'required|exists:tasks,id',
            'comment' => 'required'
        ]);
        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }

        $comment = new TaskComment();
        $comment->fill([
            'user_id' => $request->user()->id,
            'task_id' => $request->input('task_id'),
            'comment' => $request->input('comment')
        ]);
        $comment->save();
        return $this->sendOk(['success', 'task' => $this->service->getOne($request->input('task_id'))->getInfo()]);
    }

}