<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\PieTaskManager\Database\Models\Config;

class UserConfiguration extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('todo_config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('config_name');
            $table->string('config_value');
        });

        Config::create([
            'id' => 1,
            'config_name' => 'panels',
            'config_value' => 1
        ]);
        Config::create([
            'id' => 2,
            'config_name' => 'list',
            'config_value' => 1
        ]);
        Config::create([
            'id' => 3,
            'config_name' => 'to_me',
            'config_value' => 0
        ]);

		Schema::create('todo_user_configuration', function (Blueprint $table) {
			$table->increments('id');
            $table->unsignedInteger('user_id')->reference('id')->on('users');
            $table->unsignedTinyInteger('config_id')->reference('id')->on('todo_config');
            $table->tinyInteger('value')->default(1);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('todo_user_configuration');
		Schema::dropIfExists('todo_config');
	}
}
