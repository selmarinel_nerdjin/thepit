<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\PieTaskManager\Database\Models\TaskPriority;

class InstallTodo extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('task_priority', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        TaskPriority::create(['name' => 'Lowest']);
        TaskPriority::create(['name' => 'Low']);
        TaskPriority::create(['name' => 'Medium']);
        TaskPriority::create(['name' => 'Highest']);

		Schema::create('tasks', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->text('description');
            $table->integer('user_id')->references('id')->on('users');
            $table->integer('priority_id')->references('id')->on('task_priority');
            $table->timestamp('started_at');
            $table->timestamp('due_date_at');
            $table->timestamp('completed_at');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('task_priority');
		Schema::dropIfExists('tasks');
	}
}
