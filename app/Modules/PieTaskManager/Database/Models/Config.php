<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 14.11.2016
 * Time: 20:31
 */

namespace App\Modules\PieTaskManager\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

class Config extends Base
{

    protected $table = 'todo_config';

    public $timestamps = false;

    CONST PANELS = 1;
    CONST SHOW_LIST = 2;
    CONST TO_ME = 3;

    protected $fillable = [
        'config_name',
        'config_value'
    ];
}