<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 14.11.2016
 * Time: 20:31
 */

namespace App\Modules\PieTaskManager\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

class Task extends Base
{

    protected $with = [
        'priority',
        'comments'
    ];
    protected $table = 'tasks';


    const TASK_OPEN = 0;
    const TASK_STARTED = 1;
    const TASK_COMPLETED = 2;

    protected $fillable = [
        'name',
        'description',
        'user_id',
        'priority_id',
        'started_at',
        'due_date_at',
        'completed_at',
        'state'
    ];

    public function getStateClass()
    {
        if ($this->state == static::TASK_COMPLETED) {
            return "done";
        } elseif ($this->state == static::TASK_STARTED) {
            return "timer";
        } else {
            return "assignment";
        }
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function priority()
    {
        return $this->hasOne(TaskPriority::class, 'id', 'priority_id');
    }

    public function comments()
    {
        return $this->hasMany(TaskComment::class);
    }

    public function getInfo()
    {
        return (object)[
            'created_at' => $this->created_at->setTimeZone(new \DateTimeZone('Europe/Kiev'))->format("Y-m-d H:i:s"),
            'updated_at' => $this->updated_at->setTimeZone(new \DateTimeZone('Europe/Kiev'))->format("Y-m-d H:i:s"),
            'completed_at' => $this->completed_at,
            'description' => nl2br($this->description),
            'info' => mb_substr(strip_tags($this->description),0,50),
            'due_date_at' => $this->due_date_at,
            'id' => $this->id,
            'name' => $this->name,
            'priority' => $this->priority->name,
            'color' => $this->priority->color,
            'priority_id' => $this->priority_id,
            'started_at' => $this->started_at,
            'user_id' => $this->user_id,
            'state' => $this->state,
            'user_name' => ($this->user) ? $this->user->getFullName() : "Nobody",
            'user_avatar' => ($this->user) ? $this->user->getCover() : App::make('logo'),
            'state_class' => $this->getStateClass(),
            'comments' => ($this->comments) ? $this->comments->map(function ($model) {
                return $model->getInfo();
            }) : [],
            'progress' => ($this->completed_at != "0000-00-00 00:00:00" && $this->started_at != "0000-00-00 00:00:00") ?
                $this->calculateDate($this->completed_at, $this->started_at) : "In Progress"
        ];
    }

    protected function calculateDate($completed, $started)
    {
        $diff = strtotime($completed) - strtotime($started);
        return (int)($diff / 3600) . " hour(s), " . ($diff / 60 % 60) . " minute(s) , " . $diff % 60 . " sec";
    }
}