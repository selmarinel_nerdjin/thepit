<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 14.11.2016
 * Time: 20:31
 */

namespace App\Modules\PieTaskManager\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

class TaskComment extends Base
{

    protected $table = 'task_comments';

    protected $fillable = [
        'user_id',
        'task_id',
        'status',
        'comment'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function task()
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    public function getInfo()
    {
        return (object) [
            'id' => $this->id,
            'user_name' => ($this->user) ? $this->user->getFullName() : "Deleted",
            'user_avatar' => ($this->user) ? $this->user->getCover() : App::make('logo'),
            'comment' => $this->comment,
            'created_at' => $this->created_at->setTimeZone(new \DateTimeZone('Europe/Kiev'))->format("Y-m-d H:i:s"),
        ];
    }

}