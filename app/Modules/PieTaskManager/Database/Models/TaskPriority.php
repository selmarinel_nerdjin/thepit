<?php
namespace App\Modules\PieTaskManager\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;

/**
 * Class Articles
 * @package App\Modules\PieArticle\Database\Models
 * @property App\Modules\PieTranslator\Database\Models\TranslateArticles $lang
 */
class TaskPriority extends Base
{
    public $timestamps = false;

    protected $table = 'task_priority';

    protected $fillable = [
        'name',
    ];

    const Types = [
        1 => "success",
        2 => "info",
        3 => "warning",
        4 => "danger",
    ];

    public function getColorAttribute()
    {
        return static::Types[$this->attributes['id']];
    }

}