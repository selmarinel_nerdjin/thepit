<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 14.11.2016
 * Time: 20:31
 */

namespace App\Modules\PieTaskManager\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

class UserConfig extends Base
{
    protected $table = 'todo_user_configuration';

    protected $fillable = [
        'user_id',
        'config_id',
        'value'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function config()
    {
        return $this->hasOne(Config::class, 'id', 'config_id');
    }
}