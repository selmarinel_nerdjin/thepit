<?php
namespace App\Modules\PieTaskManager\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PieTaskManagerDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('App\Modules\PieTaskManager\Database\Seeds\FoobarTableSeeder');
	}

}
