<?php
namespace App\Modules\PieTaskManager\Providers;

use Caffeinated\Modules\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use App\Modules\PieBase\Providers\Module;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your module's routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Modules\PieTaskManager\Http\Controllers';

    protected $moduleName = 'pie_task_manager';
    protected $assetsPath = 'assets/manager/';

    /**
     * Define your module's route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $this->initAssets();
        //
    }

    protected function initAssets()
    {
        $this->publishes([
            __DIR__ . '/../Resources/Assets' => public_path($this->assetsPath),
        ], $this->moduleName);

        $this->app->bind('pie_task_manager.assets', function () {
            return new Module($this->assetsPath);
        });
    }

    /**
     * Define the routes for the module.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require(config('modules.path') . '/PieTaskManager/Http/routes.php');
        });
    }
}
