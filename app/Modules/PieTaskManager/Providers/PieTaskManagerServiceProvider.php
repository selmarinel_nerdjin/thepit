<?php
namespace App\Modules\PieTaskManager\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieTaskManagerServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieTaskManager module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieTaskManager\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieTaskManager module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_task_manager', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_task_manager', base_path('resources/views/vendor/pie_task_manager'));
		View::addNamespace('pie_task_manager', realpath(__DIR__.'/../Resources/Views'));
	}
}
