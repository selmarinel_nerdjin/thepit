<?php
namespace App\Modules\PieTest\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieTestServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieTest module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieTest\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieTest module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_test', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_test', base_path('resources/views/vendor/pie_test'));
		View::addNamespace('pie_test', realpath(__DIR__.'/../Resources/Views'));
	}
}
