@extends('pie_base::admin.layouts.edit')

@section('title_name')
    {{trans('pie_static::main.sub_menu_static')}}
@endsection

@section("head")
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('js/lib/trumbowyg/ui/trumbowyg.min.css')}}">
@endsection

@section('form_body')
    <div class="form-group">
        {!! Form::label('Language') !!}
        {!! Form::select('language', $lang, $model->language, ['class'=>'form-control']) !!}
    </div>

    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    {!! Form::hidden('static_page_id', ($model->static_page_id)?$model->static_page_id:$page->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label(trans('pie_base::main.table_name')) !!}
        {!! Form::text('title_translate', ($model->title_translate)?$model->title_translate:$page->title, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_info')) !!}
        {!! Form::text('info_translate', ($model->info_translate)?$model->info_translate:$page->info, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_text')) !!}
        {!! Form::textarea('content_translate', ($model->content_translate)?$model->content_translate:$page->content, ['class'=>'form-control','id'=>'edit']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_active')) !!}
        {!! Form::checkbox('status',true,($model->status == \App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE)) !!}
    </div>
@endsection

@section('scripts')
    @yield('add_script')
    @yield('media_script')
    {{--Trumbowyg--}}
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/trumbowyg.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/pasteimage/trumbowyg.pasteimage.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/base64/trumbowyg.base64.min.js')}}"></script>
    <link rel="stylesheet"
          href="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/colors/ui/trumbowyg.colors.min.css')}}">
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/colors/trumbowyg.colors.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/noembed/trumbowyg.noembed.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/upload/trumbowyg.upload.min.js')}}"></script>

    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/table/trumbowyg.table.min.js')}}"></script>
    <script>
        $(function () {
            $('#edit').trumbowyg({

                btnsDef: {
                    image: {
                        dropdown: ['insertImage', 'base64', 'noembed'],
                        ico: 'insertImage'
                    }
                },
                btns: [
                    ['viewHTML'],
                    ['undo', 'redo'],
                    ['formatting'],
                    'btnGrp-design',
                    ['link'],
                    ['image'],
                    'btnGrp-justify',
                    'btnGrp-lists',
                    ['table'],
                    ['foreColor', 'backColor'],
                    ['preformatted'],
                    ['horizontalRule'],
                    ['fullscreen']
                ]
            });
        });

    </script>
@endsection