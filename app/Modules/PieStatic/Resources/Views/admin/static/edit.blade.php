@extends('pie_base::admin.layouts.edit')

@section("head")
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('js/lib/trumbowyg/ui/trumbowyg.min.css')}}">
    @endsection

@section('title_name')
    {{trans('pie_static::main.sub_menu_static')}}
@endsection

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    {!! Form::hidden('file_id', $model->file_id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label('Тайтл') !!}
        {!! Form::text('title', $model->title, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Slug') !!}
        {!! Form::text('slug', $model->slug, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Информация') !!}
        {!! Form::text('info', $model->info, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Обложка') !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i class="glyphicon glyphicon-plus"></i> Выбрать обложку
                </div></span>
            {!! Form::file('cover',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{$model->getCover()}}" width="250" class="img-responsive">
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('Отображать изображение') !!}
        {!! Form::checkbox('display_image',true,$model->display_image) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Отображать в меню') !!}
        {!! Form::checkbox('in_menu',true,$model->in_menu) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Позиция в меню') !!}
        {!! Form::number('position',$model->position ,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Контент') !!}
        {!! Form::textarea('content', $model->content, ['class'=>'form-control','id'=>'edit']) !!}
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            SEO
        </div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::label('@Keywords') !!}
                {!! Form::text('keywords', $model->keywords, ['class'=>'form-control','id'=>'keywords']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('@Description') !!}
                {!! Form::textarea('description', $model->description, ['class'=>'form-control','rows'=>3]) !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @yield('add_script')
    @yield('media_script')
    {{--Trumbowyg--}}
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/trumbowyg.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/pasteimage/trumbowyg.pasteimage.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/base64/trumbowyg.base64.min.js')}}"></script>
    <link rel="stylesheet"
          href="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/colors/ui/trumbowyg.colors.min.css')}}">
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/colors/trumbowyg.colors.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/noembed/trumbowyg.noembed.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/upload/trumbowyg.upload.min.js')}}"></script>

    <script src="{{$app['pie_base.assets']->getPath('/js/lib/trumbowyg/plugins/table/trumbowyg.table.min.js')}}"></script>
    <script>
        $(function () {
            $('#edit').trumbowyg({

                btnsDef: {
                    image: {
                        dropdown: ['insertImage', 'base64', 'noembed'],
                        ico: 'insertImage'
                    }
                },
                btns: [
                    ['viewHTML'],
                    ['undo', 'redo'],
                    ['formatting'],
                    'btnGrp-design',
                    ['link'],
                    ['image'],
                    'btnGrp-justify',
                    'btnGrp-lists',
                    ['table'],
                    ['foreColor', 'backColor'],
                    ['preformatted'],
                    ['horizontalRule'],
                    ['fullscreen']
                ]
            });
        });

    </script>
@endsection