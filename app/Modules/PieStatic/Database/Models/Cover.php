<?php
namespace App\Modules\PieStatic\Database\Models;

use App\Modules\PieBase\Database\Models\File;

class Cover extends File
{
    protected $path = 'static/';
}
