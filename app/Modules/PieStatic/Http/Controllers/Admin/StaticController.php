<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 21.07.2016
 * Time: 15:41
 */

namespace App\Modules\PieStatic\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Modules\PieBase\Http\Requests\Response;

class StaticController extends Controller
{
    protected $prefix = 'admin:static';
    protected $moduleName = 'pie_static';
    protected $serviceName = 'App\Modules\PieStatic\Http\Services\StaticPie';

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'lang' => App::make('App\Modules\PieTranslator\Http\Services\Languages')->prepareSelect()
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'title' => 'required|min:2',
            'slug' => 'required|unique:static_pages|min:2',
            'info' => 'required|min:3',
            'content' => 'required|min:5',
            'keywords' => '',
            'description' => '',
            'display_image' => '',
            'in_menu' => '',
            'position' => 'required|numeric',
            'file_id' => '',
        ]);
        if ($request->method() != 'GET' && isset($request['cover'])) {
            $imageId = $this->saveFile($request);
            $request->merge(array('file_id' => $imageId));
        }
        return parent::add($request);
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'title' => 'required|min:2',
            'slug' => 'required|min:2',
            'info' => 'required|min:3',
            'content' => 'required|min:5',
            'keywords' => '',
            'description' => '',
            'display_image' => '',
            'in_menu' => '',
            'position' => 'required|numeric',
            'file_id' => '',
        ]);

        if ($request->hasFile('cover')) {
            $imageId = $this->saveFile($request);
            $request->merge(array('file_id' => $imageId));
        }
        return parent::edit($request, $id);
    }

    private function saveFile(Request $request)
    {
        $service = new App\Modules\PieStatic\Http\Services\Image($request->file('cover'));
        $service->saveFile($request->user()->id);
        return $service->getModel()->id;
    }

    public function upload(Request $request)
    {
        $service = new App\Modules\PieStatic\Http\Services\Image($request->file('file'));
        $service->saveFile($request->user()->id);
        return new Response(json_encode(["link" => $service->getModel()->getCover()]));
    }

    public function addTranslate(Request $request, $id)
    {
        return view($this->getViewRoute('translate.edit'),
            $this->prepareData($this->service->getOne($id)));
    }

    public function saveTranslate(Request $request)
    {
        $this->setRules([
            'title_translate' => 'required|min:2',
            'info_translate' => 'required|min:3',
            'content_translate' => 'required|min:5',
        ]);
        $translate = new App\Modules\PieStatic\Http\Services\StaticPieTranslate();
        if ($translate->getModel()
            ->where('static_page_id', $request->input('static_page_id'))
            ->where('language', $request->input('language'))
            ->first()
        ) {
            return $this->sendWithErrors(['This language already added']);
        }
    }
}