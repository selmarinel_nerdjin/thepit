@extends('pie_base::admin.layouts.edit')

@section('title_name')Translate@endsection

@section('form_body')
    <div class="form-group">
        {!! Form::label('Language') !!}
        {!! Form::select('language', $lang, $model->language, ['class'=>'form-control']) !!}
    </div>
    {!! Form::hidden('id', ($model && $model->id) ? $model->id : '', ['class'=>'form-control']) !!}
    {!! Form::hidden('category_id', ($model && $model->id) ? $model->category_id : $category->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label(trans('pie_base::main.table_name')) !!}
        {!! Form::text('name', ($model->name)?$model->name : $category->name, ['class'=>'form-control']) !!}
    </div>
@endsection