<?php
namespace App\Modules\PieTranslator\Http\Services;

use App\Modules\PieBase\Http\Services\Base;
use App\Modules\PieTranslator\Database\Models\CategoryTranslate;

class CategoryLanguages extends Base
{
    protected $model;
    protected $modelName = CategoryTranslate::class;
    protected $hasStatus = false;
    protected $orderBy = [];

    public function prepareSelect()
    {
        $langs = [];
        foreach ($this->getAll() as $lang) {
            $langs[$lang->id] = $lang->name;
        }
        return $langs;
    }
}