<?php
namespace App\Modules\PieTranslator\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class ArticleTranslator extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieTranslator\Database\Models\TranslateArticles';
    protected $orderBy = [
        'created_at' => 'desc'
    ];
    protected $hasStatus = true;
}