<?php

namespace App\Modules\PieTranslator\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class CategoryTranslatorController extends Controller
{
    protected $prefix = 'admin:category:translate';
    protected $moduleName = 'pie_translator';
    protected $serviceName = App\Modules\PieTranslator\Http\Services\CategoryLanguages::class;

    public function index()
    {
        return redirect(route('admin:categories:index'));
    }

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'lang' => App::make('App\Modules\PieTranslator\Http\Services\Languages')->prepareSelect(),
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'name' => 'required|min:2',
            'language' => 'required',
            'category_id' => 'required'
        ]);
        if ($this->service->getModel()
            ->where('category_id', $request->input('category_id'))
            ->where('language', $request->input('language'))
            ->first()
        ) {
            return $this->sendWithErrors(['This language already added']);
        }
        return parent::add($request);
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'name' => 'required|min:2',
            'language' => 'required',
            'category_id' => 'required'
        ]);
        if ($this->service->getModel()
            ->where('category_id', $request->input('category_id'))
            ->where('language', $request->input('language'))
            ->first()
        ) {
            return $this->sendWithErrors(['This language already added']);
        }
        return parent::edit($request, $id);
    }

    public function addTranslate(Request $request, $id)
    {
        $category = App\Modules\PieArticle\Database\Models\Category::query()->find($id);
        return view($this->getViewRoute('edit'), array_merge(['category' => $category], $this->prepareData()));
    }
}