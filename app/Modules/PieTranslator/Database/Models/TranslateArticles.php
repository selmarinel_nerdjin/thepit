<?php
namespace App\Modules\PieTranslator\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;

class TranslateArticles extends Base
{
    public $timestamps = true;
    protected $table = 'translate_articles';

    protected $fillable = array(
        'article_id',
        'language',
        'trans_name',
        'trans_info',
        'trans_text',
        'status'
    );

    CONST DEFAULT_LANGUAGE = 1;
    
    CONST EN = 1;
    CONST RU = 2;
    CONST UA = 3;

    public function article(){
        return $this->belongsTo('\App\Modules\PieArticle\Database\Models\Articles','article_id');
    }

    public function lang(){
        return $this->belongsTo('\App\Modules\PieTranslator\Database\Models\LanguageModel','language');
    }

    public function setStatusAttribute($value){
        $this->attributes['status'] = isset($value)? intval($value) : 0;
    }
}