<?php
namespace App\Modules\PieTranslator\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;

class CategoryTranslate extends Base
{
    public $timestamps = false;

    protected $table = 'category_translates';

    protected $fillable = array(
        'name',
        'language',
        'category_id'
    );

    public function lang()
    {
        return $this->hasOne(App\Modules\PieBase\Database\Models\Language::class,'id', 'language');
    }

    public function category()
    {
        return $this->hasOne(App\Modules\PieArticle\Database\Models\Category::class,'id','category_id');
    }
}