<!DOCTYPE html>
<html>
<head>
    <title>404 Error | The PIT</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #2b2a28;
            display: table;
            font-weight: 100;
            font-family: 'Roboto';
            background-color: #fff;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }

        blockquote {
            font-size: 1.5em;
        }

        blockquote footer {
            text-align: right;
            font-size: .7em;
            color: #ff9624;
        }

        a {
            color: #DC7A23;
            font-weight: bold;
            text-decoration: none;
            transition: .5s;
        }

        a:hover {
            color: #d3dc22;
            text-decoration: none;
        }
        .links{
            display: block;
            width: 100%;
        }
        .links a{
            padding: 0 10px;
        }
        .links a:first-child{
            border-right: solid 1px #2B2A28;
        }
        .links a:last-child{
            border-left: solid 1px #2B2A28;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <img src="/images/404.png" width="150">
        <div class="title">404 ERROR<br/></div>
        <blockquote>
            <p>«{{trans('404.blockquote')}}»</p>
            <footer> {{trans('404.author')}}</footer>
        </blockquote>
        <div class="links">
            <a href="{{route("site:index")}}">{{trans('404.back_to_index')}}</a>
            <a>Something went wrong</a>
            <a href="{{route("site:news")}}">{{trans('404.back')}}</a>
        </div>
    </div>
</div>
</body>
</html>
