<!DOCTYPE html>
<html>
<head>
    <title>500 Error | The PIT</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #2b2a28;
            display: table;
            font-weight: 100;
            font-family: 'Roboto';
            background-color: #fff;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
        blockquote{
            font-size: 1.5em;
        }
        blockquote footer{
            text-align: right;
            font-size: .7em;
            color: #ff9624;
        }
        a{
            color: #DC7A23;
            font-weight: bold;
            text-decoration: none;
            transition: .5s;
        }
        a:hover{
            color: #d3dc22;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <img src="/images/500.png" width="150">
        <div class="title">500 ERROR<br/></div>
        <blockquote>
            <p>Неработающая программа обычно приносит меньше вреда, чем работающая плохо.</p>
            <footer>Dave Thomas</footer>
        </blockquote>
    </div>
</div>
</body>
</html>

